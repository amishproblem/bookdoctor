<!DOCTYPE html>
<html lang="TH">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<link rel="stylesheet" href="/assets/bootstrap4/css/bootstrap.css">
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,700&display=swap" rel="stylesheet">

		<link rel="icon" type="image/png" href="/assets/images/b-icon.png">

		<title>BOOKDOCTOR</title>

		<style>
			html,
			body {
				width: 100%;
				height: 100%;
			}
			body {
				color: #fcca49;
				font-family: 'Poppins', sans-serif;
				font-size: 16px;
				font-weight: 700;
				line-height: 1;
				background-color: #1f242a;
				outline: 0;
				-webkit-font-smoothing: antialiased;
				-moz-osx-font-smoothing: grayscale;
			}
			.coming-soon {
				width: 100%;
				height: 100%;
			}
			h1 {
				color: #fcca49;
				
				font-size: 4rem;
				font-weight: 100;
			}
			@media (max-width: 575.98px) {
				h1 {
					font-size: 3rem;
				}
			}
			.value {
				font-size: 4rem;
				min-width: 90px;
			}
			.tag {
				font-size: 0.9rem;
				font-weight: 100;
			}
			@media (max-width: 575.98px) {
				.value {
					font-size: 3rem;
					min-width: 70px;
				}
			}
		</style>
	</head>
	<body>
		<div class="coming-soon py-5 d-flex align-items-center justify-content-center">
			<div>
				<h1 class="mb-5 text-center">Coming Soon</h1>
				<div class="counter d-flex flex-wrap align-items-center justify-content-center text-center">
					<div class="d-flex flex-column px-2 px-sm-3">
						<div class="value days">28</div>
						<div class="tag pb-5">Days</div>
					</div>
					<div class="d-flex flex-column px-2 px-sm-3">
						<div class="value hours">17</div>
						<div class="tag pb-5">Hours</div>
					</div>
					<div class="d-flex flex-column px-2 px-sm-3">
						<div class="value minutes">00</div>
						<div class="tag pb-5">Minutes</div>
					</div>
					<div class="d-flex flex-column px-2 px-sm-3">
						<div class="value seconds">00</div>
						<div class="tag pb-5">Seconds</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-156685062-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-156685062-1');
		</script>


		<script src="/assets/js/jquery.js"></script>
		<script src="/assets/bootstrap4/js/popper.min.js"></script>
		<script src="/assets/bootstrap4/js/bootstrap.min.js"></script>
		<script src="/assets/js/moment.min.js"></script>
		<script src="/assets/js/moment-timezone.min.js"></script>
		<script src="/assets/js/moment-timezone-with-data.min.js"></script>
		<script src="/assets/js/countdowntime.js"></script>
		<script>
			$(document).ready(function(){
				$('.counter').countdown100({
					endtimeYear: 0,
					endtimeMonth: 0,
					endtimeDate: 28,
					endtimeHours: 17,
					endtimeMinutes: 0,
					endtimeSeconds: 0,
					timeZone: ""
				});
			});
		</script>
	</body>
</html>