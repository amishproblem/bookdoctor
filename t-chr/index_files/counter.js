var yandex = {
    "v1": 50944853,
    "v1c": 54810637,
    "v2": 50947139,
    "v3": 50947454,
    "v4": 50945144,
    "v5": 50945963,
    "v6": 50945516,
    "v6o": 50946902,
    "v7": 50946356,
    "v8": 50946602,
    "v9": 50946518,
    "v9n": 50946731,
    "v9nv": 50946458,
    "v9v": 50946176,
    "v10": 49118437,
    "v10c": 50946992,
    "v11": 50947244,
    // "v12":49118437,
    "v13": 50947535,
    "v14": 50946281,
    "v14v": 50946686,
    "v15": 50945273,
    "v16": 50946053,
    "v17": 50946827,
    "v18": 50947088,
    "v19": 50947415,
    "v20": 50945015,
    "v21": 50945573,
    "v22": 50946311,
    "v23": 50946557,
    "v24": 50946749,
    "v25": 50946095,
    "v26": 50946419,
    "v27": 50946395,
    // "v28":49118437,
    "v29": 50946650,
    // "v30":49118437,
    "v31": 50946797,
    "v32": 50947031,
    "v33": 50947283,
    "v34": 50947352,
    // "v35":49118437,
    "v36": 51540881,
    "v37": 51540851,
    "v38": 51540830,
    "v39": 51540908,
    "v40": 51540938,
    "v41": 51540950,
    // "v42":49118437,
    "v43": 50946011,
    "v44": 50946959,
    "v45": 50947154,
    "v46": 50947508,
    "v47": 50947577,
    "v48": 50945387,
    "v49-kz": 50944793,
    "v50": 50946857,
    // "v51":49118437,
    "v52": 50947112,
    "v53": 51540977,
    "v54": 54810640,
    "v55": 51668975,
    "v56": 51669029,
    "v57": 51669125,
    "v58-kz": 54810643,
    "v59": 54810646,
    "v60": 54810649
};

function $_GET(key) {
    var s = window.location.search;
    s = s.match(new RegExp(key + '=([^&=]+)'));
    return s ? s[1] : '';
}

var site = $_GET('version').replace('149', '')

if (!site) {
    var site = document.location.pathname
        .replace(/\/+/g, '')
        .replace('index.html', '')
        .replace('index2.html', '')
        .replace('prelp', '')
        .replace('confirm.html', '')
        .replace('149', '');
}

yandexId = yandex[site];

(function(d, w, c, id) {
    (w[c] = w[c] || []).push(function() {
        try {
            var n = 'yaCounter' + id;
            w.n = new Ya.Metrika({
                id: id,
                clickmap: true,
                trackLinks: true,
                accurateTrackBounce: true,
                webvisor: true
            });
        } catch (e) {}
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function() { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks", yandexId);

var site = document.location.pathname
    .replace(/\/+/g, '')
    .replace('149', '')
    .replace('index.html', '')
    .replace('index2.html', '')
    .replace('confirm.html', '') || $.url().param('version');

yandexId = yandex[site];

(function(d, w, c, id) {
    (w[c] = w[c] || []).push(function() {
        try {
            var n = 'yaCounter' + id;
            w.n = new Ya.Metrika({
                id: id,
                clickmap: true,
                trackLinks: true,
                accurateTrackBounce: true,
                webvisor: true
            });
        } catch (e) {}
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function() { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks", yandexId);