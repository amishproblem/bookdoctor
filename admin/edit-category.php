<?
$service = getdata("SELECT * FROM service WHERE parent = 0 ORDER BY degree ASC");
$q = getdata("SELECT * FROM service WHERE s_id = ".$_GET["id"] . " ORDER BY degree ASC");
if (!empty($q)) {
	$q = $q[0];
}
?>
<div class="admin-<?= $page; ?>">
	<div class="container">
		<?
		if(!empty($q)) {
		?>
		<div class="row">
			<div class="col-xs-12">
				<div class="back">
					<a href="?page=category" alt="Go Back!!!"><i class="fa fa-reply"></i></a>
				</div>
				<h2 class="page"><?= r($page, "-", " "); ?></h2>
			</div>
		</div>
		<form class="category">
			<div class="row">
				<div class="col-xs-12">
    				<input type="hidden" id="table" name="table" value="<?= encrypt("service"); ?>">
    				<input type="hidden" id="s_id" name="s_id" value="<?= $q["s_id"]; ?>">
    				<?
					if(!empty($service)) {
					?>
					<div class="form-group">
						<label class="control-label">หมวดหมู่</label>
						<?
						foreach ($service as $k => $v) {
						?>
							<label class="radio-inline">
								<input type="radio" name="parent" value="<?= $v["s_id"]; ?>" <?= ($v['s_id'] == $q['parent'] ? 'checked' : ''); ?>> <?= $v["s_name_th"]; ?>
							</label>
						<?
						}
						?>
					</div>
					<?
					}
					?>
					<div class="form-group">
						<label class="control-label" for="s_name">ชื่อ EN</label>
						<input type="text" class="form-control" id="s_name" name="s_name" value="<?= htmlentities($q["s_name"]); ?>" placeholder="ชื่อ EN" required>
					</div>
    				<div class="form-group">
						<label class="control-label" for="s_name_th">ชื่อ TH</label>
						<input type="text" class="form-control" id="s_name_th" name="s_name_th" value="<?= htmlentities($q["s_name_th"]); ?>" placeholder="ชื่อ TH" required>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="-bg -margin -square-3-1 image-preview" style="background-image: url('../<?= resize("assets/images/" . $q["banner"], 1140); ?>');"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 text-center">
					<button type="button" class="btn btn-info btn-file">
						เลือกรูปภาพ <input type="file" class="-image">
					</button>
					<div class="progress-bar-upload"></div>
					<button type="button" class="btn btn-success -edit-image" data-id="<?= $q["s_id"]; ?>" data-update="banner" data-path="category">Upload</button>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="form-group">
						<label class="control-label" for="s_name_th">Url</label>
						<input type="text" class="form-control" id="url" name="url" value="<?= htmlentities($q["url"]); ?>" placeholder="Url" required>
					</div>
					<div class="form-group">
						<label class="control-label" for="degree">ลำดับ</label>
						<input type="tel" class="form-control" id="degree" name="degree" value="<?= htmlentities($q["degree"]); ?>" placeholder="ลำดับ" required>
					</div>
				</div>
			</div>
		</form>
		<button type="button" class="btn btn-success btn-fixed -update" data-path="category"><i class="fa fa-save"></i><br>Save</button>
		<?
		} else {
		?>
		<div class="row">
			<div class="col-xs-12">
				<h3>ยังไม่ได้ถูกสร้าง</h3>
			</div>
		</div>
		<?
		}
		?>
	</div>
</div>