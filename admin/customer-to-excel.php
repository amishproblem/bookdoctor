<?
include_once("../assets/php/functions.php");
include_once("../assets/php/database.php");
$start = date('Y-m-d');
$end = date('Y-m-d');

if (isset($_GET['start']) && !empty($_GET['start'])) {
	$start = $_GET['start'];
}

if (isset($_GET['end']) && !empty($_GET['end'])) {
	$end = $_GET['end'];
}

$date = "AND DATE(order_date) BETWEEN '" . $start . "' AND '" . $end . "'";

$status = '';
$order = array();
$ispaid = '';
$confirm = '';
$cancel = '';

$channels = array();
$online = '';
$callcenter = '';

if (isset($_GET['ispaid']) && !empty($_GET['ispaid'])) {
	$ispaid = $_GET['ispaid'];
	$status = "AND ispaid = 1 AND cancel = 0";
}

if (isset($_GET['confirm']) && !empty($_GET['confirm'])) {
	$confirm = $_GET['confirm'];
	$status = "AND ispaid = 0 AND confirm = 1 AND cancel = 0";
}

if (isset($_GET['cancel']) && !empty($_GET['cancel'])) {
	$cancel = $_GET['cancel'];
	$status = "AND cancel = " . $cancel;
}

if (isset($_GET['na']) && !empty($_GET['na'])) {
	$na = $_GET['na'];
	$status = "AND ispaid = 0 AND confirm = 0 AND cancel = 0";
}

if (isset($_GET['online']) && !empty($_GET['online'])) {
	$online = $_GET['online'];
	$channels[] = $online;
}

if (isset($_GET['callcenter']) && !empty($_GET['callcenter'])) {
	$callcenter = $_GET['callcenter'];
	$channels[] = $callcenter;
}

if (empty($channels)) {
	$channels = array(1,2);
}

if (isset($_GET['shop_id']) && !empty($_GET['shop_id'])) {
	$shop_id = $_GET['shop_id'];
} else {
	$shop_id = "";
}

if (isset($_GET['class_code']) && !empty($_GET['class_code'])) {
	$class_code = $_GET['class_code'];
	$class = "AND class_code = '" . $_GET['class_code'] . "'";
} else {
	$class_code = "";
	$class = "";
}

if (isset($_GET['group']) && !empty($_GET['group'])) {
	$group = $_GET['group'];
	$shop_id = "";
	$class_code = "";
	$class = "";
} else {
	$group = "";
}

if (isset($_GET['order_paid']) && !empty($_GET['order_paid'])) {
	$order_paid = $_GET['order_paid'];
	$sql = "SELECT `b`.`id` AS `id`,
		`b`.`book_id` AS `book_id`,
		`b`.`user_id` AS `user_id`,
		`u`.`user_name` AS `user_name`,
		CONVERT_TZ(`b`.`stamp`,'GMT','Asia/Bangkok') AS `stamp`
	FROM `book_status` as `b`
	LEFT JOIN `user` as u ON b.user_id = u.user_id
	WHERE
		`b`.`status` = 3
		AND DATE(`b`.`stamp`) BETWEEN '" . $start . "' AND '" . date('Y-m-d') . "'
	ORDER BY `b`.`stamp` ASC";
	$update_paid = mysqli_query($dblink, $sql);
	$$order_paids = array();
	$update_paids = array();
	while ($id = mysqli_fetch_assoc($update_paid)) {
		$order_paids[] = $id['book_id'];
		$update_paids[$id['book_id']] = $id;
	}
}

if (empty($order)) {
	if (!empty($order_paids)) {
		$order[] = 'FIELD(bid,' . implode(',', $order_paids) . ')';
	} else {
		$order[] = "id DESC";		
	}
}

$sql = "SELECT *
FROM `customer`
WHERE `id` > 0
	$date
	$status
	$class
ORDER BY " . implode(',', $order);
$customer = getdata($sql);

$filename = 'customer-' . $start . '-' . $end . '.xls';

header("Content-Type: application/x-msexcel; name=\"$filename\"");
header("Content-Disposition: inline; filename=\"$filename\"");
header("Pragma:no-cache");
?>
<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv=Content-Type content="text/html; charset=utf-8">
	<head>
	<body>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Name</th>
					<th scope="col">Phone</th>
					<th scope="col">Product</th>
					<th scope="col">Date</th>
					<th scope="col">IP</th>
					<th scope="col">Refer</th>
				</tr>
			</thead>
			<tbody>
				<?
				if (!empty($customer)) {
					foreach ($customer as $v) {
					?>
						<tr>
							<th scope="row"><?= $v['id']; ?></th>
							<td><?= $v['name']; ?></td>
							<td><?= $v['phone']; ?></td>
							<td><?= $v['product']; ?></td>
							<td><?= $v['order_date']; ?></td>
							<td><?= $v['ip']; ?></td>
							<td><?= $v['reference']; ?></td>
						</tr>
					<?
					}
				} else {
				?>
					<tr>
						<td colspan="7">No Data</td>
					</tr>
				<?
				}
				?>
			</tbody>
		</table>
	</body>
</html>