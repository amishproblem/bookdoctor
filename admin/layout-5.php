<div class="block">
	<div class="del"><i class="fa fa-times"></i></div>
	<div class="row">
		<div class="col-xs-12">
			<h3 class="text-center">Layout 4</h3>
		</div>
		<div class="col-xs-6">
			<div class="-block">
				<div class="-bg -square-16-9 preview-block preview-1" data-list="1"></div>
				<div class="img-name">
					<input type="text" class="form-control" placeholder="ชื่อรูป">
				</div>
			</div>
			<div class="block-btn text-center">
				<button type="button" class="btn btn-info margin-bottom-12 btn-file">
					<i class="fa fa-picture-o"></i> เลือกรูปภาพ 1<input type="file" class="browse-file-image file-1" data-list="1">
				</button>
			</div>
		</div>
		<div class="col-xs-3">
			<div class="-block">
				<div class="-bg -square-16-9 preview-block preview-2" data-list="2"></div>
				<div class="img-name">
					<input type="text" class="form-control" placeholder="ชื่อรูป">
				</div>
			</div>
			<div class="block-btn text-center">
				<button type="button" class="btn btn-primary margin-bottom-12 btn-file">
					<i class="fa fa-picture-o"></i> เลือกรูปภาพ 2<input type="file" class="browse-file-image file-2" data-list="2">
				</button>
			</div>
		</div>
		<div class="col-xs-3">
			<div class="-block">
				<div class="-bg -square-16-9 preview-block preview-3" data-list="3"></div>
				<div class="img-name">
					<input type="text" class="form-control" placeholder="ชื่อรูป">
				</div>
			</div>
			<div class="block-btn text-center">
				<button type="button" class="btn btn-success margin-bottom-12 btn-file">
					<i class="fa fa-picture-o"></i> เลือกรูปภาพ 3<input type="file" class="browse-file-image file-3" data-list="3">
				</button>
			</div>
		</div>
		<div class="col-xs-3">
			<div class="-block">
				<div class="-bg -square-16-9 preview-block preview-4" data-list="4"></div>
				<div class="img-name">
					<input type="text" class="form-control" placeholder="ชื่อรูป">
				</div>
			</div>
			<div class="block-btn text-center">
				<button type="button" class="btn btn-warning margin-bottom-12 btn-file">
					<i class="fa fa-picture-o"></i> เลือกรูปภาพ 4<input type="file" class="browse-file-image file-4" data-list="4">
				</button>
			</div>
		</div>
		<div class="col-xs-3">
			<div class="-block">
				<div class="-bg -square-16-9 preview-block preview-5" data-list="5"></div>
				<div class="img-name">
					<input type="text" class="form-control" placeholder="ชื่อรูป">
				</div>
			</div>
			<div class="block-btn text-center">
				<button type="button" class="btn btn-danger margin-bottom-12 btn-file">
					<i class="fa fa-picture-o"></i> เลือกรูปภาพ 5<input type="file" class="browse-file-image file-5" data-list="5">
				</button>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<textarea class="form-control" name="txt" rows="3" required></textarea>
		</div>
	</div>
</div>