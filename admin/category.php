<?
$service = array();
$q = getdata("SELECT * FROM service WHERE parent = 0 ORDER BY degree ASC");
if (!empty($q)) {
	$service = $q;
	foreach ($q as $k => $v) {
		$w = getdata("SELECT * FROM service WHERE parent = " . $v['s_id'] . " ORDER BY degree ASC");
		if (!empty($w)) {
			$service[$k]['sub_category'] = $w;
		}
	}
}
?>
<div class="admin-<?= $page; ?>">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="page"><?= $page; ?> <a href="?page=add-<?= $page; ?>" class="btn btn-info pull-right"><i class="fa fa-plus"></i></a></h2>
			</div>
		</div>
		<?
		if(!empty($service)) {
		?>
		<div class="row">
			<div class="col-xs-12">
				<table class="table table-responsive table-bordered table-striped">
					<thead>
						<tr>
							<th>ชื่อ EN</th>
							<th>ชื่อ TH</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<?
					foreach ($service as $k => $v) {
					?>
						<tr>
							<td><?= $v["s_name"]; ?></td>
							<td><?= $v["s_name_th"]; ?></td>
							<td class="text-center"><a href="?page=edit-<?= $page; ?>&id=<?= $v["s_id"]; ?>" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a></td>
						</tr>
						<?
						if (!empty($v['sub_category'])) {
							foreach ($v['sub_category'] as $kk => $vv) {
							?>
								<tr>
									<td class="sub-category">- <?= $vv["s_name"]; ?></td>
									<td><?= $vv["s_name_th"]; ?></td>
									<td class="text-center"><a href="?page=edit-<?= $page; ?>&id=<?= $vv["s_id"]; ?>" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a></td>
								</tr>
							<?
							}
						}
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
		<?
		} else {
		?>
		<div class="row">
			<div class="col-xs-12">
				<h3>ยังไม่ได้ถูกสร้าง</h3>
			</div>
		</div>
		<?
		}
		?>
	</div>
</div>
