<?
include_once("../assets/php/functions.php");
include_once("../assets/php/database.php");

if(!isset($_COOKIE['user_id']) || empty($_COOKIE['user_id'])) { 
	redirect('/admin/login.php');
}
$url = parse_url($_SERVER['REQUEST_URI']);
$path = explode('/', $url['path']);
$req = array_values(array_filter($path));
if (empty($req)) {
	$req[0] = '';
}
$q = implode('/', $req);
$url = '/' . $q;
$f = array();
foreach ($_GET as $k => $v) {
	if ($k != 'search') {
		array_push($f, $k . '=' . $v);
	}
}
$f = implode('&', $f);

$start = date('Y-m-d');
$end = date('Y-m-d');

if (isset($_GET['start']) && !empty($_GET['start'])) {
	$start = $_GET['start'];
}

if (isset($_GET['end']) && !empty($_GET['end'])) {
	$end = $_GET['end'];
}

$date = "AND DATE(order_date) BETWEEN '" . $start . "' AND '" . $end . "'";

$status = '';
$order = array();
$ispaid = '';
$confirm = '';
$cancel = '';

$channels = array();
$online = '';
$callcenter = '';

if (isset($_GET['ispaid']) && !empty($_GET['ispaid'])) {
	$ispaid = $_GET['ispaid'];
	$status = "AND ispaid = 1 AND cancel = 0";
}

if (isset($_GET['confirm']) && !empty($_GET['confirm'])) {
	$confirm = $_GET['confirm'];
	$status = "AND ispaid = 0 AND confirm = 1 AND cancel = 0";
}

if (isset($_GET['cancel']) && !empty($_GET['cancel'])) {
	$cancel = $_GET['cancel'];
	$status = "AND cancel = " . $cancel;
}

if (isset($_GET['na']) && !empty($_GET['na'])) {
	$na = $_GET['na'];
	$status = "AND ispaid = 0 AND confirm = 0 AND cancel = 0";
}

if (isset($_GET['online']) && !empty($_GET['online'])) {
	$online = $_GET['online'];
	$channels[] = $online;
}

if (isset($_GET['callcenter']) && !empty($_GET['callcenter'])) {
	$callcenter = $_GET['callcenter'];
	$channels[] = $callcenter;
}

if (empty($channels)) {
	$channels = array(1,2);
}

if (isset($_GET['shop_id']) && !empty($_GET['shop_id'])) {
	$shop_id = $_GET['shop_id'];
} else {
	$shop_id = "";
}

if (isset($_GET['class_code']) && !empty($_GET['class_code'])) {
	$class_code = $_GET['class_code'];
	$class = "AND class_code = '" . $_GET['class_code'] . "'";
} else {
	$class_code = "";
	$class = "";
}

if (isset($_GET['group']) && !empty($_GET['group'])) {
	$group = $_GET['group'];
	$shop_id = "";
	$class_code = "";
	$class = "";
} else {
	$group = "";
}

if (isset($_GET['order_paid']) && !empty($_GET['order_paid'])) {
	$order_paid = $_GET['order_paid'];
	$sql = "SELECT `b`.`id` AS `id`,
		`b`.`book_id` AS `book_id`,
		`b`.`user_id` AS `user_id`,
		`u`.`user_name` AS `user_name`,
		CONVERT_TZ(`b`.`stamp`,'GMT','Asia/Bangkok') AS `stamp`
	FROM `book_status` as `b`
	LEFT JOIN `user` as u ON b.user_id = u.user_id
	WHERE
		`b`.`status` = 3
		AND DATE(`b`.`stamp`) BETWEEN '" . $start . "' AND '" . date('Y-m-d') . "'
	ORDER BY `b`.`stamp` ASC";
	$update_paid = mysqli_query($dblink, $sql);
	$$order_paids = array();
	$update_paids = array();
	while ($id = mysqli_fetch_assoc($update_paid)) {
		$order_paids[] = $id['book_id'];
		$update_paids[$id['book_id']] = $id;
	}
}

if (empty($order)) {
	if (!empty($order_paids)) {
		$order[] = 'FIELD(bid,' . implode(',', $order_paids) . ')';
	} else {
		$order[] = "id DESC";		
	}
}

$sql = "SELECT *
FROM `customer`
WHERE `id` > 0
	$date
	$status
	$class
ORDER BY " . implode(',', $order);
$customer = getdata($sql);

?>
	
<!DOCTYPE html>
<html lang="th">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

		<link rel="stylesheet" href="../assets/fontawesome/css/all.css">
		<link rel="stylesheet" href="../assets/css/datepicker.css">
		<link rel="stylesheet" href="../assets/css/admin.css">

		<title>Admin MMT</title>
		<style>
			.table {
				table-layout: fixed;
			}
			.table td, .table th {
				word-break: break-all;
				word-wrap: break-word;
			}
		</style>
	</head>
	<body>
		<div class="wrap py-5">
			<div class="container">
				<div class="filter">
					<div class="row">
						<div class="col-md-6">
							<legend class="mb-4 pb-1 text-center">Start</legend>
							<div class="datatable">
								<div class="table-wrapper">
									<div class="form-group datetime">
										<div class="datepicker start" data-name="start" data-dowd="[]" data-today-start="0" data-date-start="<?= date('Y-01-01', strtotime('-6 year')); ?>" data-select="<?= $start; ?>"></div>
										<input class="param" type="hidden" data-name="start" name="start" value="">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<legend class="mb-4 pb-1 text-center">End</legend>
							<div class="datatable">
								<div class="table-wrapper">
									<div class="form-group datetime">
										<div class="datepicker end" data-name="end" data-dowd="[]" data-today-start="0" data-date-start="<?= date('Y-01-01', strtotime('-6 year')); ?>" data-select="<?= $end; ?>"></div>
										<input class="param" type="hidden" data-name="end" name="end" value="">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<!-- <div class="form-group">
								<label for="product">Product</label>
								<select class="form-control" id="exampleFormControlSelect1">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div> -->
						</div>
						<div class="col-md-6 text-right">
							<a href="/admin/customer-to-excel.php?<?= $f; ?>" class="btn btn-outline-success"><i class="far fa-file-excel"></i></a>
							<a href="<?= $url; ?>" class="btn btn-outline-dark check-param"><i class="fas fa-calculator"></i></a>
							<a href="<?= $url; ?>" class="btn btn-outline-danger"><i class="fas fa-eraser"></i></a>
						</div>
					</div>
				</div>
				<div class="result py-5">
					<table class="table table-striped">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Name</th>
								<th scope="col">Phone</th>
								<th scope="col">Product</th>
								<th scope="col">Date</th>
								<th scope="col">IP</th>
								<th scope="col">Refer</th>
							</tr>
						</thead>
						<tbody>
							<?
							if (!empty($customer)) {
								foreach ($customer as $v) {
								?>
									<tr>
										<th scope="row"><?= $v['id']; ?></th>
										<td><?= $v['name']; ?></td>
										<td><?= $v['phone']; ?></td>
										<td><?= $v['product']; ?></td>
										<td><?= $v['order_date']; ?></td>
										<td><?= $v['ip']; ?></td>
										<td><?= $v['reference']; ?></td>
									</tr>
								<?
								}
							} else {
							?>
								<tr>
									<td colspan="7">No Data</td>
								</tr>
							<?
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
		<script src="../assets/js/datepicker.js"></script>
		<script src="../assets/js/main.js"></script>
	</body>
</html>