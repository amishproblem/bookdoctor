<?
$service = getdata("SELECT * FROM service WHERE parent = 0 ORDER BY degree ASC");
?>
<div class="admin-<?= $page; ?>">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="back">
					<a href="?page=category" alt="Go Back!!!"><i class="fa fa-reply"></i></a>
				</div>
				<h2 class="page"><?= r($page, "-", " "); ?></h2>
			</div>
		</div>
		<form class="category">
			<div class="row">
				<div class="col-xs-12">
    				<input type="hidden" id="table" name="table" value="<?= encrypt("service"); ?>">
    				<?
					if(!empty($service)) {
					?>
					<div class="form-group">
						<label class="control-label">หมวดหมู่หลัก</label>
						<?
						foreach ($service as $k => $v) {
						?>
							<label class="radio-inline">
								<input type="radio" name="parent" value="<?= $v["s_id"]; ?>"> <?= $v["s_name_th"]; ?>
							</label>
						<?
						}
						?>
					</div>
					<?
					}
					?>
    				<div class="form-group">
						<label class="control-label" for="s_name">ชื่อ EN</label>
						<input type="text" class="form-control" id="s_name" name="s_name" value="" placeholder="ชื่อ EN" required>
					</div>
    				<div class="form-group">
						<label class="control-label" for="s_name_th">ชื่อ TH</label>
						<input type="text" class="form-control" id="s_name_th" name="s_name_th" value="" placeholder="ชื่อ TH" required>
					</div>
    			</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="-bg -margin -square-3-1 image-preview"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 text-center">
					<button type="button" class="btn btn-info btn-file">
						<i class="fa fa-picture-o"></i> เลือกรูปภาพ <input type="file" class="-image">
					</button>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="form-group">
						<label class="control-label" for="url">Url</label>
						<input type="text" class="form-control" id="url" name="url" value="" placeholder="Url" required>
					</div>
					<div class="form-group">
						<label class="control-label" for="degree">ลำดับ</label>
						<input type="text" class="form-control" id="degree" name="degree" value="" placeholder="ลำดับ" required>
					</div>
				</div>
			</div>
		</form>
		<button type="button" class="btn btn-success btn-fixed no-border-radius -insert" data-update="banner" data-path="category"><i class="fa fa-save"></i><br>Add</button>
	</div>
</div>