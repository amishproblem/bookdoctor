<?
$service = getdata("SELECT * FROM service ORDER BY degree ASC");
?>
<div class="admin-<?= $page; ?>">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="back">
					<a href="?page=service" alt="Go Back!!!"><i class="fa fa-reply"></i></a>
				</div>
				<h2 class="page"><?= r($page, "-", " "); ?></h2>
			</div>
		</div>
		<form class="article">
			<div class="row">
				<div class="col-xs-12">
    				<input type="hidden" id="table" name="table" value="<?= encrypt("article"); ?>">
    				<!-- <input type="hidden" id="a_id" name="a_id" value=""> -->
    				<input type="hidden" id="a_type" name="a_type" value="2">
    				<?
					if(!empty($service)) {
					?>
					<div class="form-group">
						<label class="control-label">หมวดหมู่</label>
						<?
						foreach ($service as $k => $v) {
						?>
							<label class="radio-inline">
								<input type="radio" name="s_id" value="<?= $v["s_id"]; ?>" <?= ($k == 0 ? 'checked' : ''); ?>> <?= $v["s_name_th"]; ?>
							</label>
						<?
						}
						?>
					</div>
					<?
					}
					?>
					<div class="form-group">
						<label class="control-label" for="seo_keyword">คีย์เวิร์ด</label>
						<input type="text" class="form-control" id="seo_keyword" name="seo_keyword" value="" placeholder="SEO Keyword" required>
					</div>
    				<div class="form-group">
						<label class="control-label" for="title">ชื่อ</label>
						<input type="text" class="form-control" id="title" name="title" value="" placeholder="Title" required>
					</div>
					<div class="form-group">
						<label class="control-label" for="url">Url</label>
						<input type="text" class="form-control" id="url" name="url" value="" placeholder="Url" required>
					</div>
					<div class="form-group">
						<label class="control-label" for="description">คำอธิบาย</label>
						<textarea class="form-control wysihtml5" id="description" name="description" rows="5" required></textarea>
					</div>
					<div class="form-group">
						<label class="control-label" for="public_date">วันที่เริ่มแสดงผล</label>
						<input type="text" class="form-control date" id="public_date" name="public_date" data-date-format="dd-mm-yyyy" value="<?= date("d-m-Y"); ?>" required>
					</div>
					<div class="form-group">
						<label class="control-label">สถานะ</label>
						<label class="radio-inline">
							<input type="radio" name="active" value="1" checked> แสดง
						</label>
						<label class="radio-inline">
							<input type="radio" name="active" value="0"> ซ่อน
						</label>
					</div>
					<div class="form-group">
						<label class="control-label">แสดงหน้าหลัก</label>
						<label class="radio-inline">
							<input type="radio" name="show_homepage" value="1"> แสดง
						</label>
						<label class="radio-inline">
							<input type="radio" name="show_homepage" value="0" checked> ซ่อน
						</label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="-block">
						<div class="-bg -margin -square-16-9 image-preview"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 text-center">
					<button type="button" class="btn btn-info btn-file">
						<i class="fa fa-picture-o"></i> เลือกรูปภาพ <input type="file" class="add-file-image-noid">
					</button>
				</div>
			</div>
		</form>
		<form class="article-details">
			<div class="article-detail">
				
			</div>
			<div class="row">
				<div class="col-sm-3 col-xs-6">
					<table class="table table-bordered layout" data-value="1">
						<tbody>
							<tr>
								<td class="height-100"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-sm-3 col-xs-6">
					<table class="table table-bordered layout" data-value="2">
						<tbody>
							<tr>
								<td class="height-100"></td>
								<td class="height-100"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-sm-3 col-xs-6">
					<table class="table table-bordered layout" data-value="3">
						<tbody>
							<tr>
								<td class="height-100" rowspan="3"></td>
							</tr>
							<tr>
								<td class="height-50"></td>
							</tr>
							<tr>
								<td class="height-50"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-sm-3 col-xs-6">
					<table class="table table-bordered layout" data-value="4">
						<tbody>
							<tr>
								<td class="height-50"></td>
								<td class="height-50"></td>
							</tr>
							<tr>
								<td class="height-50"></td>
								<td class="height-50"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-sm-3 col-xs-6">
					<table class="table table-bordered layout" data-value="5">
						<tbody>
							<tr>
								<td class="height-100" rowspan="3"></td>
							</tr>
							<tr>
								<td class="height-50"></td>
								<td class="height-50"></td>
							</tr>
							<tr>
								<td class="height-50"></td>
								<td class="height-50"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<button type="button" class="btn btn-success btn-fixed no-border-radius add-btn-all" data-update="image" data-path="service"><i class="fa fa-save"></i><br>Add</button>
	</div>
</div>