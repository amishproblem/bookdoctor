<?
$service = getdata("SELECT * FROM service ORDER BY degree ASC");
?>
<div class="admin-<?= $page; ?>">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="back">
					<a href="?page=promotion" alt="Go Back!!!"><i class="fa fa-reply"></i></a>
				</div>
				<h2 class="page"><?= r($page, "-", " "); ?></h2>
			</div>
		</div>
		<form class="category">
			<div class="row">
				<div class="col-xs-12">
    				<input type="hidden" id="table" name="table" value="<?= encrypt("promotion"); ?>">
    				<?
					if(!empty($service)) {
					?>
					<div class="form-group">
						<label class="control-label">หมวดหมู่</label>
						<?
						foreach ($service as $k => $v) {
						?>
							<label class="radio-inline">
								<input type="radio" name="s_id" value="<?= $v["s_id"]; ?>" <?= ($k == 0 ? 'checked' : ''); ?>> <?= $v["s_name_th"]; ?>
							</label>
						<?
						}
						?>
					</div>
					<?
					}
					?>
					<div class="form-group">
						<label class="control-label" for="seo_keyword">คีย์เวิร์ด</label>
						<input type="text" class="form-control" id="seo_keyword" name="seo_keyword" value="" placeholder="SEO Keyword" required>
					</div>
    				<div class="form-group">
						<label class="control-label" for="p_name_th">ชื่อ</label>
						<input type="text" class="form-control" id="p_name_th" name="p_name_th" value="" placeholder="ชื่อ" required>
					</div>
					<div class="form-group">
						<label class="control-label" for="p_name">Url</label>
						<input type="text" class="form-control" id="p_name" name="p_name" value="" placeholder="Url" required>
					</div>
					<div class="form-group">
						<label class="control-label" for="description">คำอธิบาย</label>
						<textarea class="form-control wysihtml5" id="description" name="description" rows="5" required></textarea>
					</div>
					<div class="form-group">
						<label class="control-label" for="start_date">วันที่เริ่ม</label>
						<input type="text" class="form-control date" id="start_date" name="start_date" data-date-format="dd-mm-yyyy" value="<?= date("d-m-Y"); ?>" required>
					</div>
					<div class="form-group">
						<label class="control-label" for="end_date">วันที่หมด</label>
						<input type="text" class="form-control date" id="end_date" name="end_date" data-date-format="dd-mm-yyyy" value="<?= date("d-m-Y"); ?>" required>
					</div>
    			</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="-block">
						<div class="-bg -margin -square-16-9 image-preview"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 text-center">
					<button type="button" class="btn btn-info btn-file">
						<i class="fa fa-picture-o"></i> เลือกรูปภาพ <input type="file" class="-image">
					</button>
				</div>
			</div>
		</form>
		<button type="button" class="btn btn-success btn-fixed no-border-radius -insert" data-update="image" data-path="promotion"><i class="fa fa-save"></i><br>Add</button>
	</div>
</div>