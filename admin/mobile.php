<?
$banner = array();
$dir = "../assets/images/mobile/slides";
if (is_dir($dir)) {
	$files = scandir($dir, 1);
	if(!empty($files)) {
		$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
		foreach ($files as $k => $v) {
			if (is_file($dir . "/" . $v)) {
				$checkType = exif_imagetype($dir . "/" . $v);
				if (in_array($checkType, $allowedTypes)) {
					array_push($banner, $v);
				}
			}
		}
	}
}
?>
<div class="admin-<?= $page; ?>">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="page">แบนเนอร์ <?= r($page, "-", " "); ?> <a class="btn btn-info pull-right -banner-layout-mobile"><i class="fa fa-plus"></i></a></h2>
			</div>
		</div>
		<form class="article">
		<?
		if(!empty($banner)) {
		?>
			<?
			foreach ($banner as $k => $v) {
			?>
				<div class="row">
					<div class="col-sm-offset-4 col-sm-4 col-xs-12 text-center">
						<div class="-bg -margin -square-3-4 image-preview" style="background-image: url('../<?= resize("assets/images/mobile/slides/" . $v, 1920); ?>');"></div>
						<button type="button" class="btn btn-info btn-file">
							เลือกรูปภาพ <input type="file" class="-banner-image">
						</button>
						<div class="progress-bar-upload"></div>
						<button type="button" class="btn btn-success -upload-banner-mobile" data-name="<?= $v; ?>">Upload</button>
					</div>
				</div>
			<?
			}
			?>
		<?
		}
		?>
		</form>
	</div>
</div>