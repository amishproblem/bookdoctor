<?
$service = getdata("SELECT * FROM service ORDER BY degree ASC");
$q = getdata("SELECT * FROM promotion WHERE p_id = ".$_GET["id"]);
if (!empty($q)) {
	$q = $q[0];
}
?>
<div class="admin-<?= $page; ?>">
	<div class="container">
		<?
		if(!empty($q)) {
		?>
		<div class="row">
			<div class="col-xs-12">
				<div class="back">
					<a href="?page=promotion" alt="Go Back!!!"><i class="fa fa-reply"></i></a>
				</div>
				<h2 class="page"><?= r($page, "-", " "); ?></h2>
			</div>
		</div>
		<form class="promotion">
			<div class="row">
				<div class="col-xs-12">
    				<input type="hidden" id="table" name="table" value="<?= encrypt("promotion"); ?>">
    				<input type="hidden" id="p_id" name="p_id" value="<?= $q["p_id"]; ?>">
    				<?
					if(!empty($service)) {
					?>
					<div class="form-group">
						<label class="control-label">หมวดหมู่</label>
						<?
						foreach ($service as $k => $v) {
						?>
							<label class="radio-inline">
								<input type="radio" name="s_id" value="<?= $v["s_id"]; ?>" <?= ($v['s_id'] == $q['s_id'] ? 'checked' : ''); ?>> <?= $v["s_name_th"]; ?>
							</label>
						<?
						}
						?>
					</div>
					<?
					}
					?>
					<div class="form-group">
						<label class="control-label" for="seo_keyword">คีย์เวิร์ด</label>
						<input type="text" class="form-control" id="seo_keyword" name="seo_keyword" value="<?= htmlentities($q["seo_keyword"]); ?>" placeholder="SEO Keyword" required>
					</div>
    				<div class="form-group">
						<label class="control-label" for="p_name_th">ชื่อ TH</label>
						<input type="text" class="form-control" id="p_name_th" name="p_name_th" value="<?= htmlentities($q["p_name_th"]); ?>" placeholder="ชื่อ" required>
					</div>
					<div class="form-group">
						<label class="control-label" for="p_name">Url</label>
						<input type="text" class="form-control" id="p_name" name="p_name" value="<?= htmlentities($q["p_name"]); ?>" placeholder="Url" required>
					</div>
					<div class="form-group">
						<label class="control-label" for="description">คำอธิบาย</label>
						<textarea class="form-control wysihtml5" id="description" name="description" rows="5" required><?= $q["description"]; ?></textarea>
					</div>
					<div class="form-group">
						<label class="control-label" for="start_date">วันที่เริ่ม</label>
						<input type="text" class="form-control date" id="start_date" name="start_date" data-date-format="dd-mm-yyyy" value="<?= date("d-m-Y", strtotime($q["start_date"])); ?>" required>
					</div>
					<div class="form-group">
						<label class="control-label" for="end_date">วันที่หมด</label>
						<input type="text" class="form-control date" id="end_date" name="end_date" data-date-format="dd-mm-yyyy" value="<?= date("d-m-Y", strtotime($q["end_date"])); ?>" required>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="-block">
						<div class="-bg -margin image-preview" style="background-image: url('../<?= resize("assets/images/" . $q["image"], 640, 640); ?>');"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 text-center">
					<button type="button" class="btn btn-info btn-file">
						เลือกรูปภาพ <input type="file" class="-image">
					</button>
					<div class="progress-bar-upload"></div>
					<button type="button" class="btn btn-success -edit-image" data-id="<?= $q["p_id"]; ?>" data-update="image" data-path="promotion">Upload</button>
				</div>
			</div>
		</form>
		<button type="button" class="btn btn-success btn-fixed -update-promotion" data-path="promotion"><i class="fa fa-save"></i><br>Save</button>
		<?
		} else {
		?>
		<div class="row">
			<div class="col-xs-12">
				<h3>ยังไม่ได้ถูกสร้าง</h3>
			</div>
		</div>
		<?
		}
		?>
	</div>
</div>