<?
$q = getdata("SELECT * FROM promotion ORDER BY p_id DESC");
?>
<div class="admin-<?= $page; ?>">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="page"><?= $page; ?> <a href="?page=add-<?= $page; ?>" class="btn btn-info pull-right"><i class="fa fa-plus"></i></a></h2>
			</div>
		</div>
		<?
		if(!empty($q)) {
		?>
		<div class="row">
			<div class="col-xs-12">
				<table class="table table-responsive table-bordered table-striped">
					<thead>
						<tr>
							<th>ชื่อ</th>
							<th>เริ่ม</th>
							<th>สิ้นสุด</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<?
					foreach ($q as $k => $v) {
					?>
						<tr>
							<td><?= $v["p_name_th"]; ?></td>
							<td><?= date("j", strtotime($v["start_date"])); ?> <?= monthth(date("n", strtotime($v["start_date"]))); ?> <?= date("Y", strtotime($v["start_date"]))+543; ?></td>
							<td><?= date("j", strtotime($v["end_date"])); ?> <?= monthth(date("n", strtotime($v["end_date"]))); ?> <?= date("Y", strtotime($v["end_date"]))+543; ?></td>
							<td class="text-center"><a href="?page=edit-<?= $page; ?>&id=<?= $v["p_id"]; ?>" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a></td>
						</tr>
					<?
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
		<?
		} else {
		?>
		<div class="row">
			<div class="col-xs-12">
				<h3>ยังไม่ได้ถูกสร้าง</h3>
			</div>
		</div>
		<?
		}
		?>
	</div>
</div>
