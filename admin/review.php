<?
$q = getdata("SELECT * FROM review ORDER BY r_id DESC");
?>
<div class="admin-<?= $page; ?>">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="page"><?= $page; ?> <a href="?page=add-<?= $page; ?>" class="btn btn-info pull-right"><i class="fa fa-plus"></i></a></h2>
			</div>
		</div>
		<?
		if(!empty($q)) {
		?>
		<div class="row">
			<div class="col-xs-12">
				<table class="table table-responsive table-bordered table-striped">
					<thead>
						<tr>
							<th>ชื่อ</th>
							<th>คีย์เวิร์ด</th>
							<th>วันที่</th>
							<th>สถานะ</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<?
					foreach ($q as $k => $v) {
					?>
						<tr>
							<td>(<?= $v["r_id"]; ?>) <?= $v["title"]; ?><a href="<?= URL_; ?>article/<?= $v['r_id']; ?>/<?= urlencode(strtolower(trim($v['url']))); ?>?is_check=1" class="pull-right" target="_blank"><i class="fa fa-eye text-right"></i></a></td>
							<td><?= $v["seo_keyword"]; ?></td>
							<td><?= dateth($v["stamp"]); ?></td>
							<td>
								<span class="btn btn-sm btn-<?= $v["active"] ? "success" : "danger"; ?>">A</span>
								<span class="btn btn-sm btn-<?= $v["public_date"] <= date("Y-m-d") ? "success" : "danger"; ?>">P</span>
							</td>
							<td class="text-center"><a href="?page=edit-<?= $page; ?>&id=<?= $v["r_id"]; ?>" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a></td>
						</tr>
					<?
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
		<?
		} else {
		?>
		<div class="row">
			<div class="col-xs-12">
				<h3>ยังไม่ได้ถูกสร้าง</h3>
			</div>
		</div>
		<?
		}
		?>
	</div>
</div>
