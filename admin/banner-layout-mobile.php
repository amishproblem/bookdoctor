<div class="-banner-block">
	<div class="del"><i class="fa fa-times"></i></div>
	<div class="row">
		<div class="col-sm-offset-4 col-sm-4 col-xs-12 text-center">
			<div class="-bg -margin -square-3-4 image-preview"></div>
			<button type="button" class="btn btn-info btn-file">
				เลือกรูปภาพ <input type="file" class="-banner-image">
			</button>
			<div class="progress-bar-upload"></div>
			<button type="button" class="btn btn-success -upload-banner-mobile">Upload</button>
		</div>
	</div>
</div>