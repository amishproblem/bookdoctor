<?
$service = getdata("SELECT * FROM service ORDER BY degree ASC");
$q = getdata("SELECT * FROM article WHERE a_id = ".$_GET["id"]);
$d = getdata("SELECT * FROM article_detail WHERE a_id = ".$_GET["id"]." ORDER BY d_id ASC");
$q = $q[0];
?>
<div class="admin-<?= $page; ?>">
	<div class="container">
		<?
		if(!empty($q)) {
		?>
		<div class="row">
			<div class="col-xs-12">
				<div class="back">
					<a href="?page=activity" alt="Go Back!!!"><i class="fa fa-reply"></i></a>
				</div>
				<h2 class="page"><?= r($page, "-", " "); ?></h2>
			</div>
		</div>
		<form class="article">
			<div class="row">
				<div class="col-xs-12">
    				<input type="hidden" id="table" name="table" value="<?= encrypt("article"); ?>">
    				<input type="hidden" id="a_id" name="a_id" value="<?= $q["a_id"]; ?>">
    				<?
					if(!empty($service)) {
					?>
					<div class="form-group">
						<label class="control-label">หมวดหมู่</label>
						<?
						foreach ($service as $k => $v) {
						?>
							<label class="radio-inline">
								<input type="radio" name="s_id" value="<?= $v["s_id"]; ?>" <?= ($v['s_id'] == $q['s_id'] ? 'checked' : ''); ?>> <?= $v["s_name_th"]; ?>
							</label>
						<?
						}
						?>
					</div>
					<?
					}
					?>
					<div class="form-group">
						<label class="control-label" for="seo_keyword">คีย์เวิร์ด</label>
						<input type="text" class="form-control" id="seo_keyword" name="seo_keyword" value="<?= htmlentities($q["seo_keyword"]); ?>" placeholder="SEO Keyword" required>
					</div>
    				<div class="form-group">
						<label class="control-label" for="title">ชื่อ</label>
						<input type="text" class="form-control" id="title" name="title" value="<?= htmlentities($q["title"]); ?>" placeholder="Title" required>
					</div>
					<div class="form-group">
						<label class="control-label" for="url">Url</label>
						<input type="text" class="form-control" id="url" name="url" value="<?= htmlentities($q["url"]); ?>" placeholder="Url" required>
					</div>
					<div class="form-group">
						<label class="control-label" for="description">คำอธิบาย</label>
						<textarea class="form-control wysihtml5" id="description" name="description" rows="5" required><?= $q["description"]; ?></textarea>
					</div>
					<div class="form-group">
						<label class="control-label" for="public_date">วันที่เริ่มแสดงผล</label>
						<input type="text" class="form-control date" id="public_date" name="public_date" data-date-format="dd-mm-yyyy" value="<?= date("d-m-Y", strtotime($q["public_date"])); ?>" required>
					</div>
					<div class="form-group">
						<label class="control-label">สถานะ</label>
						<label class="radio-inline">
							<input type="radio" name="active" value="1" <?= ($q["active"] == 1 ? "checked" : "") ?>> แสดง
						</label>
						<label class="radio-inline">
							<input type="radio" name="active" value="0" <?= ($q["active"] == 0 ? "checked" : "") ?>> ซ่อน
						</label>
					</div>
					<div class="form-group">
						<label class="control-label">แสดงหน้าหลัก</label>
						<label class="radio-inline">
							<input type="radio" name="show_homepage" value="1" <?= ($q["show_homepage"] == 1 ? "checked" : "") ?>> แสดง
						</label>
						<label class="radio-inline">
							<input type="radio" name="show_homepage" value="0" <?= ($q["show_homepage"] == 0 ? "checked" : "") ?>> ซ่อน
						</label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="-block">
						<div class="-bg -margin -square-16-9 image-preview" style="background-image: url('../<?= resize("assets/images/" . $q["image"], 800); ?>');"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 text-center">
					<button type="button" class="btn btn-info btn-file">
						เลือกรูปภาพ <input type="file" class="edit-file-image">
					</button>
					<div class="progress-bar-upload"></div>
					<button type="button" class="btn btn-success edit-file-submit" data-id="<?= $q["a_id"]; ?>" data-update="image" data-path="activity">Upload</button>
				</div>
			</div>
		</form>
		<form class="article-details">
			<div class="article-detail-show">
			<?
			foreach ($d as $k => $v) {
				if(!empty($v["image1"]) && !empty($v["image2"]) && !empty($v["image3"]) && !empty($v["image4"]) && !empty($v["image5"])) {
				?>
				<div class="row">
					<input type="hidden" name="d_id" value="<?= $v["d_id"] ?>">
					<div class="col-xs-6">
						<div class="img-block">
							<img src='../<?= resize("assets/images/" . $v["image1"], 800); ?>' alt="<?= $v["alt1"] ?>" title="<?= $v["alt1"] ?>">
							<input type="file" class="edit-file-image-detail hidden">
							<div class="progress-bar-upload"></div>
							<button type="button" class="btn btn-success edit-file-submit-detail" data-table="<?= encrypt("article_detail"); ?>" data-name="<?= $v["image1"]; ?>" data-did="<?= $v["d_id"]; ?>" data-aid="<?= $q["a_id"]; ?>" data-update="image1" data-path="activity">Upload</button>
						</div>
						<div class="img-name">
							<input type="text" class="form-control changer" name="alt1" placeholder="ชื่อรูป" value="<?= $v["alt1"] ?>">
						</div>
					</div>
					<div class="col-xs-3">
						<div class="img-block">
							<img src='../<?= resize("assets/images/" . $v["image2"], 800); ?>' alt="<?= $v["alt2"] ?>" title="<?= $v["alt2"] ?>">
							<input type="file" class="edit-file-image-detail hidden">
							<div class="progress-bar-upload"></div>
							<button type="button" class="btn btn-success edit-file-submit-detail" data-table="<?= encrypt("article_detail"); ?>" data-name="<?= $v["image2"]; ?>" data-did="<?= $v["d_id"]; ?>" data-aid="<?= $q["a_id"]; ?>" data-update="image2" data-path="activity">Upload</button>
						</div>
						<div class="img-name">
							<input type="text" class="form-control changer" name="alt2" placeholder="ชื่อรูป" value="<?= $v["alt2"] ?>">
						</div>
					</div>
					<div class="col-xs-3">
						<div class="img-block">
							<img src='../<?= resize("assets/images/" . $v["image3"], 800); ?>' alt="<?= $v["alt3"] ?>" title="<?= $v["alt3"] ?>">
							<input type="file" class="edit-file-image-detail hidden">
							<div class="progress-bar-upload"></div>
							<button type="button" class="btn btn-success edit-file-submit-detail" data-table="<?= encrypt("article_detail"); ?>" data-name="<?= $v["image3"]; ?>" data-did="<?= $v["d_id"]; ?>" data-aid="<?= $q["a_id"]; ?>" data-update="image3" data-path="activity">Upload</button>
						</div>
						<div class="img-name">
							<input type="text" class="form-control changer" name="alt3" placeholder="ชื่อรูป" value="<?= $v["alt3"] ?>">
						</div>
					</div>
					<div class="col-xs-3">
						<div class="img-block">
							<img src='../<?= resize("assets/images/" . $v["image4"], 800); ?>' alt="<?= $v["alt4"] ?>" title="<?= $v["alt4"] ?>">
							<input type="file" class="edit-file-image-detail hidden">
							<div class="progress-bar-upload"></div>
							<button type="button" class="btn btn-success edit-file-submit-detail" data-table="<?= encrypt("article_detail"); ?>" data-name="<?= $v["image4"]; ?>" data-did="<?= $v["d_id"]; ?>" data-aid="<?= $q["a_id"]; ?>" data-update="image4" data-path="activity">Upload</button>
						</div>
						<div class="img-name">
							<input type="text" class="form-control changer" name="alt4" placeholder="ชื่อรูป" value="<?= $v["alt4"] ?>">
						</div>
					</div>
					<div class="col-xs-3">
						<div class="img-block">
							<img src='../<?= resize("assets/images/" . $v["image5"], 800); ?>' alt="<?= $v["alt5"] ?>" title="<?= $v["alt5"] ?>">
							<input type="file" class="edit-file-image-detail hidden">
							<div class="progress-bar-upload"></div>
							<button type="button" class="btn btn-success edit-file-submit-detail" data-table="<?= encrypt("article_detail"); ?>" data-name="<?= $v["image5"]; ?>" data-did="<?= $v["d_id"]; ?>" data-aid="<?= $q["a_id"]; ?>" data-update="image5" data-path="activity">Upload</button>
						</div>
						<div class="img-name">
							<input type="text" class="form-control changer" name="alt5" placeholder="ชื่อรูป" value="<?= $v["alt5"] ?>">
						</div>
					</div>
					<div class="col-xs-12">
						<textarea class="form-control changer wysihtml5" name="txt" rows="5" required><?= $v["txt"] ?></textarea>
					</div>
				</div>
				<?
				} else if(!empty($v["image1"]) && !empty($v["image2"]) && !empty($v["image3"]) && !empty($v["image4"])) {
				?>
				<div class="row">
					<input type="hidden" name="d_id" value="<?= $v["d_id"] ?>">
					<div class="col-xs-6">
						<div class="img-block">
							<img src='../<?= resize("assets/images/" . $v["image1"], 800); ?>' alt="<?= $v["alt1"] ?>" title="<?= $v["alt1"] ?>">
							<input type="file" class="edit-file-image-detail hidden">
							<div class="progress-bar-upload"></div>
							<button type="button" class="btn btn-success edit-file-submit-detail" data-table="<?= encrypt("article_detail"); ?>" data-name="<?= $v["image1"]; ?>" data-did="<?= $v["d_id"]; ?>" data-aid="<?= $q["a_id"]; ?>" data-update="image1" data-path="activity">Upload</button>
						</div>
						<div class="img-name">
							<input type="text" class="form-control changer" name="alt1" placeholder="ชื่อรูป" value="<?= $v["alt1"] ?>">
						</div>
					</div>
					<div class="col-xs-6">
						<div class="img-block">
							<img src='../<?= resize("assets/images/" . $v["image2"], 800); ?>' alt="<?= $v["alt2"] ?>" title="<?= $v["alt2"] ?>">
							<input type="file" class="edit-file-image-detail hidden">
							<div class="progress-bar-upload"></div>
							<button type="button" class="btn btn-success edit-file-submit-detail" data-table="<?= encrypt("article_detail"); ?>" data-name="<?= $v["image2"]; ?>" data-did="<?= $v["d_id"]; ?>" data-aid="<?= $q["a_id"]; ?>" data-update="image2" data-path="activity">Upload</button>
						</div>
						<div class="img-name">
							<input type="text" class="form-control changer" name="alt2" placeholder="ชื่อรูป" value="<?= $v["alt2"] ?>">
						</div>
					</div>
					<div class="col-xs-6">
						<div class="img-block">
							<img src='../<?= resize("assets/images/" . $v["image3"], 800); ?>' alt="<?= $v["alt3"] ?>" title="<?= $v["alt3"] ?>">
							<input type="file" class="edit-file-image-detail hidden">
							<div class="progress-bar-upload"></div>
							<button type="button" class="btn btn-success edit-file-submit-detail" data-table="<?= encrypt("article_detail"); ?>" data-name="<?= $v["image3"]; ?>" data-did="<?= $v["d_id"]; ?>" data-aid="<?= $q["a_id"]; ?>" data-update="image3" data-path="activity">Upload</button>
						</div>
						<div class="img-name">
							<input type="text" class="form-control changer" name="alt3" placeholder="ชื่อรูป" value="<?= $v["alt3"] ?>">
						</div>
					</div>
					<div class="col-xs-6">
						<div class="img-block">
							<img src='../<?= resize("assets/images/" . $v["image4"], 800); ?>' alt="<?= $v["alt4"] ?>" title="<?= $v["alt4"] ?>">
							<input type="file" class="edit-file-image-detail hidden">
							<div class="progress-bar-upload"></div>
							<button type="button" class="btn btn-success edit-file-submit-detail" data-table="<?= encrypt("article_detail"); ?>" data-name="<?= $v["image4"]; ?>" data-did="<?= $v["d_id"]; ?>" data-aid="<?= $q["a_id"]; ?>" data-update="image4" data-path="activity">Upload</button>
						</div>
						<div class="img-name">
							<input type="text" class="form-control changer" name="alt4" placeholder="ชื่อรูป" value="<?= $v["alt4"] ?>">
						</div>
					</div>
					<div class="col-xs-12">
						<textarea class="form-control changer wysihtml5" name="txt" rows="5" required><?= $v["txt"] ?></textarea>
					</div>
				</div>
				<?
				} else if(!empty($v["image1"]) && !empty($v["image2"]) && !empty($v["image3"])) {
				?>
				<div class="row">
					<input type="hidden" name="d_id" value="<?= $v["d_id"] ?>">
					<div class="col-xs-6">
						<div class="img-block">
							<img src='../<?= resize("assets/images/" . $v["image1"], 800); ?>' alt="<?= $v["alt1"] ?>" title="<?= $v["alt1"] ?>">
							<input type="file" class="edit-file-image-detail hidden">
							<div class="progress-bar-upload"></div>
							<button type="button" class="btn btn-success edit-file-submit-detail" data-table="<?= encrypt("article_detail"); ?>" data-name="<?= $v["image1"]; ?>" data-did="<?= $v["d_id"]; ?>" data-aid="<?= $q["a_id"]; ?>" data-update="image1" data-path="activity">Upload</button>
						</div>
						<div class="img-name">
							<input type="text" class="form-control changer" name="alt1" placeholder="ชื่อรูป" value="<?= $v["alt1"] ?>">
						</div>
					</div>
					<div class="col-xs-6">
						<div class="img-block">
							<img src='../<?= resize("assets/images/" . $v["image2"], 800); ?>' alt="<?= $v["alt2"] ?>" title="<?= $v["alt2"] ?>">
							<input type="file" class="edit-file-image-detail hidden">
							<div class="progress-bar-upload"></div>
							<button type="button" class="btn btn-success edit-file-submit-detail" data-table="<?= encrypt("article_detail"); ?>" data-name="<?= $v["image2"]; ?>" data-did="<?= $v["d_id"]; ?>" data-aid="<?= $q["a_id"]; ?>" data-update="image2" data-path="activity">Upload</button>
						</div>
						<div class="img-name">
							<input type="text" class="form-control changer" name="alt2" placeholder="ชื่อรูป" value="<?= $v["alt2"] ?>">
						</div>
					</div>
					<div class="col-xs-6">
						<div class="img-block">
							<img src='../<?= resize("assets/images/" . $v["image3"], 800); ?>' alt="<?= $v["alt3"] ?>" title="<?= $v["alt3"] ?>">
							<input type="file" class="edit-file-image-detail hidden">
							<div class="progress-bar-upload"></div>
							<button type="button" class="btn btn-success edit-file-submit-detail" data-table="<?= encrypt("article_detail"); ?>" data-name="<?= $v["image3"]; ?>" data-did="<?= $v["d_id"]; ?>" data-aid="<?= $q["a_id"]; ?>" data-update="image3" data-path="activity">Upload</button>
						</div>
						<div class="img-name">
							<input type="text" class="form-control changer" name="alt3" placeholder="ชื่อรูป" value="<?= $v["alt3"] ?>">
						</div>
					</div>
					<div class="col-xs-12">
						<textarea class="form-control changer wysihtml5" name="txt" rows="5" required><?= $v["txt"] ?></textarea>
					</div>
				</div>
				<?
				} else if(!empty($v["image1"]) && !empty($v["image2"])) {
				?>
				<div class="row">
					<input type="hidden" name="d_id" value="<?= $v["d_id"] ?>">
					<div class="col-xs-6">
						<div class="img-block">
							<img src='../<?= resize("assets/images/" . $v["image1"], 800); ?>' alt="<?= $v["alt1"] ?>" title="<?= $v["alt1"] ?>">
							<input type="file" class="edit-file-image-detail hidden">
							<div class="progress-bar-upload"></div>
							<button type="button" class="btn btn-success edit-file-submit-detail" data-table="<?= encrypt("article_detail"); ?>" data-name="<?= $v["image1"]; ?>" data-did="<?= $v["d_id"]; ?>" data-aid="<?= $q["a_id"]; ?>" data-update="image1" data-path="activity">Upload</button>
						</div>
						<div class="img-name">
							<input type="text" class="form-control changer" name="alt1" placeholder="ชื่อรูป" value="<?= $v["alt1"] ?>">
						</div>
					</div>
					<div class="col-xs-6">
						<div class="img-block">
							<img src='../<?= resize("assets/images/" . $v["image2"], 800); ?>' alt="<?= $v["alt2"] ?>" title="<?= $v["alt2"] ?>">
							<input type="file" class="edit-file-image-detail hidden">
							<div class="progress-bar-upload"></div>
							<button type="button" class="btn btn-success edit-file-submit-detail" data-table="<?= encrypt("article_detail"); ?>" data-name="<?= $v["image2"]; ?>" data-did="<?= $v["d_id"]; ?>" data-aid="<?= $q["a_id"]; ?>" data-update="image2" data-path="activity">Upload</button>
						</div>
						<div class="img-name">
							<input type="text" class="form-control changer" name="alt2" placeholder="ชื่อรูป" value="<?= $v["alt2"] ?>">
						</div>
					</div>
					<div class="col-xs-12">
						<textarea class="form-control changer wysihtml5" name="txt" rows="5" required><?= $v["txt"] ?></textarea>
					</div>
				</div>
				<?
				} else {
				?>
				<div class="row">
					<input type="hidden" name="d_id" value="<?= $v["d_id"] ?>">
					<div class="col-sm-6 col-sm-push-3 col-xs-12">
						<div class="img-block">
							<img src='../<?= resize("assets/images/" . $v["image1"], 800); ?>' alt="<?= $v["alt1"] ?>" title="<?= $v["alt1"] ?>">
							<input type="file" class="edit-file-image-detail hidden">
							<div class="progress-bar-upload"></div>
							<button type="button" class="btn btn-success edit-file-submit-detail" data-table="<?= encrypt("article_detail"); ?>" data-name="<?= $v["image1"]; ?>" data-did="<?= $v["d_id"]; ?>" data-aid="<?= $q["a_id"]; ?>" data-update="image1" data-path="activity">Upload</button>
						</div>
						<div class="img-name">
							<input type="text" class="form-control changer" name="alt1" placeholder="ชื่อรูป" value="<?= $v["alt1"] ?>">
						</div>
					</div>
					<div class="col-xs-12">
						<textarea class="form-control changer wysihtml5" name="txt" rows="5" required><?= $v["txt"] ?></textarea>
					</div>
				</div>
				<?
				}
			}
			?>
			</div>
			<div class="article-detail">
				
			</div>
			<div class="row">
				<div class="col-sm-3 col-xs-6">
					<table class="table table-bordered layout" data-value="1">
						<tbody>
							<tr>
								<td class="height-100"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-sm-3 col-xs-6">
					<table class="table table-bordered layout" data-value="2">
						<tbody>
							<tr>
								<td class="height-100"></td>
								<td class="height-100"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-sm-3 col-xs-6">
					<table class="table table-bordered layout" data-value="3">
						<tbody>
							<tr>
								<td class="height-100" rowspan="3"></td>
							</tr>
							<tr>
								<td class="height-50"></td>
							</tr>
							<tr>
								<td class="height-50"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-sm-3 col-xs-6">
					<table class="table table-bordered layout" data-value="4">
						<tbody>
							<tr>
								<td class="height-50"></td>
								<td class="height-50"></td>
							</tr>
							<tr>
								<td class="height-50"></td>
								<td class="height-50"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-sm-3 col-xs-6">
					<table class="table table-bordered layout" data-value="5">
						<tbody>
							<tr>
								<td class="height-100" rowspan="3"></td>
							</tr>
							<tr>
								<td class="height-50"></td>
								<td class="height-50"></td>
							</tr>
							<tr>
								<td class="height-50"></td>
								<td class="height-50"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>			
		</form>
		<button type="button" class="btn btn-success btn-fixed edit-btn-all" data-id="<?= $q["a_id"]; ?>" data-path="activity"><i class="fa fa-save"></i><br>Save</button>
		<?
		} else {
		?>
		<div class="row">
			<div class="col-xs-12">
				<h3>ยังไม่ได้ถูกสร้าง</h3>
			</div>
		</div>
		<?
		}
		?>
	</div>
</div>