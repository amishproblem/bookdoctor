<div class="block">
	<div class="del"><i class="fa fa-times"></i></div>
	<div class="row">
		<div class="col-xs-12">
			<h3 class="text-center">Layout 1</h3>
		</div>
		<div class="col-xs-12">
			<div class="-block">
				<div class="-bg -square-16-9 preview-block preview-1" data-list="1"></div>
				<div class="img-name">
					<input type="text" class="form-control" placeholder="ชื่อรูป">
				</div>
			</div>
			<div class="block-btn text-center">
				<button type="button" class="btn btn-info margin-bottom-12 btn-file">
					<i class="fa fa-picture-o"></i> เลือกรูปภาพ 1<input type="file" class="browse-file-image file-1" data-list="1">
				</button>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<textarea class="form-control" name="txt" rows="3" required></textarea>
		</div>
	</div>
</div>