$(document).ready(function() {
	$(document).on('make', '.datetime', function() {
		var t = this;
		console.log(t);
		$('.datepicker', this).each(function() {
			var today_start = $(this).data('today-start') || 0,
				date_start = $(this).data('date-start'),
				date_select = $(this).data('select');
			$(this).datepicker({
				language: $('html').attr('lang'),
				startDate: date_start,
				format: "yyyy-mm-dd",
				daysOfWeekDisabled: $(this).data('dowd') || [],
				todayHighlight: false
			}).on('changeDate', function(){
				var date = $(this).datepicker('getDate'),
					disable = false,
					init = false,
					f = $(this).parents('form');
				if (date == 'Invalid Date') {
					date = new Date();
					disable = true;
					init = true;
				} else {
					date.setHours(0, - date.getTimezoneOffset(), 0, 0);
					$('input[name="' + $(this).data('name') + '"]').val(date.toISOString().slice(0, 10));
					// if ($(this).hasClass('start')) {
					//	 $('input[name="start"]', f).val(date.toISOString().slice(0, 10));
					// } else if ($(this).hasClass('end')) {
					//	 $('input[name="end"]', f).val(date.toISOString().slice(0, 10));
					// } else {
					//	 $('input[name="date"]', f).val(date.toISOString().slice(0, 10));						
					// }
				}
				$('input[name="time"]', f).val('');
				var day = date.getDay() == 0 ? 7 : date.getDay(),
					tp = $('.timepicker', t),
					time = tp.data('worktime') || { };
				tp.html('');
				if (time[day] != undefined && time[day]['closed'] == '0') {
					var today = date_start == date.toISOString().slice(0, 10),
						start = time[day]['time_start'].split(':'),
						end = time[day]['time_end'].split(':'),
						s, e,
						h, m;
					start[0] = parseInt(start[0]);
					end[0] = parseInt(end[0]);
					s = start[0] * 60 + parseInt(start[1]);
					if (end[0] <= start[0]) {
						end[0] += 24;
					}
					e = end[0] * 60 + parseInt(end[1]);
					while (s < e) {
						h = Math.floor(s / 60);
						if (h >= 24) {
							h -= 24;
						}
						h = h < 10 ? '0' + h : h;
						m = s % 60;
						m = m < 10 ? '0' + m : m;
						if (today && !init) {
							disable = s <= today_start;
						}
						tp.append(
							$('<div' + (disable ? ' class="disable"' : '') + '/>').html(h + ':' + m)
						);
						s += 30;
					}
					tp.append(
						$('<div class="disable"/>').html((end[0] >= 24 ? end[0] - 24 : end[0]) + ':' + end[1])
					);
					$('div:not(.disable)', tp).bind('click', function() {
						$(this)
							.addClass('current')
							.siblings()
							.removeClass('current');
						$('input[name="time"]', f).val($(this).text());
					});
					$('div:not(.disable):first', tp).trigger('click');
				}
			});
			// $('.day:not(.disabled):first', this).trigger('click'); //trigger('changeDate');
			if (date_select !== undefined && date_select != '') {
				$(this).datepicker('setDate', date_select).datepicker('fill');
			}
		});
	});
	$('.datetime').each(function() {
		$(this).trigger('make');
	});

	$(document).on('click', '.check-param', function(e) {
		e.preventDefault();
		var url = $(this).attr('href'),
			submit = true;
		if (url.indexOf('?') < 0) {
			url += '?';
		}
		if ($(this).data('name') !== undefined && $(this).data('name') != '') {
			url += '&' + $(this).data('name') + '=' + $(this).data('value');
		}
		$('.param, [data-param]').each(function () {
			var param = '';
			if ($(this).hasClass('required') && $(this).val() == '') {
				alert($(this).data('error'));
				submit = false;
				return false;
			}
			if ($(this).data('param')) {
				if ($(this).data('param') != '') {
					param = '&' + $(this).data('name') + '=' + $(this).data('param');
				}
			} else if ($(this).is('select') || $(this).is('textarea')) {
				if ($(this).val() != '') {
					param = '&' + $(this).data('name') + '=' + $(this).val();
				}
			} else if ($(this).is('input')) {
				if ($(this).is(':checkbox') || $(this).is(':radio')) {
					if ($(this).is(':checked')) {
						param = '&' + $(this).data('name') + '=' + $(this).val();
					}
				} else {
					param = '&' + $(this).data('name') + '=' + $(this).val();
				}
			} else {

			}
			url += param;
		});
		if (!submit) {
			return false;
		} else {
			window.location = location.protocol + '//' + location.hostname + url;
		}
	});

	$("#btnLogin").click(function(event) {
		
		var form = $("#formLogin")

		if (form[0].checkValidity() === false) {
			event.preventDefault()
			event.stopPropagation()
		}

		form.addClass('was-validated');
	});

});