
$(function() {

	var domain = '.' + location.hostname.split('.').slice(-2).join('.'),
		lang = $('html').attr('lang');

	$(document).on('modals', '*', function(e, d) {

		if (d == undefined) {
			d = {};
		}

		var href = '',
			url = location.href,
			size = $(this).data('size') || d.size || '',
			empty = $(this).data('empty') || d.empty || '',
			nohistory = $(this).data('nohistory') || d.nohistory || false,
			method = $(this).data('method') || d.method || 'POST',
			isAjax = false;

		//console.log(history);

		if (d != undefined && d.href != undefined) {
			href = d.href;
		}
		else if ($(this).data('href') != undefined) {
			href = $(this).data('href');
		}
		else if ($(this).attr('href') == undefined) {
			return false;
		} else {
			href = $(this).attr('href');
		}

		var index = 0;
		if (href == '#gallery') {
			if ($(this).hasClass('zoom')) {
				index = $(this).index('.zoom');
			}
		}

		$('body').addClass('modals-overflow').append(
			$('<div class="modals"/>').html(
				$('<div class="modals-table"/>').html(
					$('<div class="modals-cell"/>').html(
						$('<div class="modals-inline"/>')
					)
				)
			)
		);

		function content(data) {

			$('.modals-inline').html(

				$('<div class="modals-content"/>').append(
					$('<span class="modals-close"><i class="far fa-times-circle"></i></span>')
				).append(data)

			).on('mouseenter', function() {

				$(this).addClass('m-active');

			}).on('mouseleave', function() {

				$(this).removeClass('m-active');

			});

			if ($('.modals .datetime').length) {
				$('.modals .datetime').each(function() {
					$(this).trigger('make');
				});
			}
			if ($('.modals .automatically').length) {
				$('.modals .automatically').each(function() {
					$(this).trigger('make');
				});
			}
			if ($('.modals .sortable').length) {
				$('.modals .sortable').each(function() {
					$(this).trigger('make');
				});
			}

			if ($('.modals .texteditor').length) {
				$('.modals .texteditor').each(function() {
					mkEditor($(this));
				});
			}

			$('.modals-inline, .modals-content').addClass(size).addClass(empty);

			$('.modals-close').on('click', function() {

				$('body').removeClass('modals-overflow');

				$('.modals')
					.off('click')
					.remove();

				$(window).off('keyup');

				if (typeof window.history.pushState === 'function' && !nohistory)
					window.history.pushState('', '', url);
			});

			/* need plugins? check */

			$('.fotorama', '.modals').each(function() {

				var fr = $(this).fotorama( $(this).data() ).data('fotorama');
				fr.show(index);

				$(window).bind('resize', function() {
					fr.resize({
						width: $(this).width() - 100,
						height: $(this).height() - 200
					});
				}).trigger('resize');
			});

			$('[data-autofocus]', '.modals').each(function() {
				$(this).focus();
			});

			// gmap();
		}

		var data = '';

		if (href[0] == '#') {

			data = $(href).clone().wrap('<div></div>').parent().html();
			content(data);

			// ga_event('Modal', 'static', href);

		} else {
			isAjax = true;
			$('.modals-inline').html(
				'<div class="modals-loading"><i>' + $('[data-loading-text]').data('loading-text') + '</i></div>'
			);
			var formData = {};
			if ($(this).closest('form').length > 0) {
				formData = new FormData($(this).closest('form')[0]);
			}
			
			$.ajax({
				url: href,
				type: method,
				data: formData,
				dataType: 'json',
				cache: false,
				contentType: false,
				processData: false,
				success: function(data) {
					content(data.data.html);
				}
			});

			ga_event('Modal', 'ajax', href);

			if (typeof window.history.pushState === 'function' && !nohistory)
				window.history.pushState('', '', href);
		}

		$(window).on('keyup', function(e) {

			if ( e.keyCode == 27 ) {
				$('.modals-close').trigger('click');
			}
		});

		var move = false;

		$('.modals').on('mousedown', function(e) {

			move = false;

			// if(!$(e.target).closest('.modals-inline').length) {
			if(!$(e.target).closest('.modals-inline').length && !$(e.target).hasClass('modals')) {

			//$('.modals-close').trigger('click');

			} else {
				$(this).on('mousemove', function () {

					move = true;
					$('.modals-inline').addClass('m-active');
				});
			}

		}).on('mouseup', function(e) {

			// if (!move && !$(e.target).closest('.modals-inline').length) {
			if (!$(e.target).closest('.modals-inline').length && !$(e.target).hasClass('modals')) {
				$('.modals-close').trigger('click');
			}

			if (!$(e.target).parents('.modals-inline').is('div')) {
				$('.modals-inline').removeClass('m-active');
			}

			$(this).off('mousemove');

			move = false;
		});


		//$('.modals-inline form input:first').focus();

		return false;

	});

	$(document).on('click', '.js-modals', function(e) {

		e.preventDefault();

		$(this).trigger('modals');
	});


});