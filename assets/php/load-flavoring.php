<?
include_once("functions.php");
include_once("database.php");
$f = getdata("SELECT * FROM food_flavoring ORDER BY ff_th_name ASC");
$u = getdata("SELECT * FROM food_unit ORDER BY fu_th_name ASC");
?>
<div class="col-xs-6">
	<div class="form-group">
		<label class="control-label" for="ff_id">ส่วนประกอบ</label>
		<select id="ff_id" name="ff_id[]" class="form-control">
		<?
		foreach ($f as $k => $v) {
			echo "<option value=\"".$v["ff_id"]."\">".$v["ff_th_name"]."</option>";
		}
		?>
		</select>
	</div>
</div>
<div class="col-xs-3">
	<div class="form-group">
		<label class="control-label" for="frf_quan">ปริมาณ</label>
		<input type="text" class="form-control" id="frf_quan" name="frf_quan[]" value="" placeholder="ปริมาณ" required>
	</div>
</div>
<div class="col-xs-3">
	<div class="form-group">
		<label class="control-label" for="fu_id">หน่วย</label>
		<select id="fu_id" name="fu_id[]" class="form-control">
		<?
		foreach ($u as $k => $v) {
			echo "<option value=\"".$v["fu_id"]."\">".$v["fu_th_name"]."</option>";
		}
		?>
		</select>
	</div>
</div>