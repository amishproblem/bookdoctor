<?

$dblink = null;
$last_err = null;
$last_insert_id = null;
$last_affected = 0;

function db_connect($name) {
	global $dblink, $conn, $conn_default;
	$localList = array(
		'127.0.0.1',
		'::1'
	);
	if(IS_DEV) {
		$dblink = mysqli_connect('localhost', 'root', 'root');
	} else {
		$dblink = mysqli_connect('localhost', 'bookdoctor', 'B00kdoctorMMT12#$');
	}
	if(!$dblink) {
		echo 'OOps. No db connection';
		exit;
	}
	mysqli_select_db($dblink, 'mmt_db');
	mysqli_query($dblink, "SET character_set_client='utf8'"); 
	mysqli_query($dblink, "SET character_set_results='utf8'"); 
	mysqli_query($dblink, "SET collation_connection='utf8_general_ci'");
	mysqli_query($dblink, "SET NAMES 'utf8'");
	mysqli_query($dblink, "SET CHARACTER SET 'utf8'");
	mysqli_query($dblink, "SET SESSION collation_connection = 'utf8_general_ci'");
	return $dblink;
}

function getmicrotime() {
	list($usec, $sec) = explode(" ",microtime());
	return ((float)$usec + (float)$sec);
}

function trace($sql, $err=null, $dur=0, $cnt=0) {
	global $dblink;
	$qq	= 'insert into tmp (status, tmp, referer, dur, cnt, stamp) values ("'.($err != null ? 'error' : 'success').'", "'.($err != null ? $err.': ' : '').r(r($sql,'"','\''),"'","\'").'", "'.$_SERVER['HTTP_REFERER'].'",'.($dur * 1000).','.$cnt.', NOW())';
	mysqli_query($dblink,$qq);
}

function query($sql, $quiet=false) {
	global $query_count, $query_time, $dblink, $last_err, $last_affected;
	if(!$dblink) {
		db_connect(null);
	}
	if(!$dblink) {
		echo 'NODB';
		return;
	}
	try {
		$time_start = getmicrotime();
		$result = mysqli_query($dblink, $sql);
		$time_end = getmicrotime();
		$last_err = null;
		if(!$result) {
			$last_err = mysqli_error($dblink);
			$report = "SQL call failed : " . $last_err
					.' : ['.$sql.']'
					."\n<br>";
			if(!$quiet) {
				// $directmail = "winai.hongsibsong@gmail.com";
				// sendmail(array(
				// 	'from' => "info@salary",
				// 	'to'   => $directmail,
				// 	'cc' => 'nupp.gun@gmail.com',
				// 	'subj' => 'SQL problem on '.domain().' - ' . date('Y-m-d H:i:s'),
				// 	'body' 	=> '<html><pre>'.$report	
				// 						."\n<h3>Server</h3>"
				// 						."\n".neat_r($_SERVER, true)
				// 						."\n<h3>Session</h3>"
				// 						."\n".neat_r($_SESSION, true)
				// 						."\n<h3>Stack trace</h3>"
				// 						."\n".neat_r(debug_backtrace(), true)
				// ));
			}
			// trace($sql, $last_err);
			return false;
		}
		@$mysql_num_rows = mysqli_num_rows($result);
		if(mysqli_affected_rows($dblink)) {
			$mysql_num_rows = mysqli_affected_rows($dblink);
			$last_affected = $mysql_num_rows;
		}
	} catch(Exception $e) {

	}
	$query_count = $query_count + 1;
	$query_time = $query_time + ($time_end - $time_start);
	$low = strtolower(' '.$sql);
	if(strpos($low,'insert') == 1) {
		$result =  mysqli_insert_id($dblink);
	}
	// if(strpos($low,'insert') == 1 || strpos($low,'update') == 1 || strpos($low,'delete') == 1) {
	// 	return	$result;
	// 	return $mysql_num_rows;
	// }
	if(!$quiet) {
		// trace($sql, mysqli_error($dblink), ($time_end - $time_start), $mysql_num_rows);
	}	
	return	$result;
}

function query_record($sql) {
	$result = query($sql);
	if(!empty($result)) {
		$lst = mysqli_fetch_assoc($result);
		mysqli_free_result($result);
		return $lst;
	}
	return false;
}

function getDBValue($sql, $quiet=false) {
	$res = query_record($sql, $quiet);
	if(!$res) {
		return false;
	}
	$keys =	array_keys($res);
	return @$res[$keys[0]];
}

function getDBArray($sql, $fld='0') {
	$a = Array();
	$result = query($sql);
	if($result) {
		while($row = mysqli_fetch_array($result)) {
			$a[] = $row[$fld == '0' ? 0 : $fld];
		}
	}
	return	$a;
}

function getdata($q, $quiet=false) {
	$res = query($q, $quiet);
	$r = array();
	$keys = null;
	if($res) {
		while($row = mysqli_fetch_array($res)) {
			if($keys == null) {
				$keys = array_keys($row);
			}
			$m = array();
			foreach ($keys as $key) {
				if(!is_numeric($key)) {
					$m[$key] = $row[$key];
				}
			}				
			$r[] = $m;
		}
	}
	return $r;
}

function getPKFor($table, $schema) {
	global $setup;
	$qq = "SELECT distinct `COLUMN_NAME`
		FROM `information_schema`.`COLUMNS`
		WHERE (`TABLE_NAME` = '".$table."')
		  AND (`COLUMN_KEY` = 'PRI')"
		  .($schema ? " and (`TABLE_SCHEMA` = '".$setup['schema']."')" : " limit 1");
	return getDBValue($qq);
}

function getMaxKey($table, $schema = null) {
	if(!$table) {
		return null;
	}
	return getDBValue('select max('.getPKFor($table, $schema).')+1 from '.$table);
}

?>