<?
include_once("functions.php");
include_once("database.php");
$data = array();
$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
$detectedType = exif_imagetype($_FILES[0]["tmp_name"]);
if(in_array($detectedType, $allowedTypes)) {
	$error = false;
	$files = array();
	$table = decrypt($_POST["table"]);
	$key_name = decrypt($_POST["key_name"]);
	$key_value = $_POST["key_value"];
	$uploaddir = ROOT."assets/images/".$table."/".$key_value."/";
	$dir = "assets/images/".$table."/".$key_value."/";
	$ext = pathinfo($_FILES[0]["name"], PATHINFO_EXTENSION);
	if(!empty($_POST["name"])) {
		$name = explode(".", end(explode("/", $_POST["name"])));
		$name = $name[0];
	}
	if(!empty($name)) {
		$new = $name.".".$ext;
	} else {
		$new = empty($key_value) ? uniqueID().".".$ext : $key_value.".".$ext;		
	}
	if(!is_dir($dir)) {
		$d = explode("/", $dir);
		$c = ROOT;
		for($i = 0, $ic = count($d); $i < $ic; $i++) {
			if($d[$i] == "") {
				continue;
			}
			$c .= "/" . $d[$i];
			if(!is_dir($c)) {
				mkdir($c, 0777);
				chmod($c, 0777);
			}
		}
	}
	foreach ($_FILES as $file) {
	    if(move_uploaded_file($file["tmp_name"], $uploaddir.$new)) {
	        $files["filename"] = $new;
	        chmod($uploaddir.$new, 0777);
	    } else {
	        $error = true;
	    }
	}
	$data = ($error) ? array("error" => "There was an error uploading your files") : array("files" => $files);
} else {
	$data = array("error" => "Wrong File Types");
}
if(!$error) {
	$q = query("UPDATE ".$table." SET ".$_POST["update"]." = \"".$table."/".$key_value."/".$new."\" WHERE ".$key_name." = ".$key_value);
	if($q != 1) {
		$data = array("error" => "Update Fail!!!");
	}
}
echo json_encode($data);
?>