<?
include_once("functions.php");
include_once("database.php");
$i = getdata("SELECT * FROM food_ingredient ORDER BY fi_th_name ASC");
$u = getdata("SELECT * FROM food_unit ORDER BY fu_th_name ASC");
?>
<div class="col-xs-6">
	<div class="form-group">
		<label class="control-label" for="fi_id">ส่วนประกอบ</label>
		<select id="fi_id" name="fi_id[]" class="form-control">
		<?
		foreach ($i as $k => $v) {
			echo "<option value=\"".$v["fi_id"]."\">".$v["fi_th_name"]."</option>";
		}
		?>
		</select>
	</div>
</div>
<div class="col-xs-3">
	<div class="form-group">
		<label class="control-label" for="fri_quan">ปริมาณ</label>
		<input type="text" class="form-control" id="fri_quan" name="fri_quan[]" value="" placeholder="ปริมาณ" required>
	</div>
</div>
<div class="col-xs-3">
	<div class="form-group">
		<label class="control-label" for="fu_id">หน่วย</label>
		<select id="fu_id" name="fu_id[]" class="form-control">
		<?
		foreach ($u as $k => $v) {
			echo "<option value=\"".$v["fu_id"]."\">".$v["fu_th_name"]."</option>";
		}
		?>
		</select>
	</div>
</div>