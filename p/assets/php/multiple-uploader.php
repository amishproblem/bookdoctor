<?
include_once("functions.php");
include_once("database.php");
$data = array();
$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
$uploaddir = ROOT."assets/images/".$_POST["path"]."/".$_POST["id"]."/";
$dir = "assets/images/".$_POST["path"]."/".$_POST["id"]."/";
$old_id = -1;
foreach ($_FILES as $k => $v) {
	$detectedType = exif_imagetype($v["tmp_name"]);
	$ext = pathinfo($v["name"], PATHINFO_EXTENSION);
	if(in_array($detectedType, $allowedTypes)) {
		$error = false;
		$new = uniqueID().".".$ext;
		list($id, $order) = explode(",",$k);
		if($id == 0) {
			if(!is_dir($dir)) {
				$d = explode("/", $dir);
				$c = ROOT;
				for($i = 0, $ic = count($d); $i < $ic; $i++) {
					if($d[$i] == "") {
						continue;
					}
					$c .= "/" . $d[$i];
					if(!is_dir($c)) {
						mkdir($c, 0777);
						chmod($c, 0777);
					}
				}
			}
		}
		if(move_uploaded_file($v["tmp_name"], $uploaddir.$new)) {
	        chmod($uploaddir.$new, 0777);
	    } else {
	        $error = true;
	    }
	    if($error) {
	    	$data[$id+1][$order+1]["image"] = "notfound.jpg";
	    	$data[$id+1][$order+1]["name"] = $_POST[$id.",".$order];
	    } else {
	    	$data[$id+1][$order+1]["image"] = $_POST["path"]."/".$_POST["id"]."/".$new;
	    	$data[$id+1][$order+1]["name"] = $_POST[$id.",".$order];
	    }
	} else {
		$data[$id+1][$order+1]["image"] = "notfound.jpg";
		$data[$id+1][$order+1]["name"] = $_POST[$id.",".$order];
	}
	if($id > $old_id) {
    	$data[$id+1]["txt"] = $_POST[$id.",txt"];
    	$old_id = $id;
    }
}
$values = array();
$last_order = getDBValue("SELECT MAX(a_order) FROM article_detail WHERE a_id = ".$_POST["id"]);
foreach ($data as $k => $v) {
	$val = "";
	$val .= "('".$_POST["id"]."','".($last_order+$k)."',";
	for($i=1; $i < 6; $i++) {
		if(!empty($v[$i]["image"])) {
			$val .= "'".$v[$i]["image"]."',";
		} else {
			$val .= "'',";
		}
		if(!empty($v[$i]["name"])) {
			$val .= "'".$v[$i]["name"]."',";
		} else {
			$val .= "'',";
		}
	}
	$val .= "'".addslashes($v["txt"])."')";
	array_push($values, $val);
}
$q = query("INSERT INTO article_detail (a_id,a_order,image1,alt1,image2,alt2,image3,alt3,image4,alt4,image5,alt5,txt) VALUES ".implode(',', $values));
echo $q;
?>