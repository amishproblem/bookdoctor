<?
define("LANG", "th");
define('_KEY', "bookdoctor");
$localList = array(
	'127.0.0.1',
	'::1'
);
if (in_array($_SERVER['REMOTE_ADDR'], $localList)) {
	define("IS_DEV", true);
} else {
	define("IS_DEV", false);
}
if (IS_DEV) {
	define('ROOT' , $_SERVER['DOCUMENT_ROOT'].'/bookdoctor/');
	define('PATH_', '/bookdoctor/');
	define('URL_', 'http://' . $_SERVER['SERVER_NAME'] . '/');
} else {
	define('ROOT' , $_SERVER['DOCUMENT_ROOT'].'/');
	define('PATH_', '/');
	define('URL_', 'https://' . $_SERVER['SERVER_NAME'] . '/');
}
define('DOMAIN_MAIN', implode('.', array_slice(explode('.', $_SERVER['HTTP_HOST']), -2)));
define('DOMAIN_COOKIE', '.' . DOMAIN_MAIN);
$files_request = array(
	"robots.txt",
	"sitemap.xml"
);

function pr($object, $exit = 0) {
	echo "<pre>";
	print_r($object);
	echo "</pre>";
	if($exit > 0){
		exit;
	}
}

function r($s, $w, $st='') {
	return str_replace($w, $st, $s);
}

function addquote($s) {
	return "'" . $s . "'";
}

function array_to_sql($a) {
	$s = array();
	foreach ($a as $k => $i) {
		$s[] = "`$k` = '" . addslashes($i) . "'";
	}
	return $s;
}

function clear($s) {
	return r(r(r(r(r(r(r(r(r(r(r(r($s,'|?|'), ';'), '*'), ')'),'('),'\\'),'/'),'&'),'undefined'),"\n"),"'"),'"');
}

function domain() {
	if (IS_DEV) {
		$s = 'https://' . $_SERVER['SERVER_NAME'] . '/bookdoctor/';
	} else {
		$s = 'https://' . $_SERVER['SERVER_NAME'] . '/';
	}
	return $s;
}

function neat_r($arr, $return = false) {
	$out = array();
	$oldtab = "	";
	$newtab = "  ";
	$lines = explode("\n", print_r($arr, true));
	foreach ($lines as $line) {
		if(substr($line, -5) != "Array") {
			$line = preg_replace("/^(\s*)\[[0-9]+\] => /", "$1", $line, 1);
		}
		foreach (array(
			"Array" => "",
			" =>"   => ":",
		) as $old => $new) {
			$out = str_replace($old, $new, $out);
		}
		if(in_array(trim($line), array("Array", "(", ")", ""))) {
			continue;
		}
		$indent = "";
		$indents = floor((substr_count($line, $oldtab) - 1) / 2);
		if($indents > 0) {
			for($i = 0; $i < $indents; $i++) {
				$indent .= $newtab;
			}
		}
		$out[] = $indent . trim($line);
	}
	$out = implode("\n", $out) . "\n";
	if($return == true) {
		return $out;
	}
	echo $out;
}

function uniqueID() {
	list($usec, $sec) = explode(" ",microtime());
	return uniqid()."-".((int)$usec + (int)$sec);
}

function redirect( $url="" ) {
	if ($url == "") {
		header("Location: " . $_SERVER["REQUEST_URI"]);
	} else if ($url == 404) {
		header("HTTP/1.0 404 Not Found", true, 404);
		throw new Exception("404");
	} else {
		header("Location: " . $url, true, 301);
	}
	exit;
}

function trim_string($s) {
	$f = array('"', "'", "<");
	$t = array("&quot;", "&#39;", "&lt;");
	$s = trim(str_replace($f, $t, $s));
	return $s;
}

function trim_array(&$a) {
	foreach ($a as $k => $i) {
		if (is_array($i)) {
			trim_array($i);
		} else {
			$a[$k] = trim_string($i);
		}
	}
}

function urlname($s) {
	return r(strtolower($s), ' ', '-');
}
function reurlname($s) {
	return r(r(ucwords($s), '-', ' '), '_', ' ');
}

function dateth($s, $fm='d M Y') {
	$month = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
	$r = date('j', strtotime($s)).' '.$month[date('n', strtotime($s))].' '.(date('Y', strtotime($s))+543);
	return $r;
}

function monthth($s) {
	$month = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
	$r = $month[$s];
	return $r;
}

function encrypt($data) {
	return base64_encode($data._KEY);
}

function decrypt($data) {
	return r(base64_decode($data), _KEY);
}

function realurl($text) {
	$text = strtolower($text);
	$symbol_one = array(" ", "_", "%20", "&");
	$symbol_two = array("*", "!", "@", "#", "$", "%", "/");
	$symbol_three = array("-------", "------", "----", "---", "--");
	$text = str_replace($symbol_one, "-", $text);
	$text = str_replace($symbol_two, "", $text);
	return str_replace($symbol_three, "-", $text);
}

function set_cookie($name, $value, $expires = 3600, $domain = null) {
	if($expires < 0) {
		$expires = -1;		
	} else{
		$expires = time() + $expires;		
	}
	if(PHP_VERSION < 5.2) {
		setcookie($name, $value, $expires, "/", is_null($domain) ? $_SERVER["HTTP_HOST"] : $domain . "; HttpOnly");
	} else {
		setcookie($name, $value, $expires, "/", $domain, NULL, TRUE);
	}
}

function shorter($txt, $limit) {
	if (strlen($txt) > $limit) {
		$new_text = mb_substr($txt, 0, $limit, 'UTF-8');
		$new_text = trim_string(strip_tags($new_text));
		return $new_text . '...';
	} else {
		return $txt;
	}
}

function resize($original, $w = 230, $h = false) {
	if(strpos($original, "notfound")) {
		return $original;
	}
	if(!file_exists(ROOT . $original)) {
		return "assets/images/notfound.png";
	}
	$new = "cache/" . substr_replace($original, "-" . $w . ($h ? "x" . $h : ""), strrpos($original, "."), 0);
	if(isset($_GET["flush-images"]) || !file_exists(ROOT . $new) || filemtime(ROOT . $new) < filemtime(ROOT . $original)) {
		$dir = dirname($new);
		if(!is_dir($dir)) {
			$d = explode("/", $dir);
			$c = ROOT;
			for($i = 0, $ic = count($d); $i < $ic; $i++) {
				if($d[$i] == "") {
					continue;
				}
				$c .= "/" . $d[$i];
				if(!is_dir($c)) {
					mkdir($c, 0777);
					chmod($c, 0777);
				}
			}
		}
		$quality = "60";
		$s = "convert \"" . ROOT . $original . "\" -resize \"{$w}" . ($h ? "x{$h}^" : "") . "\" -gravity center " . ($h ? "-crop \"{$w}x{$h}+0+0\" +repage" : "") . " -quality $quality -background transparent \"" . ROOT . $new . "\"";
		$u = exec($s);
		chmod(ROOT.$new, 0777);
	}
	return $new;
}

function get_client_ip() {
	$ipaddress = '';
	if (isset($_SERVER['HTTP_CLIENT_IP']))
		$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_X_FORWARDED']))
		$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
		$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_FORWARDED']))
		$ipaddress = $_SERVER['HTTP_FORWARDED'];
	else if(isset($_SERVER['REMOTE_ADDR']))
		$ipaddress = $_SERVER['REMOTE_ADDR'];
	else
		$ipaddress = 'UNKNOWN';
	return $ipaddress;
}

?>