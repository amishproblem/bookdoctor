<?
include_once("functions.php");
$data = array();
$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
$detectedType = exif_imagetype($_FILES[0]["tmp_name"]);
// if(!empty($_POST["name"])) {
// 	$name = explode(".", end(explode("/", $_POST["name"])));
// 	$name = $name[0];
// }
$name = @$_POST["name"];
$ext = pathinfo($_FILES[0]["name"], PATHINFO_EXTENSION);
if(in_array($detectedType, $allowedTypes)) {
	$error = false;
	$files = array();
	$uploaddir = ROOT."assets/images/".$_POST["path"]."/".$_POST["id"]."/";
	$dir = "assets/images/".$_POST["path"]."/".$_POST["id"]."/";
	if(!empty($name)) {
		$new = $name.".".$ext;
	} else {
		$new = !isset($_POST["id"]) ? uniqueID().".".$ext : $_POST["id"].".".$ext;		
	}
	if(!is_dir($dir)) {
		$d = explode("/", $dir);
		$c = ROOT;
		for($i = 0, $ic = count($d); $i < $ic; $i++) {
			if($d[$i] == "") {
				continue;
			}
			$c .= "/" . $d[$i];
			if(!is_dir($c)) {
				mkdir($c, 0777);
				chmod($c, 0777);
			}
		}
	}
	foreach ($_FILES as $file) {
	    if(move_uploaded_file($file["tmp_name"], $uploaddir.$new)) {
	        $files["filename"] = $new;
	        chmod($uploaddir.$new, 0777);
	    } else {
	        $error = true;
	    }
	}
	$data = ($error) ? array("error" => "There was an error uploading your files") : array("files" => $files);
} else {
	$data = array("error" => "Wrong File Types");
}
echo json_encode($data);
?>