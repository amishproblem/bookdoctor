<?
$nav = array(
	'beauty' => 'ความงาม',
	'lose-weight' => 'ลดน้ำหนัก',
	'for-men' => 'สำหรับผู้ชาย',
	'health' => 'สุขภาพ',
)
?>
<!DOCTYPE html>
<html lang="TH">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<link href="https://fonts.googleapis.com/css?family=Sarabun:400,700&display=swap&subset=thai" rel="stylesheet">

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		<link href="/assets/fontawesome/css/all.css" rel="stylesheet">
		<link href="/assets/css/center.css" rel="stylesheet">
		<link href="/assets/css/modals.css" rel="stylesheet">

		<link href="/assets/css/main.css" rel="stylesheet">
		
		<link href="/assets/css/custom-bootstrap.css" rel="stylesheet">

		<link rel="icon" type="image/png" href="/assets/images/b-icon.png">

		<title>BOOKDOCTOR</title>
	</head>
	<body>
		<header class="py-3">
			<div class="container">
				<h1 class="text-center">naenam.com</h1>
			</div>
		</header>
		<nav>
			<div class="container">
				<ul class="d-flex align-items-center justify-content-center list-unstyled">
					<?
					foreach ($nav as $k => $v) {
					?>
						<li><a class="d-block py-3 px-4" href="/<?= $k; ?>"><?= $v; ?></a></li>
					<?
					}
					?>
					
				</ul>
			</div>
		</nav>
		<div class="container">	
			<section class="product">
				<div class="card-columns">
					<div class="card">
						<div class="img-wrap py-3 px-3">
							<img src="/assets/images/product-cla-plus.png">
							<span class="ribbon3">สินค้าแนะนำ</span>
						</div>
						<div class="text-wrap pb-3">
							<h6 class="my-0 px-3">CLA PLUS</h6>
							<p class="px-3">
								ลดน้ำหนัก 10 กิโลกรัมภายในสองสัปดาห์ ไม่เหนื่อย ไม่เพลีย หุ่นสวยกระชับได้อย่างใจ
							</p>
						</div>
					</div>
					<div class="card">
						<div class="img-wrap py-3 px-3">
							<img src="/assets/images/product-fortamin.png">
							<span class="ribbon3">สินค้าแนะนำ</span>
						</div>
						<div class="text-wrap pb-3">
							<h6 class="my-0 px-3">Fortamin</h6>
							<p class="px-3">
								ขจัดปัญหาปวดข้อ บำรุงข้อต่อให้กลับมามีสุขภาพดี หายปวดภายใน 4 วัน
							</p>
						</div>
					</div>
					<div class="card">
						<div class="img-wrap py-3 px-3">
							<img src="/assets/images/product-fos-plus.png">
							<span class="ribbon3">สินค้าแนะนำ</span>
						</div>
						<div class="text-wrap pb-3">
							<h6 class="my-0 px-3">FOS PLUS</h6>
							<p class="px-3">
								ลดความอ้วน ดีท็อกซ์ลำไส้ ปลอดภัย ผอมไว 10 — 12 กก. ต่อเดือน
							</p>
						</div>
					</div>
					<div class="card">
						<div class="img-wrap py-3 px-3">
							<img src="/assets/images/product-fun-fan.png">
							<span class="ribbon3">สินค้าแนะนำ</span>
						</div>
						<div class="text-wrap pb-3">
							<h6 class="my-0 px-3">FUN FAN</h6>
							<p class="px-3">
								จะมีผู้ชายคนไหนในโลกที่ไม่ต้องการมีเซ็กส์ที่สุดยอด การถึงจุดสุดยอดที่ให้ความสุขสุดๆแบบเน้นๆ
							</p>
						</div>
					</div>
					<div class="card">
						<div class="img-wrap py-3 px-3">
							<img src="/assets/images/product-helmina.png">
							<span class="ribbon3">สินค้าแนะนำ</span>
						</div>
						<div class="text-wrap pb-3">
							<h6 class="my-0 px-3">Helmina</h6>
							<p class="px-3">
								กำจัดกลิ่นปากจากการติดเชื้อปรสิต ป้องกัน รักษา กำจัดพยาธิทุกชนิดแม้แต่ตัวอ่อนและไข่ได้หมดเกลี้ยง
							</p>
						</div>
					</div>
					<div class="card">
						<div class="img-wrap py-3 px-3">
							<img src="/assets/images/product-solli.png">
							<span class="ribbon3">สินค้าแนะนำ</span>
						</div>
						<div class="text-wrap pb-3">
							<h6 class="my-0 px-3">Solli</h6>
							<p class="px-3">
								ช่วยลดน้ำหนักและกำจัดไขมัน 80% ในร่างกายได้ โดยไม่ต้องควบคุมอาหารหรือออกกำลังกาย
							</p>
						</div>
					</div>
					<div class="card">
						<div class="img-wrap py-3 px-3">
							<img src="/assets/images/product-star-plus.png">
							<span class="ribbon3">สินค้าแนะนำ</span>
						</div>
						<div class="text-wrap pb-3">
							<h6 class="my-0 px-3">Star Plus</h6>
							<p class="px-3">
								กระบวนการเมตาบอลิซึมโดยไม่มีผลข้างเคียงอย่างมีนัยสำคัญ STAR PLUS ใช้ในโปรแกรมสร้างรูปร่างเพื่อรักษา
							</p>
						</div>
					</div>
					<div class="card">
						<div class="img-wrap py-3 px-3">
							<img src="/assets/images/product-t-chrome.png">
							<span class="ribbon3">สินค้าแนะนำ</span>
						</div>
						<div class="text-wrap pb-3">
							<h6 class="my-0 px-3">T-CHROME</h6>
							<p class="px-3">
								ลดน้ำหนัก ลดสัดส่วนอย่างได้ผล ด้วยผลิตภัณฑ์ที่เน้นสภาวะทางชีวภาพของร่างกาย
							</p>
						</div>
					</div>
				</div>
			</section>
		</div>

		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	</body>
</html>