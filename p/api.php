<?php

include_once('assets/php/functions.php');
include_once('assets/php/database.php');
if (!empty($_POST)) {
    if (isset($_POST['name']) && !empty($_POST['name']) && isset($_POST['phone']) && !empty($_POST['phone'])) {
        $sql = 'INSERT INTO customer (name,phone,product,ip,order_date,reference) VALUES ("' . nl2br($_POST['name']) . '", "' . nl2br($_POST['phone']) . '", "' . nl2br($_POST['product']) . '", "' . $_POST['ip'] . '", "' . date('Y-m-d H:i:s') . '", "' . $_POST['reference'] . '")';
        $q = query($sql);
    }
}

require_once __DIR__ . '/api-manager/autoloader.php';

if (DataGuard::notEmptyArray([
    $_POST['name'],
    $_POST['phone'],
])){
    $config = require __DIR__ . '/config.php';

    $leadvertexApi = new LeadvertexAdminApi();

    $leadvertexApi->methodManager->addOrder();
    $leadvertexApi->url->setOfferName($config['offer_name'])->setToken($config['offer_token']);

    $leadvertexApi->goods->registerGood($leadvertexApi->goodTemplate->setGoodID($config['good_id'])->setQuantity($config['good_quantity'])->setPrice($config['good_price']), Goods::IMPORT);

    $leadvertexApi->params
        ->setFio($_POST['name'])
        ->setPhone($_POST['phone'])
        ->setReferer(DataHelper::getReferer())
        ->setIp(DataHelper::getUserIp())
        ->setGoods($leadvertexApi->goods)
    ;

    $answer = $leadvertexApi->getSender()->setParams($leadvertexApi->params)->sendPost();

    Logger::logInput();
    Logger::logOutput($leadvertexApi);
    Logger::logAnswer($answer);

    // print_r($answer);
    header("Location: /success", true, 301);
}

