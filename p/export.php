<?
header('Content-Type: text/html; charset=utf-8');
include_once('assets/php/functions.php');
include_once('assets/php/database.php');
$allow_list = array('leadvertex');
$export_id = array();
$return = array();
if (isset($_GET['key']) && !empty($_GET['key'])) {
	$q = 'SELECT * FROM apikey WHERE apikey = "' . $_GET['key'] . '"';
	$key = getdata($q);
	if (in_array($key[0]['name'], $allow_list)) {
		$sql = 'SELECT id, name, phone, product, ip, order_date, reference FROM customer WHERE status = 0';
		$data = getdata($sql);
		if (!empty($data)) {
			foreach ($data as $k => $v) {
				array_push($export_id, $v['id']);
				unset($v['id']);
				$return[$k] = $v;
			}
			if (!empty($export_id)) {
				$u = 'UPDATE customer SET status = 1 WHERE id IN(' . implode(',', $export_id) . ')';
				query($u);
			}
		}
		echo json_encode($return);
	} else {
		echo json_encode("Access Denied");
	}
}
?>