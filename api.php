<?php

include_once('assets/php/functions.php');
include_once('assets/php/database.php');
if (!empty($_POST)) {
    if (isset($_POST['name']) && !empty($_POST['name']) && isset($_POST['phone']) && !empty($_POST['phone'])) {
        $sql = 'INSERT INTO customer (name,phone,product,ip,order_date,reference) VALUES ("' . nl2br($_POST['name']) . '", "' . nl2br($_POST['phone']) . '", "' . nl2br($_POST['product']) . '", "' . $_POST['ip'] . '", "' . date('Y-m-d H:i:s') . '", "' . $_POST['reference'] . '")';
        $q = query($sql);
    }
}

$dpg_url = "https://digitalpromotiongroup.ru/API/conversion/create?user_api_key=5BXMB-LSWOG-4F4X2-ZIHMJ-W2VXZ&clickid=" . $_POST['utm_content'] . "&action_id={{id}}&goal=claim&status=0&sum=0&currency=usd";

// $curl = curl_init();
// curl_setopt($curl, CURLOPT_URL, $curl);
// curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
// $answer = curl_exec($curl);
// curl_close($curl);

// if ($this->isJSON($answer)) {
//     return json_decode($answer, $assoc);
// }

if ($_POST['utm_source'] == 'mgid') {
    $sql = "SELECT * FROM customer WHERE product = " . $_POST['product'] . " AND reference LIKE '%/mg/%' AND order_date > '2020-02-07 14:00:00'";
    $sum = getdata($sql);
    $utm_medium = sprintf("%04d", count($sum));
    $_POST['utm_medium'] = $utm_medium;
}

require_once __DIR__ . '/api-manager/autoloader.php';

if (DataGuard::notEmptyArray([
    $_POST['name'],
    $_POST['phone'],
])){
    $config = require __DIR__ . '/config.php';

    // $leadvertexApi = new LeadvertexAdminApi();

    // $leadvertexApi->methodManager->addOrder();
    // $leadvertexApi->url->setOfferName($config['offer_name'])->setToken($config['offer_token']);

    $leadvertexApi = new LeadvertexWebmasterApi();
    $leadvertexApi->methodManager->addOrder();
    $leadvertexApi->url->setOfferName($config['offer_name'])->setToken($config['offer_token'])->addGetParam('webmasterID',4);

    $leadvertexApi->goods->registerGood($leadvertexApi->goodTemplate->setGoodID($config['good_id'])->setQuantity($config['good_quantity'])->setPrice($config['good_price']), Goods::IMPORT);

    $leadvertexApi->params
        ->setFio($_POST['name'])
        ->setPhone($_POST['phone'])
        ->setReferer(DataHelper::getReferer())
        ->setIp(DataHelper::getUserIp())
        ->setGoods($leadvertexApi->goods)
        ->setUtmCampaign($config['utm_campaign'])
        ->setUtmContent($config['utm_content'])
        ->setUtmMedium($config['utm_medium'])
        ->setUtmTerm($config['utm_term'])
        ->setUtmSource($config['utm_source'])
    ;

    $answer = $leadvertexApi->getSender()->setParams($leadvertexApi->params)->sendPost();

    Logger::logInput();
    Logger::logOutput($leadvertexApi);
    Logger::logAnswer($answer);

    // print_r($answer);
    header("Location: /success", true, 301);
}

