
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		<link href="/fontawesome/css/all.css" rel="stylesheet">
		
		<link rel="stylesheet" href="../main.css">

		<title>Hello, world!</title>
	</head>
	<body>
		<div class="wrapper d-flex flex-column align-items-center justify-content-center">
			<div class="container">
				<div class="row">
					<div class="mx-auto mb-4 col-12 col-lg-8">
						<div class="multisteps-form__progress">
							<button class="multisteps-form__progress-btn js-active" type="button" title="What kind of diabetes do you have?">1</button>
							<button class="multisteps-form__progress-btn" type="button" title="What's your relationship with meat?">2</button>
							<button class="multisteps-form__progress-btn" type="button" title="Order Info">3</button>
							<button class="multisteps-form__progress-btn" type="button" title="Comments">4</button>
							<button class="multisteps-form__progress-btn" type="button" title="Comments">5</button>
							<button class="multisteps-form__progress-btn" type="button" title="Comments">6</button>
							<button class="multisteps-form__progress-btn" type="button" title="Comments">7</button>
							<button class="multisteps-form__progress-btn" type="button" title="Comments">8</button>
							<button class="multisteps-form__progress-btn" type="button" title="Comments">9</button>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="mx-auto mb-4 col-12 col-lg-8">
						<form class="multisteps-form__form">

							<div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="scaleOut">
								<h3 class="multisteps-form__title mb-4">What kind of diabetes do you have?</h3>
								<div class="multisteps-form__content full-width">
									<input class="checkbox-tools" type="radio" name="step1" id="step1-1" checked>
									<label class="for-checkbox-tools" for="step1-1">
										Pre-diabetes
									</label>

									<input class="checkbox-tools" type="radio" name="step1" id="step1-2">
									<label class="for-checkbox-tools" for="step1-2">
										Type 1
									</label>

									<input class="checkbox-tools" type="radio" name="step1" id="step1-3">
									<label class="for-checkbox-tools" for="step1-3">
										Type 2
									</label>

									<input class="checkbox-tools" type="radio" name="step1" id="step1-4">
									<label class="for-checkbox-tools" for="step1-4">
										Lada (1.5)
									</label>

									<input class="checkbox-tools" type="radio" name="step1" id="step1-5">
									<label class="for-checkbox-tools" for="step1-5">
										Gestational
									</label>

									<input class="checkbox-tools" type="radio" name="step1" id="step1-6">
									<label class="for-checkbox-tools" for="step1-6">
										I don't know
									</label>
									<div class="button-row d-flex mt-4">
										<button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Next</button>
									</div>
								</div>
							</div>

							<div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleOut">
								<h3 class="multisteps-form__title">What's your relationship with meat?</h3>
								<div class="multisteps-form__content full-width">
									<input class="checkbox-tools" type="radio" name="step2" id="step2-1" checked>
									<label class="for-checkbox-tools" for="step2-1">
										I love meat - I eat it all the time
									</label>

									<input class="checkbox-tools" type="radio" name="step2" id="step2-2">
									<label class="for-checkbox-tools" for="step2-2">
										I eat meat occasionally
									</label>

									<input class="checkbox-tools" type="radio" name="step2" id="step2-3">
									<label class="for-checkbox-tools" for="step2-3">
										I eat meat rarely
									</label>

									<input class="checkbox-tools" type="radio" name="step2" id="step2-4">
									<label class="for-checkbox-tools" for="step2-4">
										I never eat meat
									</label>
									<div class="button-row d-flex mt-4">
										<button class="btn btn-primary js-btn-prev" type="button" title="Prev">Prev</button>
										<button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Next</button>
									</div>
								</div>
							</div>

							<div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleOut">
								<h3 class="multisteps-form__title">What veggies are your favorites?</h3>
								<div class="multisteps-form__content full-width">
									<input class="checkbox-tools" type="checkbox" name="step3" id="step3-1">
									<label class="for-checkbox-tools" for="step3-1">
										Carrots
									</label>

									<input class="checkbox-tools" type="checkbox" name="step3" id="step3-2">
									<label class="for-checkbox-tools" for="step3-2">
										Green beans
									</label>

									<input class="checkbox-tools" type="checkbox" name="step3" id="step3-3">
									<label class="for-checkbox-tools" for="step3-3">
										Broccoli
									</label>

									<input class="checkbox-tools" type="checkbox" name="step3" id="step3-4">
									<label class="for-checkbox-tools" for="step3-4">
										Zucchini
									</label>

									<input class="checkbox-tools" type="checkbox" name="step3" id="step3-5">
									<label class="for-checkbox-tools" for="step3-5">
										Cuumber
									</label>

									<input class="checkbox-tools" type="checkbox" name="step3" id="step3-6">
									<label class="for-checkbox-tools" for="step3-6">
										Potatoes
									</label>

									<input class="checkbox-tools" type="checkbox" name="step3" id="step3-7">
									<label class="for-checkbox-tools" for="step3-7">
										Lettuce
									</label>

									<input class="checkbox-tools" type="checkbox" name="step3" id="step3-8">
									<label class="for-checkbox-tools" for="step3-8">
										Tomatoes
									</label>

									<input class="checkbox-tools" type="checkbox" name="step3" id="step3-9">
									<label class="for-checkbox-tools" for="step3-9">
										I like them all
									</label>
									<div class="button-row d-flex mt-4">
										<button class="btn btn-primary js-btn-prev" type="button" title="Prev">Prev</button>
										<button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Next</button>
									</div>
								</div>
							</div>

							<div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleOut">
								<h3 class="multisteps-form__title">What kind of carbs and starchy foods do you like?</h3>
								<div class="multisteps-form__content full-width">
									<input class="checkbox-tools" type="checkbox" name="step4" id="step4-1">
									<label class="for-checkbox-tools" for="step4-1">
										Pasta
									</label>

									<input class="checkbox-tools" type="checkbox" name="step4" id="step4-2">
									<label class="for-checkbox-tools" for="step4-2">
										Rice
									</label>

									<input class="checkbox-tools" type="checkbox" name="step4" id="step4-3">
									<label class="for-checkbox-tools" for="step4-3">
										Sweet potatoes
									</label>

									<input class="checkbox-tools" type="checkbox" name="step4" id="step4-4">
									<label class="for-checkbox-tools" for="step4-4">
										Chickpeas
									</label>

									<input class="checkbox-tools" type="checkbox" name="step4" id="step4-5">
									<label class="for-checkbox-tools" for="step4-5">
										Couscous
									</label>

									<input class="checkbox-tools" type="checkbox" name="step4" id="step4-6">
									<label class="for-checkbox-tools" for="step4-6">
										Sorghum
									</label>

									<input class="checkbox-tools" type="checkbox" name="step4" id="step4-7">
									<label class="for-checkbox-tools" for="step4-7">
										I like them all
									</label>
									<div class="button-row d-flex mt-4">
										<button class="btn btn-primary js-btn-prev" type="button" title="Prev">Prev</button>
										<button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Next</button>
									</div>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>

		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
		<script src="../script.js"></script>
	</body>
</html>