<?
include_once('../../../assets/php/functions.php');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		<link href="../../fontawesome/css/all.css" rel="stylesheet">
		
		<link rel="stylesheet" href="../../main.css">

		<title>รักษาโรคเบาหวานไม่ใช่เรื่องยากอีกต่อไป</title>
	</head>
	<body>
		<div class="wrapper no-bg d-flex flex-column align-items-center justify-content-center">
			<div class="container">
				<div class="row">
					<div class="mx-auto mb-sm-4 col-12 col-lg-8">
						<div class="multisteps-form__progress">
							<button class="multisteps-form__progress-btn js-active" type="button" title="คุณเป็นโรคเบาหวานประเภทอะไร?">1</button>
							<button class="multisteps-form__progress-btn" type="button" title="คุณชอบรับประทานเนื้อสัตว์มากน้อยเท่าไหร่?">2</button>
							<button class="multisteps-form__progress-btn" type="button" title="ผักโปรดของคุณคือผักอะไรบ้าง?">3</button>
							<button class="multisteps-form__progress-btn" type="button" title="คุณรับประทานคาร์โบไฮเดรตและแป้งประเภทไหนบ้าง?">4</button>
							<button class="multisteps-form__progress-btn" type="button" title="ผลไม้ชนิดใดที่คุณโปรดปราน?">5</button>
							<button class="multisteps-form__progress-btn" type="button" title="อาหารที่คนทั่วไปรับประทาน">6</button>
							<button class="multisteps-form__progress-btn" type="button" title="คุณใช้เวลาในการเตรียมอาหารเป็นเวลาเท่าไหร่ต่อวัน?">7</button>
							<button class="multisteps-form__progress-btn" type="button" title="คุณกระตือรือร้นมากแค่ไหน?">8</button>
							<button class="multisteps-form__progress-btn" type="button" title="กรุณากรอกแบบฟอร์ม">9</button>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="mx-auto mb-4 col-12 col-lg-8">
						<form class="multisteps-form__form" method="POST" action="/diabetes/th/success.php">

							<div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="scaleOut">
								<div class="title step-1">
									<h3 class="multisteps-form__title mb-0">คุณเป็นโรคเบาหวานประเภทอะไร?</h3>
								</div>
								<div class="multisteps-form__content full-width">

									<input class="checkbox-tools" type="radio" name="step1" id="step1-1" value="Pre-diabetes">
									<label class="for-checkbox-tools" for="step1-1">
										เสี่ยงต่อการเป็นโรคเบาหวาน
									</label>

									<input class="checkbox-tools" type="radio" name="step1" id="step1-2" value="Type 1">
									<label class="for-checkbox-tools" for="step1-2">
										เบาหวานชนิดที่ 1
									</label>

									<input class="checkbox-tools" type="radio" name="step1" id="step1-3" value="Type 2">
									<label class="for-checkbox-tools" for="step1-3">
										เบาหวานชนิดที่ 2
									</label>

									<input class="checkbox-tools" type="radio" name="step1" id="step1-4" value="Lada (1.5)">
									<label class="for-checkbox-tools" for="step1-4">
										เบาหวานชนิด 1.5
									</label>

									<input class="checkbox-tools" type="radio" name="step1" id="step1-5" value="Gestational">
									<label class="for-checkbox-tools" for="step1-5">
										โรคเบาหวานขณะตั้งครรภ์
									</label>

									<input class="checkbox-tools" type="radio" name="step1" id="step1-6" value="I don't know">
									<label class="for-checkbox-tools" for="step1-6">
										ยังไม่ทราบ
									</label>

									<div class="button-row d-flex mt-4">
										<button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">ต่อไป</button>
									</div>

								</div>
							</div>

							<div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleOut">
								<div class="title step-2">
									<h3 class="multisteps-form__title mb-0">คุณชอบรับประทานเนื้อสัตว์มากน้อยเท่าไหร่?</h3>
								</div>
								<div class="multisteps-form__content full-width">

									<input class="checkbox-tools" type="radio" name="step2" id="step2-1" value="I love meat - I eat it all the time">
									<label class="for-checkbox-tools" for="step2-1">
										ทุกมื้อ
									</label>

									<input class="checkbox-tools" type="radio" name="step2" id="step2-2" value="I eat meat occasionally">
									<label class="for-checkbox-tools" for="step2-2">
										บางมื้อ
									</label>

									<input class="checkbox-tools" type="radio" name="step2" id="step2-3" value="I eat meat rarely">
									<label class="for-checkbox-tools" for="step2-3">
										น้อยมาก
									</label>

									<input class="checkbox-tools" type="radio" name="step2" id="step2-4" value="I never eat meat">
									<label class="for-checkbox-tools" for="step2-4">
										ไม่รับประทานเนื้อสัตว์เลย
									</label>

									<div class="button-row d-flex mt-4">
										<button class="btn btn-primary js-btn-prev" type="button" title="Prev">ย้อนกลับ</button>
										<button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">ต่อไป</button>
									</div>

								</div>
							</div>

							<div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleOut">
								<div class="title step-3">
									<h3 class="multisteps-form__title">ผักโปรดของคุณคือผักอะไรบ้าง?</h3>
									<p class="mb-0">เลือกที่คุณชอบ</p>
								</div>
								<div class="multisteps-form__content full-width">

									<input class="checkbox-tools" type="checkbox" name="step3[]" id="step3-1" value="Carrots">
									<label class="for-checkbox-tools" for="step3-1">
										แครอท
									</label>

									<input class="checkbox-tools" type="checkbox" name="step3[]" id="step3-2" value="Green beans">
									<label class="for-checkbox-tools" for="step3-2">
										ถั่วเขียว
									</label>

									<input class="checkbox-tools" type="checkbox" name="step3[]" id="step3-3" value="Broccoli">
									<label class="for-checkbox-tools" for="step3-3">
										บร็อคโคลี
									</label>

									<input class="checkbox-tools" type="checkbox" name="step3[]" id="step3-4" value="Zucchini">
									<label class="for-checkbox-tools" for="step3-4">
										บวบ
									</label>

									<input class="checkbox-tools" type="checkbox" name="step3[]" id="step3-5" value="Cuumber">
									<label class="for-checkbox-tools" for="step3-5">
										แตงกวา
									</label>

									<input class="checkbox-tools" type="checkbox" name="step3[]" id="step3-6" value="Potatoes">
									<label class="for-checkbox-tools" for="step3-6">
										มันฝรั่ง
									</label>

									<input class="checkbox-tools" type="checkbox" name="step3[]" id="step3-7" value="Lettuce">
									<label class="for-checkbox-tools" for="step3-7">
										ผักกาดหอม
									</label>

									<input class="checkbox-tools" type="checkbox" name="step3[]" id="step3-8" value="Tomatoes">
									<label class="for-checkbox-tools" for="step3-8">
										มะเขือเทศ
									</label>

									<input class="checkbox-tools check-all" type="checkbox" name="step3[]" id="step3-9" value="">
									<label class="for-checkbox-tools" for="step3-9">
										ชอบทุกอย่าง
									</label>

									<div class="button-row d-flex mt-4">
										<button class="btn btn-primary js-btn-prev" type="button" title="Prev">ย้อนกลับ</button>
										<button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">ต่อไป</button>
									</div>

								</div>
							</div>

							<div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleOut">
								<div class="title step-4">
									<h3 class="multisteps-form__title">คุณรับประทานคาร์โบไฮเดรตและแป้งประเภทไหนบ้าง?</h3>
									<p class="mb-0">เลือกที่คุณชอบ</p>
								</div>
								<div class="multisteps-form__content full-width">

									<input class="checkbox-tools" type="checkbox" name="step4[]" id="step4-1" value="Pasta">
									<label class="for-checkbox-tools" for="step4-1">
										พาสต้า
									</label>

									<input class="checkbox-tools" type="checkbox" name="step4[]" id="step4-2" value="Rice">
									<label class="for-checkbox-tools" for="step4-2">
										ข้าว
									</label>

									<input class="checkbox-tools" type="checkbox" name="step4[]" id="step4-3" value="Sweet potatoes">
									<label class="for-checkbox-tools" for="step4-3">
										มันฝรั่งหวาน
									</label>

									<input class="checkbox-tools" type="checkbox" name="step4[]" id="step4-4" value="Chickpeas">
									<label class="for-checkbox-tools" for="step4-4">
										ถั่วชิกพี
									</label>

									<input class="checkbox-tools" type="checkbox" name="step4[]" id="step4-5" value="Couscous">
									<label class="for-checkbox-tools" for="step4-5">
										คูสคูส
									</label>

									<input class="checkbox-tools" type="checkbox" name="step4[]" id="step4-6" value="Sorghum">
									<label class="for-checkbox-tools" for="step4-6">
										ข้าวฟ่าง
									</label>

									<input class="checkbox-tools check-all" type="checkbox" name="step4[]" id="step4-7" value="">
									<label class="for-checkbox-tools" for="step4-7">
										ชอบทุกอย่าง
									</label>

									<div class="button-row d-flex mt-4">
										<button class="btn btn-primary js-btn-prev" type="button" title="Prev">ย้อนกลับ</button>
										<button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">ต่อไป</button>
									</div>

								</div>
							</div>

							<div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleOut">
								<div class="title step-5">
									<h3 class="multisteps-form__title">ผลไม้ชนิดใดที่คุณโปรดปราน?</h3>
									<p class="mb-0">เลือกที่คุณชอบ</p>
								</div>
								<div class="multisteps-form__content full-width">

									<input class="checkbox-tools" type="checkbox" name="step5[]" id="step5-1" value="Banana">
									<label class="for-checkbox-tools" for="step5-1">
										กล้วย
									</label>

									<input class="checkbox-tools" type="checkbox" name="step5[]" id="step5-2" value="Apples">
									<label class="for-checkbox-tools" for="step5-2">
										แอปเปิ้ล
									</label>

									<input class="checkbox-tools" type="checkbox" name="step5[]" id="step5-3" value="Kiwi">
									<label class="for-checkbox-tools" for="step5-3">
										กีวี่
									</label>

									<input class="checkbox-tools" type="checkbox" name="step5[]" id="step5-4" value="Orange">
									<label class="for-checkbox-tools" for="step5-4">
										ส้ม
									</label>

									<input class="checkbox-tools" type="checkbox" name="step5[]" id="step5-5" value="Peaches">
									<label class="for-checkbox-tools" for="step5-5">
										ลูกพีช
									</label>

									<input class="checkbox-tools" type="checkbox" name="step5[]" id="step5-6" value="Melon">
									<label class="for-checkbox-tools" for="step5-6">
										แตงโม
									</label>

									<input class="checkbox-tools" type="checkbox" name="step5[]" id="step5-7" value="Pears">
									<label class="for-checkbox-tools" for="step3-7">
										ลูกแพร์
									</label>

									<input class="checkbox-tools" type="checkbox" name="step5[]" id="step5-8" value="Berries">
									<label class="for-checkbox-tools" for="step5-8">
										ผลเบอร์รี่
									</label>

									<input class="checkbox-tools check-all" type="checkbox" name="step5[]" id="step5-9" value="">
									<label class="for-checkbox-tools" for="step5-9">
										ชอบทุกอย่าง
									</label>

									<div class="button-row d-flex mt-4">
										<button class="btn btn-primary js-btn-prev" type="button" title="Prev">ย้อนกลับ</button>
										<button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">ต่อไป</button>
									</div>

								</div>
							</div>

							<div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleOut">
								<div class="title step-6">
									<h3 class="multisteps-form__title">อาหารที่คนทั่วไปรับประทาน</h3>
									<p class="mb-0">เลือกที่คุณชอบ</p>
								</div>
								<div class="multisteps-form__content full-width">

									<input class="checkbox-tools" type="checkbox" name="step6[]" id="step6-1" value="Bell peppers">
									<label class="for-checkbox-tools" for="step6-1">
										พริกหยวก
									</label>

									<input class="checkbox-tools" type="checkbox" name="step6[]" id="step6-2" value="Eggs">
									<label class="for-checkbox-tools" for="step6-2">
										ไข่
									</label>

									<input class="checkbox-tools" type="checkbox" name="step6[]" id="step6-3" value="Nuts">
									<label class="for-checkbox-tools" for="step6-3">
										ถั่ว
									</label>

									<input class="checkbox-tools" type="checkbox" name="step6[]" id="step6-4" value="Mushrooms">
									<label class="for-checkbox-tools" for="step6-4">
										เห็ด
									</label>

									<input class="checkbox-tools" type="checkbox" name="step6[]" id="step6-5" value="Onions">
									<label class="for-checkbox-tools" for="step6-5">
										หัวหอม
									</label>

									<input class="checkbox-tools" type="checkbox" name="step6[]" id="step6-6" value="Cauliflower">
									<label class="for-checkbox-tools" for="step6-6">
										กะหล่ำปลี
									</label>

									<input class="checkbox-tools" type="checkbox" name="step6[]" id="step6-7" value="Asparagus">
									<label class="for-checkbox-tools" for="step6-7">
										หน่อไม้ฝรั่ง
									</label>

									<input class="checkbox-tools" type="checkbox" name="step6[]" id="step6-8" value="Celery">
									<label class="for-checkbox-tools" for="step6-8">
										ผักชีฝรั่ง
									</label>

									<input class="checkbox-tools check-all" type="checkbox" name="step6[]" id="step6-9" value="">
									<label class="for-checkbox-tools" for="step6-9">
										เคยรับประทานทุกอย่าง
									</label>

									<div class="button-row d-flex mt-4">
										<button class="btn btn-primary js-btn-prev" type="button" title="Prev">ย้อนกลับ</button>
										<button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">ต่อไป</button>
									</div>

								</div>
							</div>

							<div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleOut">
								<div class="title step-7">
									<h3 class="multisteps-form__title mb-0">คุณใช้เวลาในการเตรียมอาหารเป็นเวลาเท่าไหร่ต่อวัน?</h3>
								</div>
								<div class="multisteps-form__content full-width">

									<input class="checkbox-tools" type="radio" name="step7" id="step7-1" value="Up to 30 minutes">
									<label class="for-checkbox-tools" for="step7-1">
										30 นาที
									</label>

									<input class="checkbox-tools" type="radio" name="step7" id="step7-2" value="Up to 1 hour">
									<label class="for-checkbox-tools" for="step7-2">
										1 ชั่วโมง
									</label>

									<input class="checkbox-tools" type="radio" name="step7" id="step7-3" value="More than 1 hour">
									<label class="for-checkbox-tools" for="step7-3">
										มากกว่า 1 ชั่วโมง
									</label>

									<div class="button-row d-flex mt-4">
										<button class="btn btn-primary js-btn-prev" type="button" title="Prev">ย้อนกลับ</button>
										<button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">ต่อไป</button>
									</div>

								</div>
							</div>

							<div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleOut">
								<div class="title step-8">
									<h3 class="multisteps-form__title mb-0">คุณกระตือรือร้นมากแค่ไหน?</h3>
								</div>
								<div class="multisteps-form__content full-width">

									<input class="checkbox-tools" type="radio" name="step8" id="step8-1" value="Very active">
									<label class="for-checkbox-tools" for="step8-1">
										มาก
									</label>

									<input class="checkbox-tools" type="radio" name="step8" id="step8-2" value="Moderately active">
									<label class="for-checkbox-tools" for="step8-2">
										ปานกลาง
									</label>

									<input class="checkbox-tools" type="radio" name="step8" id="step8-3" value="Not active">
									<label class="for-checkbox-tools" for="step8-3">
										น้อย
									</label>

									<div class="button-row d-flex mt-4">
										<button class="btn btn-primary js-btn-prev" type="button" title="Prev">ย้อนกลับ</button>
										<button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">ต่อไป</button>
									</div>

								</div>
							</div>

							<div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleOut">
								<div class="title step-9">
									<h3 class="multisteps-form__title">กรุณากรอกแบบฟอร์ม</h3>
									<p class="mb-0">บอกเราเกี่ยวกับคุณเพื่อให้เราสามารถวางแผนให้คุณได้</p>
								</div>
								<div class="multisteps-form__content full-width">

									<input type="tel" class="digits form-control" id="age" name="age" placeholder="อายุ" required>

									<input type="tel" class="digits form-control" id="height" name="height" placeholder="ส่วนสูง" required>

									<input type="tel" class="digits form-control" id="weight" name="weight" placeholder="น้ำหนัก" required>

									<input type="tel" class="digits form-control" id="goal" name="goal" placeholder="เป้าหมาย" required>

									<input type="hidden" name="gender" value="<?= $_GET['plan']; ?>">
									<input type="hidden" name="ip" value="<?= get_client_ip(); ?>">
									<input type="hidden" name="reference" value="<?= $_SERVER['HTTP_REFERER']; ?>">


									<div class="button-row d-flex mt-4">
										<button class="btn btn-primary js-btn-prev" type="button" title="Prev">ย้อนกลับ</button>
										<button class="btn btn-primary ml-auto" type="submit" title="Next">ต่อไป</button>
									</div>

								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>

		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
		<script src="../../script.js"></script>
	</body>
</html>