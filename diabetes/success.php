<?
include_once('../assets/php/functions.php');
include_once('../assets/php/database.php');
if (!empty($_POST)) {
	$sql = 'INSERT INTO diabetes (gender,age,height,weight,goal,step1,step2,step3,step4,step5,step6,step7,step8,ip,create_date,reference) VALUES ("' . nl2br($_POST['gender']) . '", "' . nl2br($_POST['age']) . '", "' . nl2br($_POST['height']) . '", "' . nl2br($_POST['weight']) . '", "' . nl2br($_POST['goal']) . '", "' . $_POST['step1'] . '", "' . $_POST['step2'] . '", "' .implode(', ', $_POST['step3']) . '", "' .implode(', ', $_POST['step4']) . '", "' .implode(', ', $_POST['step5']) . '", "' .implode(', ', $_POST['step6']) . '", "' . $_POST['step7'] . '", "' . $_POST['step8'] . '", "' . $_POST['ip'] . '", "' . date('Y-m-d H:i:s') . '", "' . $_POST['reference'] . '")';
	$q = query($sql);
	if ($q) {
		redirect('/diabetes/success.php?q=' . $q);
	}
}
if (isset($_GET['q'])) {
	$sql = "SELECT *
	FROM `diabetes`
	WHERE `id` = " . $_GET['q'];
	$diabetes = getdata($sql);
	$diabetes = $diabetes[0];
	if (empty($diabetes)) {
		redirect('/diabetes');
	}
	$bmi = $diabetes['weight'] / ($diabetes['height'] * $diabetes['height']) * 10000;
	$bmi = number_format($bmi, 2);
	if ($bmi < 25) {
		$obesity = 1;
	} else if ($bmi >= 25 && $bmi < 30) {
		$obesity = 2;
	} else {
		$obesity = 3;
	}
	if ($diabetes['gender'] == 'male') {
		$bmr = round(66 + (13.7 * $diabetes['weight']) + (5 *$diabetes['height']) - (6.8 * $diabetes['age']));
	} else {
		$bmr = round(665 + (9.6 * $diabetes['weight']) + (1.8 * $diabetes['height']) - (4.7 * $diabetes['age']));
	}
	$lostPerWeek = ($diabetes['weight'] - $diabetes['goal']) / 11.12;
	$lostPerWeek = number_format($lostPerWeek, 2);
	$url = array('solli-th', 'cla-plus-th', 't-chrome-th', 'star-plus-th', 'fosplus-th');
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		
		<link rel="stylesheet" href="main.css">

		<title>Get your personalized diabetes diet</title>
	</head>
	<body>
		<div class="new-summary container py-3">
			<!-- <img class="logo mb-4" src="logo.webp"> -->
			<h1 class="mb-4">Get your personalized diabetes diet</h1>
			<h4 class="mb-5">Based on your answers, you will be...</h4>
			<div class="b-blocks-featured">
				<div class="b-result b-result--weight">
					<div class="b-stats">
						<div class="b-stats--header">
							<p><?= $diabetes['goal'] + 2; ?></p>
							<p><?= $diabetes['goal'] + 1; ?></p>
							<p class="e-special"><?= $diabetes['goal']; ?> <span>kg</span></p>
							<p><?= $diabetes['goal'] - 1; ?></p>
							<p><?= $diabetes['goal'] - 2; ?></p>
						</div>
						<div class="b-stats--footer">
							<p>
								by
								<span class="e-month"><?= date('M', strtotime($diabetes['create_date'])); ?></span>,
								<span class="e-year"><?= date('Y', strtotime($diabetes['create_date'])) + 1; ?></span>
							</p>
						</div>
					</div>
					<div class="b-months">
						<div class="b-stats--header">
							<p><?= date('M', strtotime($diabetes['create_date'])); ?></p>
							<p><?= date('M', strtotime('+7 months', strtotime($diabetes['create_date']))); ?></p>
							<p><?= date('M', strtotime($diabetes['create_date'])); ?></p>
						</div>
						<div class="b-stats--results">
							<div class="b-stats--results--from">
								<?= $diabetes['weight']; ?>
								<span>kg</span>
							</div>
							<div class="b-stats--results--to">
								<?= $diabetes['goal']; ?>
								<span>kg</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<h2 class="mb-5">Your personal summary</h2>
			<div class="b-dietresults">
				<div class="b-info" data-type="bmi">
					<div class="e-title">BMI</div>
					<div class="e-results">
						<div class="e-subtitle">Your body mass index:</div>
						<div class="e-subtitle m-special"><?= $bmi; ?> (obesity class <?= $obesity; ?>)</div>
					</div>
					<div class="e-infobar position-<?= $obesity; ?>">
						<div class="e-value obesityclass<?= $obesity; ?>">
							<p class="value"><?= $bmi; ?></p>
							<!-- <p class="text">Obesity class <?= $obesity; ?></p> -->
						</div>
						<div class="e-infobar--gap">
						</div>
					</div>
					<div class="b-results m-detailed">
						<p>44% of our plan users have similar BMI</p>
					</div>
				</div>
				<div class="b-info m-vertical" data-type="weight_loss_forecast">
					<div class="e-title">Weight loss forecast</div>
					<div class="e-results">
						<p class="e-subtitle">Your weight loss by week</p>
					</div>
					<div class="e-img">
						<div class="e-numers">
							<div class="e-number"><?= $lostPerWeek + 1.42; ?> kg</div>
							<div class="e-number"><?= $lostPerWeek + 0.67; ?> kg</div>
							<div class="e-number"><?= $lostPerWeek; ?> kg</div>
							<div class="e-number"><?= $lostPerWeek; ?> kg</div>
						</div>
						<img src="https://mydiabetes.diet/assets/results/weight_loss.png" alt="">
					</div>
				</div>
				<div class="b-info m-vertical" data-type="calorie_intake">
					<div class="e-title">Calorie intake</div>
					<div class="e-results">
						<div class="e-subtitle">Your current calorie intake:</div>
						<div class="e-subtitle m-special"><?= $bmr; ?>-<?= $bmr + 150; ?> calories</div>
					</div>
					<div class="e-calorieintake">
						<p><?= $bmr; ?>-<?= $bmr + 150; ?></p>
						<p class="messure">kcal.</p>
					</div>
				</div>
				<div class="b-info m-vertical" data-type="water_intake">
					<div class="e-title">Water intake</div>
					<div class="e-results">
						<div class="e-subtitle">Recommended water:</div>
						<div class="e-subtitle m-special">2 liters</div>
					</div>
					<div class="e-calorieintake">
						<p>2</p>
						<p class="messure">liters</p>
					</div>
				</div>
				<div class="b-info" data-type="dishes">
					<div class="e-title">Meals</div>
					<div class="e-results">
						<div class="e-subtitle">Unique personalized food combinations to help you reach your goals:</div>
						<div class="e-subtitle m-special food-combinations" style="color: #31D4CA;  text-align: center;   font-size: 78px; font-weight: bold; padding-top: 30px;">
							1000+
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<footer>
			<a href="https://bookdoctor.asia/leads/<?= $url[array_rand($url)]; ?>">Get your plan</a>
		</footer>
		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	</body>
</html>