<?
include_once('/assets/php/functions.php');
$url = array('solli-th', 'cla-plus-th', 't-chrome-th', 'star-plus-th', 'fosplus-th');
?>
<!DOCTYPE html>
<html lang="th">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>ดูแลสุขภาพและลดน้ำหนักด้วยการควบคุมอาหารและการออกกำลังกาย</title>

		<link href="/assets/bootstrap4/css/bootstrap.min.css" rel="stylesheet">
		<link href="/assets/fontawesome/css/all.css" rel="stylesheet">

		<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet">

		<link href="./calorie.css" rel="stylesheet">
		<link href="./switch-checkbox.css" rel="stylesheet">

		<script type="text/javascript" src="./modernizr.custom.js"></script>
	</head>
	<body>
		<div class="wrap">	
			<div id="bl-main" class="bl-main">
				<section class="bl-work-section">
					<div class="bl-box">
						<h2 class="m-0">BMI</h2>
					</div>
					<div class="bl-content text-center">
						<div class="container">
							<h2 class="mb-2">เครื่องคำนวณหาค่าดัชนีมวลกาย (BMI)</h2>
							<p class="mb-4">การหาค่าดัชนีมวลกาย (Body Mass Index : BMI) คือเป็นมาตรการที่ใช้ประเมินภาวะอ้วนและผอมในผู้ใหญ่ ตั้งแต่อายุ 20 ปีขึ้นไป สามารถทำได้โดยการชั่งน้ำหนักตัวเป็นกิโลกรัม และวัดส่วนสูงเป็นเซนติเมตร แล้วนำมาหาดัชมีมวลกาย โดยใช้โปรแกรมวัดค่าความอ้วนข้างต้น</p>
							<form class="bmi d-inline-block text-left" method="POST" data-panel="panel-1">
								<div class="row">
									<div class="col-12">
										<div class="mb-4">
											<label for="weight1">น้ำหนัก<span class="d-none d-sm-inline"> (กิโลกรัม)</span></label>
											<input type="tel" class="digits form-control" id="weight1" name="weight" placeholder="" required>
										</div>
									</div>
									<div class="col-12">
										<div class="mb-4">
											<label for="height1">ส่วนสูง<span class="d-none d-sm-inline"> (เซนติเมตร)</span></label>
											<input type="tel" class="digits form-control" id="height1" name="height" placeholder="" required>
										</div>
									</div>
								</div>
								<div class="text-center">
									<button type="submit" class="btn btn-success">คำนวณ</button>
								</div>
							</form>
						</div>
					</div>
					<span class="bl-icon bl-icon-close">
						<i class="far fa-times-circle fa-2x"></i>
					</span>
				</section>
				<section class="bl-work-section">
					<div class="bl-box">
						<h2 class="m-0">BMR</h2>
					</div>
					<div class="bl-content text-center">
						<div class="container">
							<h2 class="mb-2">อัตราการเผาผลาญพลังงาน (BMR)</h2>
							<p class="my-4">BMR ย่อมาจาก Basal Metabolic Rate หรือเราสามารถเรียกได้ว่าเป็นอัตราการเผาผลาญพลังงานในแต่ละวัน</p>
							<form class="bmr d-inline-block text-left" method="POST" data-panel="panel-2">
								<div class="row">
									<div class="col-sm-6">
										<div class="mb-4">
											<label for="gender2">เพศ</label>
											<div class="can-toggle demo-rebrand-1">
												<input type="checkbox" id="gender2" name="gender" value="1">
												<label for="gender2">
													<div class="can-toggle__switch" data-checked="Female" data-unchecked="Male"></div>
												</label>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-4">
											<label for="age2">อายุ</label>
											<input type="tel" class="digits form-control" id="age2" name="age" placeholder="" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-4">
											<label for="weight2">น้ำหนัก<span class="d-none d-sm-inline"> (กิโลกรัม)</span></label>
											<input type="tel" class="digits form-control" id="weight2" name="weight" placeholder="" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-4">
											<label for="height2">ส่วนสูง<span class="d-none d-sm-inline"> (เซนติเมตร)</span></label>
											<input type="tel" class="digits form-control" id="height2" name="height" placeholder="" required>
										</div>
									</div>
								</div>
								<div class="text-center">
									<button type="submit" class="btn btn-success">คำนวณ</button>
								</div>
							</form>
							<div class="result text-center">
								
							</div>
						</div>
					</div>
					<span class="bl-icon bl-icon-close">
						<i class="far fa-times-circle fa-2x"></i>
					</span>
				</section>
				<section class="bl-work-section">
					<div class="bl-box">
						<h2 class="m-0">TDEE</h2>
					</div>
					<div class="bl-content text-center">
						<div class="container">
							<h2 class="mb-2">พลังงานที่ใช้กิจกรรมอื่น (TDEE)</h2>
							<p class="mb-4">TDEE คือ Total Daily Energy Expenditure หรือ ค่าของพลังงานที่ใช้กิจกรรมอื่นในแต่ละวัน</p>
							<form class="tdee d-inline-block text-left" method="POST" data-panel="panel-3">
								<div class="row">
									<div class="col-sm-6">
										<div class="mb-4">
											<label for="gender3">เพศ</label>
											<div class="can-toggle demo-rebrand-1">
												<input type="checkbox" id="gender3" name="gender" value="1">
												<label for="gender3">
													<div class="can-toggle__switch" data-checked="Female" data-unchecked="Male"></div>
												</label>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-4">
											<label for="age3">อายุ</label>
											<input type="tel" class="digits form-control" id="age3" name="age" placeholder="" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-4">
											<label for="weight3">น้ำหนัก<span class="d-none d-sm-inline"> (กิโลกรัม)</span></label>
											<input type="tel" class="digits form-control" id="weight3" name="weight" placeholder="" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-4">
											<label for="height3">ส่วนสูง<span class="d-none d-sm-inline"> (เซนติเมตร)</span></label>
											<input type="tel" class="digits form-control" id="height3" name="height" placeholder="" required>
										</div>
									</div>
									<div class="col-12">
										<div class="mb-4">
											<label for="exercise3">คุณออกกำลังกายบ่อยแค่ไหน</label>
											<select class="select-2 form-control" id="exercise3" name="exercise">
												<option value="1.2">ออกกำลังกายน้อยมากหรือไม่ออกเลย</option>
												<option value="1.375">ออกกำลังกาย 1-3 ครั้งต่อสัปดาห์</option>
												<option value="1.55">ออกกำลังกาย 4-5 ครั้งต่อสัปดาห์</option>
												<option value="1.725">อออกกำลังกาย 6-7 ครั้งต่อสัปดาห์</option>
												<option value="1.9">ออกกำลังกายวันละ 2 ครั้งขึ้นไป</option>
											</select>
										</div>
									</div>
								</div>
								<div class="text-center">
									<button type="submit" class="btn btn-success">คำนวณ</button>
								</div>
							</form>
						</div>
					</div>
					<span class="bl-icon bl-icon-close">
						<i class="far fa-times-circle fa-2x"></i>
					</span>
				</section>
				
				<section class="bl-work-section">
					<div class="bl-box">
						<h2 class="m-0">Diet Planning</h2>
					</div>
					<div class="bl-content text-center">
						<div class="container">
							<h2 class="mb-4">แผนการลดน้ำหนัก (Diet Planning)</h2>
							<p class="my-4">Diet Planning การวางแผนการลดน้ำหนักโดยคำนวณจากแคลอรี่ที่ต้องลดลงในแต่ละวัน</p>
							<form class="diet d-inline-block text-left" method="POST" data-panel="panel-4">
								<div class="row">
									<div class="col-sm-6">
										<div class="mb-4">
											<label for="gender4">เพศ</label>
											<div class="can-toggle demo-rebrand-1">
												<input type="checkbox" id="gender4" name="gender" value="1">
												<label for="gender4">
													<div class="can-toggle__switch" data-checked="Female" data-unchecked="Male"></div>
												</label>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-4">
											<label for="age4">อายุ</label>
											<input type="tel" class="digits form-control" id="age4" name="age" placeholder="" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-4">
											<label for="weight4">น้ำหนัก<span class="d-none d-sm-inline"> (กิโลกรัม)</span></label>
											<input type="tel" class="digits form-control" id="weight4" name="weight" placeholder="" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-4">
											<label for="goal4">เป้าหมาย<span class="d-none d-sm-inline"> (กิโลกรัม)</span></label>
											<input type="tel" class="digits form-control" id="goal4" name="goal" placeholder="" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-4">
											<label for="height4">ส่วนสูง<span class="d-none d-sm-inline"> (เซนติเมตร)</span></label>
											<input type="tel" class="digits form-control" id="height4" name="height" placeholder="" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-4">
											<label for="days">จำนวนวัน</label>
											<input type="tel" class="digits form-control" id="days4" name="days" placeholder="" required>
										</div>
									</div>
									<div class="col-12">
										<div class="mb-4">
											<label for="exercise4">คุณออกกำลังกายบ่อยแค่ไหน</label>
											<select class="select-2 form-control" id="exercise4" name="exercise">
												<option value="1.2">ออกกำลังกายน้อยมากหรือไม่ออกเลย</option>
												<option value="1.375">ออกกำลังกาย 1-3 ครั้งต่อสัปดาห์</option>
												<option value="1.55">ออกกำลังกาย 4-5 ครั้งต่อสัปดาห์</option>
												<option value="1.725">อออกกำลังกาย 6-7 ครั้งต่อสัปดาห์</option>
												<option value="1.9">ออกกำลังกายวันละ 2 ครั้งขึ้นไป</option>
											</select>
										</div>
									</div>
								</div>
								<div class="text-center">
									<button type="submit" class="btn btn-success">คำนวณ</button>
								</div>
							</form>
							<textarea class="d-none">
								<h4 class="my-0 mb-2">ผลลัพธ์แคลลอรี่ของคุณคือ</h4>
								<p class="pb-2">
									<strong>{fatLevel}</strong><br>
									อัตราการเผาผลาญพลังงาน : <strong>{bmr}</strong> (kcal)<br>
									พลังงานที่ใช้กิจกรรมอื่น : <strong>{tdee}</strong> (kcal)<br>
									ปริมาณพลังงานที่ต้องลดต่อวัน : <strong>{calLose}</strong> (kcal)<br>
									ปริมาณพลังงานที่แนะนำต่อวัน : <strong>{calTotal}</strong> (kcal)<br>
								</p>
								<!-- <p>
									เพื่อเป็นการปรับระบบการเผาผลาญของร่างกายให้เร่งการนำโปรตีนมาใช้และลดปริมาณมวลไขมันทั้งหมดเพื่อลดปริมาณแคลอรี่ส่วนเกินที่เป็นอันตรายควรทำตามขั้นตอนดังนี้
								</p>
								<ol class="my-0">
									<li>บริหารร่างกายและทำคาร์ดิให้โอไม่น้อยกว่า 1.5 ชั่วโมงต่อวัน</li>
									<li>จำกัดการกิน ควบคุมอาหาร ห้ามทานยิบย่อยหรือของทานเล่นและควรทานแค่มื้อหลักเท่านั้น</li>
									<li>ทานผักและผลไม้แคลอรี่ต่ำ อาทิ แก้วมังกร ส้ม สตรอเบอร์รี่ บลูเบอร์รี่ เพื่อจำกัดการควบคุมแคลอรี่</li>
									<li>ทาน <a class="js-modals" href="#order-modals"><strong>CLA PLUS</strong></a> เพื่อช่วยในการเร่งการเผาผลาญของร่างกาย เพราะ <a class="js-modals" href="#order-modals"><strong>CLA PLUS</strong></a> มีสารประกอบที่มีส่วนช่วยในการลดน้ำหนักและเร่งกระบวนการเผาผลาญไขมันแม้จะไม่ได้ออกกำลังกายหรือควบคุมอาหาร ดังนั้นควรทาน <a class="js-modals" href="#order-modals"><strong>CLA PLUS</strong></a> ต่อเนื่องอย่าให้ขาดช่วง เพื่อคุมสภาพแคลอรี่ของคุณ</li>
								</ol> -->
							</textarea>
							<div class="result px-3 text-left"></div>
						</div>
					</div>
					<span class="bl-icon bl-icon-close">
						<i class="far fa-times-circle fa-2x"></i>
					</span>
				</section>

				<div class="bl-panel-items" id="bl-panel-work-items">
					<div data-panel="panel-1">
						<div class="result text-center">
							<h2 class="mb-4">BMI</h2>
							<h3 class="mb-4"><strong>0.00</strong></h3>
							<div class="table-responsive mb-4">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th scope="col">BMI kg/m<sup>2</sup></th>
											<th scope="col">ภาวะน้ำหนักตัว</th>
										</tr>
									</thead>
									<tbody class="text-left">
										<tr data-min="0.00" data-max="18.49">
											<td>น้อยกว่า 18.50</td>	
											<td>น้ำหนักต่ำกว่าเกณฑ์</td>
										</tr>
										<tr data-min="18.50" data-max="22.99">
											<td>18.50 - 22.90</td>
											<td>สมส่วน</td>
										</tr>
										<tr data-min="23.00" data-max="24.99">
											<td>23.00 - 24.90</td>
											<td>น้ำหนักเกิน</td>
										</tr>
										<tr data-min="25.00" data-max="29.99">
											<td>25.00 - 29.90</td>
											<td>โรคอ้วน</td>
										</tr>
										<tr data-min="30.00" data-max="149.99">
											<td>มากกว่า 30.00</td>
											<td>โรคอ้วนอันตราย</td>
										</tr>
									</tbody>
								</table>
							</div>
							<p class="my-0">การหาค่าดัชนีมวลกาย (Body Mass Index : BMI) คือเป็นมาตรการที่ใช้ประเมินภาวะอ้วนและผอมในผู้ใหญ่ ตั้งแต่อายุ 20 ปีขึ้นไป สามารถทำได้โดยการชั่งน้ำหนักตัวเป็นกิโลกรัม และวัดส่วนสูงเป็นเซนติเมตร แล้วนำมาหาดัชมีมวลกาย โดยใช้โปรแกรมวัดค่าความอ้วนข้างต้น</p>
						</div>
					</div>
					<div data-panel="panel-2">
						<div class="result text-center">
							<h2 class="mb-4">BMR</h2>
							<h3 class="mb-4"><strong>0</strong></h3>
							<p class="my-0">BMR ย่อมาจาก Basal Metabolic Rate หรือเราสามารถเรียกได้ว่าเป็นอัตราการเผาผลาญพลังงานในแต่ละวัน โดยค่าพลังงานนี้ร่างกายจะใช้ในการขับเคลื่อนระบบและควบคุมอวัยวะต่าง ๆ ในร่างกาย</p>
						</div>
					</div>
					<div data-panel="panel-3">
						<div class="result text-center">
							<p class="mb-4">TDEE</p>
							<h3 class="mb-4"><strong>0</strong></h3>
							<p class="my-0">TDEE คือ Total Daily Energy Expenditure หรือ ค่าของพลังงานที่ใช้กิจกรรมอื่นในแต่ละวัน ค่าที่ออกมาจะได้ค่าของการเผาผลาญพลังงานที่เป็นค่าเพียว ๆ และค่าจากการทำกิจกรรมร่วมกัน</p>
						</div>
					</div>
					<div data-panel="panel-4">
						<div class="result text-center">
							<p class="mb-4">Diet Planning</p>
							<h4 class="mb-4">ผลลัพธ์แคลลอรี่ของคุณคือ</h4>
							<p class="my-0">
								<strong>ปกติ (สุขภาพดี)</strong><br>
								อัตราการเผาผลาญพลังงาน : <strong>0</strong> (kcal)<br>
								พลังงานที่ใช้กิจกรรมอื่น : <strong>0</strong> (kcal)<br>
								ปริมาณพลังงานที่ต้องลดต่อวัน : <strong>0</strong> (kcal)<br>
								ปริมาณพลังงานที่แนะนำต่อวัน : <strong>0</strong> (kcal)<br>
							</p>
						</div>
					</div>
					<nav>
						<form action="https://bookdoctor.asia/leads/<?= $url[array_rand($url)]; ?>/#calorie-result" method="POST">
							<input type="hidden" name="gender" value="male">
							<input type="hidden" name="age" value="28">
							<input type="hidden" name="weight" value="100">
							<input type="hidden" name="goal" value="60">
							<input type="hidden" name="height" value="160">
							<input type="hidden" name="days" value="60">
							<input type="hidden" name="exercise" value="1">
							<button class="btn btn-lg btn-info" type="submit">อ่านเพิ่มเติม</button>
						</form>
					</nav>
				</div>
			</div>
		</div>

		<script type="text/javascript" src="/assets/js/jquery.js"></script>
		<script type="text/javascript" src="/assets/bootstrap4/js/popper.min.js"></script>
		<script type="text/javascript" src="/assets/bootstrap4/js/bootstrap.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
		<script type="text/javascript" src="./page-transitions.js"></script>
		<script>
			$(function() {
				Boxlayout.init();
			});
		</script>
		<script type="text/javascript" src="./calorie.js"></script>
		<!-- <div class="page-transitions">
			<div class="page page-1">
				<header class="mb-4 py-3 text-center">Menu</header>
				<div class="container-fluid">
					<div class="row">
						<div class="col-6">
							<div class="main-icon">
								<a class="my-0">BMI</a>
							</div>
						</div>
						<div class="col-6">
							<div class="main-icon">
								<a class="my-0">TDEE</a>
							</div>
						</div>
					</div>
					<div class="row mt-4">
						<div class="col-6">
							<div class="main-icon">
								<a class="my-0">BMR</a>
							</div>
						</div>
						<div class="col-6">
							<div class="main-icon">
								<a class="my-0">BMR</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="page page-2">
				<div class="mb-4">
					<div class="row">
						<div class="col-lg-6">
							<form class="calorie-form py-4 px-4" action="http://loc.bookdoctor.asia/cla-plus-th/#calorie-form" method="POST">
								<h4 class="my-0 mb-4">โปรแกรมคำนวณแคลลอรี่</h4>
								<div class="row">
									<div class="col-6">
										<div class="mb-4">
											<label>เพศ</label>
											<div class="form-radio">
												<div class="form-check form-check-inline">
													<input type="radio" class="form-check-input" id="gender1" name="gender" value="male" checked="">
													<label class="form-check-label" for="gender1"><i class="fas fa-2x fa-male"></i></label>
												</div>
												<div class="form-check form-check-inline">
													<input type="radio" class="form-check-input" id="gender2" name="gender" value="female">
													<label class="form-check-label" for="gender2"><i class="fas fa-2x fa-female"></i></label>
												</div>
											</div>
										</div>
									</div>
									<div class="col-6">
										<div class="mb-4">
											<label for="age">อายุ</label>
											<input type="tel" class="digits form-control" id="age" name="age" placeholder="" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<div class="mb-4">
											<label for="weight">น้ำหนัก<span class="d-none d-sm-inline"> (กิโลกรัม)</span></label>
											<input type="tel" class="digits form-control" id="weight" name="weight" placeholder="" required>
										</div>
									</div>
									<div class="col-6">
										<div class="mb-4">
											<label for="goal">เป้าหมาย <span class="d-none d-sm-inline">(กิโลกรัม)</span></label>
											<input type="tel" class="digits form-control" id="goal" name="goal" placeholder="" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<div class="mb-4">
											<label for="height">ส่วนสูง<span class="d-none d-sm-inline"> (เซนติเมตร)</span></label>
											<input type="tel" class="digits form-control" id="height" name="height" placeholder="" required>
										</div>
									</div>
									<div class="col-6">
										<div class="mb-4">
											<label for="days">จำนวนวัน</label>
											<input type="tel" class="digits form-control" id="days" name="days" placeholder="" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12">
										<div class="mb-4">
											<label for="exercise">คุณออกกำลังกายบ่อยแค่ไหน</label>
											<select class="select-2 form-control" id="exercise" name="exercise">
												<option value="1.2">ออกกำลังกายน้อยมากหรือไม่ออกเลย</option>
												<option value="1.375">ออกกำลังกาย 1-3 ครั้งต่อสัปดาห์</option>
												<option value="1.55">ออกกำลังกาย 4-5 ครั้งต่อสัปดาห์</option>
												<option value="1.725">อออกกำลังกาย 6-7 ครั้งต่อสัปดาห์</option>
												<option value="1.9">ออกกำลังกายวันละ 2 ครั้งขึ้นไป</option>
											</select>
										</div>
									</div>
								</div>
								<div class="text-center">
									<button type="submit" class="btn btn-info">คำนวณ</button>
								</div>
							</form>
						</div>
						<div class="col-lg-6 col-xl-5">
							<div class="calorie-result mt-4 mt-lg-0">
								<div class="calorie">
									ผลจากเครื่องคำนวณนี้จะเป็นพลังงานที่ต้องลด และ ปริมาณที่ใช้ได้ต่อวันค่ะ แต่ต้องเตือนไว้ว่า ค่าที่ได้เป็นค่าที่ได้จากการคำนวณจากสูตรทางคณิตศาสตร์ ค่ะควรจะยึดไว้เป็นกรอบไว้คร่าวๆนะคะ เพื่อวางแผนการลดน้ำหนักให้มีประสิทธิภาพ เพราะร่างกายแต่ละคนไม่เหมือนกันค่ะ ระยะเวลาและผลลัพธ์อาจต่างกันค่ะ
								</div>
								<div class="fat mt-3 text-center">
									<img src="/assets/images/fat-w-1.jpg">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> -->
	</body>
</html>