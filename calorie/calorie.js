$(function() {
	$(document).on('keypress', '.digits', function (e) {

		var key = window.event ? e.keyCode : e.which;

		if ($(this).hasClass('real') && (e.keyCode == 46 || e.keyCode == 44) &&
			($(this).val().indexOf('.') == -1 && $(this).val().indexOf(',') == -1)) {
			return true;
		}

		if (e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 39 || e.keyCode == 43) {
			return true;
		}
		else return !(key < 48 || key > 57);

	});

	$(document).on('cut copy paste', 'input', function (e) {
		e.preventDefault();
	});

	$(document).on('submit', 'form', function() {

		var thisForm = $(this);
		var formData = $(this).serializeArray();
		var obj = {};
		$.each(formData, function(i, v){
			obj[v.name] = v.value;
		});
		if (obj.gender) {
			obj.gender = 'female';
		} else {
			obj.gender = 'male';
		}

		$.each(obj, function(i, v){
			$('input[type="hidden"][name="' + i + '"]').val(v);
		});

		var s = true;

		$('input[required]', this).each(function() {
			if ($(this).val() == '') {
				s = false;
				$(this).focus();
				return false;
			} else {
				var l = $(this).val().length;
				if ($(this).attr('name') == 'name') {
					if (l < 2 || l > 50) {
						s = false;
						$(this).focus();
						return false;
					}
				} else if ($(this).attr('name') == 'phone') {
					var f = $(this).val().substr(0, 1);
					if (f != '+' && f != '0') {
						s = false;
						$(this).focus();
						return false;
					} else {
						if (f == '+' && (l < 12 || l > 14)) {
							s = false;
							$(this).focus();
							return false;
						} else if (f == '0' && l != 10) {
							s = false;
							$(this).focus();
							return false;
						}
					}
				}
			}
		});
		if (!s) {
			return false;
		}
	});

	if ($('.select-2').length > 0) {
		$('.select-2').select2({
			minimumResultsForSearch: Infinity
		});
	}

		function bmi_calc(g, w, h) {
		var bmi = w / (h * h);
		var res = '/assets/images/fat-';
		if (g == 'male') {
			res += 'm-';
		} else {
			res += 'w-';
		}
		if (bmi < 25) {
			res += '1.jpg';
		} else if (bmi >= 25 && bmi < 30) {
			res += '2.jpg';
		} else if (bmi >= 30 && bmi < 40) {
			res += '3.jpg';
		} else {
			res += '4.jpg';
		}
		return res;
	}

	$(document).on('submit', '.bmi', function() {
		var thisForm = $(this);
		var formData = $(this).serializeArray();
		var obj = {};
		$.each(formData, function(i, v){
			obj[v.name] = v.value;
		});
		var bmi = obj.weight / (obj.height * obj.height) * 10000;
		bmi = bmi.toFixed(2);

		$(this).closest('section').addClass('bl-scale-down');
		$('#bl-panel-work-items').addClass('bl-panel-items-show');
		var $panel = $('#bl-panel-work-items').find("[data-panel='" + $( this ).data( 'panel' ) + "']");
		Boxlayout.currentWorkPanel = $panel.index();
		$panel.addClass('bl-show-work');

		$panel.find('.result table tbody tr').removeClass('bg-success');
		$panel.find('.result h3 strong').prop('Counter', 0).animate({
			Counter: bmi
		}, {
			duration: 1000,
			easing: 'swing',
			step: function (now) {
				$(this).text(now.toFixed(2));
			}
		}).promise().then(function() {
			$panel.find('.result table tbody tr').removeClass('bg-success');
			$panel.find('.result table tbody tr').each(function() {
				if (bmi > $(this).data('min') && bmi < $(this).data('max')) {
					$(this).addClass('bg-success');
				}
			});
		});
		
		return false
		// // var fatLevel = '';
		// // if (bmi < 25) {
		// // 	fatLevel = 'ปกติ (สุขภาพดี)';
		// // } else if (bmi >= 25 && bmi < 30) {
		// // 	fatLevel = 'ท้วม / โรคอ้วนระดับ 1 (อันตรายระดับ 1)';
		// // } else if (bmi >= 30 && bmi < 40) {
		// // 	fatLevel = 'อ้วน / โรคอ้วนระดับ 2 (อันตรายระดับ 2)';
		// // } else {
		// // 	fatLevel = 'อ้วนมาก / โรคอ้วนระดับ 3 (อันตรายระดับ 3)';
		// // }
		// // var bmr = 0
		// // if (obj.gender == 'male') {
		// // 	bmr = Math.round(66 + (13.7 * obj.weight) + (5 * obj.height) - (6.8 * obj.age));
		// // } else {
		// // 	bmr = Math.round(665 + (9.6 * obj.weight) + (1.8 * obj.height) - (4.7 * obj.age));
		// // }
		// // var tdee = Math.round(obj.exercise * bmr);
		// // var calLose = Math.round((6614 * (obj.weight - obj.goal)) / obj.days);
		// // var calTotal = Math.round(tdee - calLose);
		// // var res = $('#calorie-form .calorie-result textarea').val();
		// // res = res.replace('{fatLevel}', fatLevel).replace('{bmr}', bmr).replace('{tdee}', tdee).replace('{calLose}', calLose).replace('{calTotal}', calTotal);
		// // $('#calorie-form .calorie-result .calorie').empty().append(res);
		// return false
	});

	$(document).on('submit', '.bmr', function() {
		var thisForm = $(this);
		var formData = $(this).serializeArray();
		var obj = {};
		$.each(formData, function(i, v){
			obj[v.name] = v.value;
		});
		if (obj.gender) {
			obj.gender = 'female';
		} else {
			obj.gender = 'male';
		}
		var bmr = 0
		if (obj.gender == 'male') {
			bmr = Math.round(66 + (13.7 * obj.weight) + (5 * obj.height) - (6.8 * obj.age));
		} else {
			bmr = Math.round(665 + (9.6 * obj.weight) + (1.8 * obj.height) - (4.7 * obj.age));
		}

		$(this).closest('section').addClass('bl-scale-down');
		$('#bl-panel-work-items').addClass('bl-panel-items-show');
		var $panel = $('#bl-panel-work-items').find("[data-panel='" + $( this ).data( 'panel' ) + "']");
		Boxlayout.currentWorkPanel = $panel.index();
		$panel.addClass('bl-show-work');

		$panel.find('.result h3 strong').prop('Counter', 0).animate({
			Counter: bmr
		}, {
			duration: 1000,
			easing: 'swing',
			step: function (now) {
				$(this).text(now.toFixed(0));
			}
		});
		
		return false
		// // var fatLevel = '';
		// // if (bmi < 25) {
		// // 	fatLevel = 'ปกติ (สุขภาพดี)';
		// // } else if (bmi >= 25 && bmi < 30) {
		// // 	fatLevel = 'ท้วม / โรคอ้วนระดับ 1 (อันตรายระดับ 1)';
		// // } else if (bmi >= 30 && bmi < 40) {
		// // 	fatLevel = 'อ้วน / โรคอ้วนระดับ 2 (อันตรายระดับ 2)';
		// // } else {
		// // 	fatLevel = 'อ้วนมาก / โรคอ้วนระดับ 3 (อันตรายระดับ 3)';
		// // }
		// // var bmr = 0
		// // if (obj.gender == 'male') {
		// // 	bmr = Math.round(66 + (13.7 * obj.weight) + (5 * obj.height) - (6.8 * obj.age));
		// // } else {
		// // 	bmr = Math.round(665 + (9.6 * obj.weight) + (1.8 * obj.height) - (4.7 * obj.age));
		// // }
		// // var tdee = Math.round(obj.exercise * bmr);
		// // var calLose = Math.round((6614 * (obj.weight - obj.goal)) / obj.days);
		// // var calTotal = Math.round(tdee - calLose);
		// // var res = $('#calorie-form .calorie-result textarea').val();
		// // res = res.replace('{fatLevel}', fatLevel).replace('{bmr}', bmr).replace('{tdee}', tdee).replace('{calLose}', calLose).replace('{calTotal}', calTotal);
		// // $('#calorie-form .calorie-result .calorie').empty().append(res);
		// return false
	});

	$(document).on('submit', '.tdee', function() {
		var thisForm = $(this);
		var formData = $(this).serializeArray();
		var obj = {};
		$.each(formData, function(i, v){
			obj[v.name] = v.value;
		});
		if (obj.gender) {
			obj.gender = 'female';
		} else {
			obj.gender = 'male';
		}
		var bmr = 0
		if (obj.gender == 'male') {
			bmr = Math.round(66 + (13.7 * obj.weight) + (5 * obj.height) - (6.8 * obj.age));
		} else {
			bmr = Math.round(665 + (9.6 * obj.weight) + (1.8 * obj.height) - (4.7 * obj.age));
		}
		var tdee = Math.round(obj.exercise * bmr);

		$(this).closest('section').addClass('bl-scale-down');
		$('#bl-panel-work-items').addClass('bl-panel-items-show');
		var $panel = $('#bl-panel-work-items').find("[data-panel='" + $( this ).data( 'panel' ) + "']");
		Boxlayout.currentWorkPanel = $panel.index();
		$panel.addClass('bl-show-work');

		$panel.find('.result h3 strong').prop('Counter', 0).animate({
			Counter: tdee
		}, {
			duration: 1000,
			easing: 'swing',
			step: function (now) {
				$(this).text(now.toFixed(0));
			}
		});
		
		return false
		// // var tdee = Math.round(obj.exercise * bmr);
		// // var calLose = Math.round((6614 * (obj.weight - obj.goal)) / obj.days);
		// // var calTotal = Math.round(tdee - calLose);
		// // var res = $('#calorie-form .calorie-result textarea').val();
		// // res = res.replace('{fatLevel}', fatLevel).replace('{bmr}', bmr).replace('{tdee}', tdee).replace('{calLose}', calLose).replace('{calTotal}', calTotal);
		// // $('#calorie-form .calorie-result .calorie').empty().append(res);
		// return false
	});

	$(document).on('submit', '.diet', function() {
		var thisForm = $(this);
		var formData = $(this).serializeArray();
		var obj = {};
		$.each(formData, function(i, v){
			obj[v.name] = v.value;
		});
		if (obj.gender) {
			obj.gender = 'female';
		} else {
			obj.gender = 'male';
		}
		var bmi = obj.weight / (obj.height * obj.height) * 10000;
		bmi = bmi.toFixed(2);
		var fatLevel = '';
		if (bmi < 25) {
			fatLevel = 'ปกติ (สุขภาพดี)';
		} else if (bmi >= 25 && bmi < 30) {
			fatLevel = 'ท้วม / โรคอ้วนระดับ 1 (อันตรายระดับ 1)';
		} else if (bmi >= 30 && bmi < 40) {
			fatLevel = 'อ้วน / โรคอ้วนระดับ 2 (อันตรายระดับ 2)';
		} else {
			fatLevel = 'อ้วนมาก / โรคอ้วนระดับ 3 (อันตรายระดับ 3)';
		}
		var bmr = 0
		if (obj.gender == 'male') {
			bmr = Math.round(66 + (13.7 * obj.weight) + (5 * obj.height) - (6.8 * obj.age));
		} else {
			bmr = Math.round(665 + (9.6 * obj.weight) + (1.8 * obj.height) - (4.7 * obj.age));
		}
		var tdee = Math.round(obj.exercise * bmr);
		var calLose = Math.round((6614 * (obj.weight - obj.goal)) / obj.days);
		var calTotal = Math.round(tdee - calLose);

		$(this).closest('section').addClass('bl-scale-down');
		$('#bl-panel-work-items').addClass('bl-panel-items-show');
		var $panel = $('#bl-panel-work-items').find("[data-panel='" + $( this ).data( 'panel' ) + "']");
		Boxlayout.currentWorkPanel = $panel.index();
		$panel.addClass('bl-show-work');

		$panel.find('.result p strong:first-child').empty().append(fatLevel);
		$panel.find('.result p strong:eq(1)').prop('Counter', 0).animate({
			Counter: bmr
		}, {
			duration: 1000,
			easing: 'swing',
			step: function (now) {
				$(this).text(now.toFixed(0));
			}
		});
		$panel.find('.result p strong:eq(2)').prop('Counter', 0).animate({
			Counter: tdee
		}, {
			duration: 1000,
			easing: 'swing',
			step: function (now) {
				$(this).text(now.toFixed(0));
			}
		});
		$panel.find('.result p strong:eq(3)').prop('Counter', 0).animate({
			Counter: calLose
		}, {
			duration: 1000,
			easing: 'swing',
			step: function (now) {
				$(this).text(now.toFixed(0));
			}
		});
		$panel.find('.result p strong:eq(4)').prop('Counter', 0).animate({
			Counter: calTotal
		}, {
			duration: 1000,
			easing: 'swing',
			step: function (now) {
				$(this).text(now.toFixed(0));
			}
		});
		// res = res.replace('{fatLevel}', fatLevel).replace('{bmr}', bmr).replace('{tdee}', tdee).replace('{calLose}', calLose).replace('{calTotal}', calTotal);
		// $(this).parent().find('.result').empty().append(res);
		return false;
	});

	$(window).on('load', function() {
		if ($('#calorie-form .calorie-form').length > 0) {
			var submit = true;
			$('#calorie-form .calorie-form input[type="tel"]').each(function() {
				if ($(this).val() == '') {
					submit = false;
				}
			});
			if (submit) {
				$('#calorie-form .calorie-form').submit();
				if (window.location.href.indexOf('#calorie-result') > 0) {
					setTimeout(function(){
						$("html, body").animate({
							scrollTop: $('body').offset().top + "px"
						});
					}, 4500);
				}
			}
		}
	});

	$(document).on('change', '.calorie-form input[name="gender"], .calorie-form input[name="weight"], .calorie-form input[name="height"]', function() {
		var g = $('.calorie-form input[name="gender"]:checked').val();
		var w = $('.calorie-form input[name="weight"]').val();
		var h = $('.calorie-form input[name="height"]').val();
		h = h / 100;
		if (g != '' && w > 0 && h > 0) {
			$('.calorie-result .fat img').attr('src', bmi_calc(g, w, h));
		}
	});

	setTimeout(function(){
		if ($('.js-modals').length > 0) {
			$('.js-modals:first').click();
		} 
	}, 6000);
});