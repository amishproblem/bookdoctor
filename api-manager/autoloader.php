<?php

/** Приоритетные подключения абстрактных классов */
require_once __DIR__ . '/Api/Api.php';
requirePhpFiles(__DIR__ . '/Api/Components');
require_once __DIR__ . '/Api/Plugins/Leadvertex/LeadvertexApi.php';

/** Подключение всех PHP файлов ядра приложения, находящиеся в каталоге "Api" */
requirePhpFiles(__DIR__ . '/Api');


#TODO:
# - пересадить эту либу под композер
# - разделить API вебмастера и админа для лидвертекс на разные классы, убрать такие штуки как "offer,token" и т.д. в конструктор
# - сделать сущности для объектов типа Status и т.д. в Leadvertex
# - использование CURL заменить на Guzzle
# - логирование сделать на основе монолог
# - сделать гит-репо для DEV-версии


function requirePhpFiles($path)
{
    $directoryIterator = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS);
    $iterator = new RecursiveIteratorIterator($directoryIterator, RecursiveIteratorIterator::CHILD_FIRST);
    while ($iterator->valid()) {
        $file = $iterator->current();

        if ($file->getExtension() === 'php') {
            require_once realpath($iterator->key());

            $iterator->next();
            continue;
        }

        $iterator->next();
    }
}