<?php

#TODO проверить трекинг
#TODO интегрировать неймспейсы
#TODO сделать копию проекта, назвать "ShipRocketIntegration", вычистить из неё сторонние плагины и экземплы
#TODO https://apiv2.shiprocket.in/v1/external/inventory - сделать

require_once __DIR__ . '/autoloader.php';

if (DataGuard::notEmptyArray([
    'method' => $_POST['method'],
],
    true
)) {
    try {
        $answer = handle($_POST['method']);
    } catch (Exception $e) {
//        var_dump(json_decode($e->getMessage()));
    }
    var_dump($answer);

    return $answer;
}

function handle($method)
{
    switch ($method) {
        case 'authLogin': return require_once __DIR__ . '/Examples/ShipRocket/ShipRocketAuth.php';
        case 'channels': return require_once __DIR__ . '/Examples/ShipRocket/ShipRocketChannels.php';
        case 'courierTrackAwbs': return require_once __DIR__ . '/Examples/ShipRocket/ShipRocketCourierTrackByAwbs.php';
        case 'courierTrackAwbAwbCode': return require_once __DIR__ . '/Examples/ShipRocket/ShipRocketCourierTrackByAwbCode.php'; # Работает
        case 'courierTrackByOrderIdAndChanelId': return require_once __DIR__ . '/Examples/ShipRocket/ShipRocketCourierTrackByOrderIdAndChanelId.php';
        case 'courierTrackShipmentShipmentId': return require_once __DIR__ . '/Examples/ShipRocket/ShipRocketCourierTrackByShipmentId.php'; # Работает
        case 'ordersCreate': return require_once __DIR__ . '/Examples/ShipRocket/ShipRocketOrdersCreate.php';
        case 'ordersCreateAdhoc': return require_once __DIR__ . '/Examples/ShipRocket/ShipRocketOrdersCreateAdhoc.php'; # Работает
        default: throw new Exception('Нет такого метода!');
    }
}