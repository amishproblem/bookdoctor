<?php
require_once __DIR__ . '/api-manager/autoloader.php';

if (DataGuard::notEmptyArray([
    $_POST['fio'],
    $_POST['phone'],
])){
    $handleApi = new HandleApi();

    $handleApi->url->setUrl('https://dev.leadvertex.ru/api/admin/addOrder.html?token=123test');

    $handleApi->params
        ->setCustomField('fio', $_POST['fio'])
        ->setCustomField('phone', $_POST['phone'])
        ->setCustomField('goods', [
            0 => [
                'goodID' => 41728,
                'quantity' => 2,
                'price' => 999
            ]])
    ;

    $answer = $handleApi->getSender()->setParams($handleApi->params)->sendPost();

    Logger::logInput();
    Logger::logOutput($handleApi);
    Logger::logAnswer($answer, 'Leadvertex');

    Logger::logCustomVar('Просто коммент', $handleApi);

    print_r($answer);
}

