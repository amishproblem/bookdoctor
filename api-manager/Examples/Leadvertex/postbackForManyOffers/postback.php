<?php
require_once __DIR__ . '/api-manager/autoloader.php';

Logger::logInput();

if (DataGuard::notEmptyArray([
    $_POST['id'],
    $_POST['webmasterID'],
    $_POST['offerName'],
])){
    DataGuard::postDataGuard('lv_api_token', 'fsdg234rtfqdf');
    $leadvertexApi = new LeadvertexAdminApi(true);

    $accessOfferList = require_once __DIR__ . '/accessOffers.php';
    if (array_key_exists($_POST['offerName'], $accessOfferList)) {
        $offerName = $_POST['offerName'];
        $token = $accessOfferList[$_POST['offerName']];
    }

    $leadvertexApi->methodManager->updateOrder();
    $leadvertexApi->url->setOfferName($offerName)->setToken($token)->addGetParam('id', $_POST['id']);

    $leadvertexApi->params->setStatus(1);

    $answer = $leadvertexApi->getSender()->setParams($leadvertexApi->params)->sendPost();

    Logger::logOutput($leadvertexApi);
    Logger::logAnswer($answer);

    print_r($answer);
}

