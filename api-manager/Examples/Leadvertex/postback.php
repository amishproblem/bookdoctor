<?php
require_once __DIR__ . '/api-manager/autoloader.php';

Logger::logInput();

if (DataGuard::notEmptyArray([
    $_POST['fio'],
    $_POST['phone'],
])){
    DataGuard::postDataGuard('lv_api_token', 'fsdg234rtfqdf');
    $leadvertexApi = new LeadvertexAdminApi(true);

    $leadvertexApi->methodManager->updateOrder();
    $leadvertexApi->url->setOfferName(OFFER_NAME)->setToken(YOU_API_PASS)->addGetParam('id', ORDER_ID);

    $leadvertexApi->goods->registerGood($leadvertexApi->goodTemplate->setGoodID(GOOD_ID)->setQuantity(1)->setPrice(GOOD_PRICE), Goods::IMPORT);

    $leadvertexApi->params->setStatus(10);

    $answer = $leadvertexApi->getSender()->setParams($leadvertexApi->params)->sendPost();

    Logger::logOutput($leadvertexApi);
    Logger::logAnswer($answer);

    print_r($answer);
}

