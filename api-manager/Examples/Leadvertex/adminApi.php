<?php
require_once __DIR__ . '/api-manager/autoloader.php';

if (DataGuard::notEmptyArray([
    $_POST['fio'],
    $_POST['phone'],
])){
    $leadvertexApi = new LeadvertexAdminApi(true);

    $leadvertexApi->methodManager->addOrder();
    $leadvertexApi->url->setOfferName(OFFER_NAME)->setToken(YOU_API_PASS);

    $leadvertexApi->goods->registerGood($leadvertexApi->goodTemplate->setGoodID(GOOD_ID)->setQuantity(1)->setPrice(GOOD_PRICE), Goods::IMPORT);

    $leadvertexApi->params
        ->setFio( $_POST['fio'])
        ->setPhone($_POST['phone'])
        ->setGoods($leadvertexApi->goods)
    ;

    $answer = $leadvertexApi->getSender()->setParams($leadvertexApi->params)->sendPost();

    Logger::logInput();
    Logger::logOutput($leadvertexApi);
    Logger::logAnswer($answer);

    print_r($answer);

    Redirect::to('success.html');
}

