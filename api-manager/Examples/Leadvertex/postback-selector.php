<?php
require_once __DIR__ . '/api-manager/autoloader.php';

Logger::logInput();
DataGuard::postDataGuard('lv_api_token', 'jyagdfvkasgsdflfy');
if (DataGuard::notEmptyArray([
    $_POST['allowedUrls'],
])) {
    $allowedUrls = explode(', ', $_POST['allowedUrls']);

    if (in_array('https://url-1/index.php', $allowedUrls)) {
        $Api = new HandleApi();

        $Api->url->setUrl('https://url-1/index.php');

        $Api->params
            ->setCustomField('action', $_POST['action'])
            ->setCustomField('order_id', $_POST['order_id'])
            ->setCustomField('hash', $_POST['hash']);

        $answer = $Api->getSender()->setParams($Api->params)->sendPost();

        Logger::logOutput($Api);
        Logger::logAnswer($answer, 'https://url-1/index.php');
    }

    if (in_array('https://url-2/index.php', $allowedUrls)) {
        $Api = new HandleApi();

        $Api->url->setUrl('https://url-2/index.php');

        $Api->params
            ->setCustomField('id', $_POST['id'])
            ->setCustomField('phone', $_POST['phone'])
            ->setCustomField('offerName', $_POST['offerName'])
            ->setCustomField('key', $_POST['key']);

        $answer = $Api->getSender()->setParams($Api->params)->sendPost();

        Logger::logOutput($Api);
        Logger::logAnswer($answer, 'https://url-2/index.php');
    }

    if (in_array('https://url-3/index.php', $allowedUrls)) {
        $Api = new HandleApi();

        $Api->url->setUrl('https://url-3/index.php');

        $Api->params
            ->setCustomField('id', $_POST['id'])
            ->setCustomField('name', $_POST['name'])
            ->setCustomField('phone', $_POST['phone'])
            ->setCustomField('email', $_POST['email'])
            ->setCustomField('price', $_POST['price'])
            ->setCustomField('date', $_POST['date'])
            ->setCustomField('status', $_POST['status'])
            ->setCustomField('goods', $_POST['goods'])
            ->setCustomField('comment', $_POST['comment'])
            ->setCustomField('kazpostTrack', $_POST['kazpostTrack'])
            ->setCustomField('postIndex', $_POST['postIndex'])
            ->setCustomField('baseurl', $_POST['baseurl'])
            ->setCustomField('key', $_POST['key']);

        $answer = $Api->getSender()->setParams($Api->params)->sendPost();

        Logger::logOutput($Api);
        Logger::logAnswer($answer, 'https://url-3/index.php');
    }


}