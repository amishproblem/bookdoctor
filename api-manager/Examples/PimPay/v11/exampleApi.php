<?php
require_once __DIR__ . '/api-manager/autoloader.php';

Logger::logInput();

if (DataGuard::notEmptyArray([
    $_POST['status'],
    $_POST['id'],
    $_POST['phone'],
])){
    $status = (int)$_POST['status'];
    $orderId = (int)$_POST['id'];

    $pimPayPartner = 'CDEK:3eafse-70943gc-4d6c-fgf2-c4cc382cd2a1';
    $pimPaySignature = '2fd5afsgsfhf4f56';

    if ($status === 1) {
        $leadvertexApi = new LeadvertexAdminApi(false);
        $leadvertexApi->methodManager->updateOrder();
        $leadvertexApi->url->setOfferName(OFFER_NAME)->setToken(YOU_API_PASS)->addGetParam('id', $orderId);

        $pimPayApi = new HandleApi();

        $pimPayApi->url->setUrl('https://api.checkclient.ru/v11/recipient');

        $pimPayApi->params
            ->setCustomField('jsonrpc', '2.0')
            ->setCustomField('method', 'Check')
            ->setCustomField('params', ['phone' => $_POST['phone']])
            ->setCustomField('id', DataHelper::generateUUID())
        ;

        $curlSettingsPimPay = new CurlSettings();
        $curlSettingsPimPay->addHeaders([
            "X-SalesMemory-Request-Partner: {$pimPayPartner}",
            "X-SalesMemory-Request-Signature: {$pimPaySignature}",
            'Content-Type' => 'application/json'
        ]);

        $pimPayAnswer = $pimPayApi->getSender()->setParams($pimPayApi->params)->sendPost(true, $curlSettingsPimPay);
        Logger::logOutput($pimPayApi, 'PimPay');
        Logger::logAnswer($pimPayAnswer, 'PimPay');

        $leadvertexApi->params->setAdditional(2, $pimPayAnswer['result']['rating']); // Меняем статус
        $leadvertexAnswer = $leadvertexApi->getSender()->setParams($leadvertexApi->params)->sendPost(false);

        Logger::logOutput($leadvertexApi, 'Leadvertex');
        Logger::logAnswer($leadvertexAnswer,'Leadvertex');

        print_r($leadvertexAnswer);
    }
}





