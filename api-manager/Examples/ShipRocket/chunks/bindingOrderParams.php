<?php
/** @var ShipRocketApi $shipRocketApi */

$orderParams = [
    'name' => $_POST['name'],
    'sku' => $_POST['sku'],
    'units' => $_POST['units'],
    'selling_price' => $_POST['selling_price'],
    'discount' => $_POST['discount'],
    'tax' => $_POST['tax'],
    'hsn' => $_POST['hsn'],
];



$shipRocketApi->params
    ->setOrderId($_POST['order_id'])
    ->setOrderDate($_POST['order_date'])
    ->setPickupLocation($_POST['pickup_location'])
    ->setChannelId($_POST['channel_id'])
    ->setComment($_POST['comment'])
    ->setBillingCustomerName($_POST['billing_customer_name'])
    ->setBillingLastName($_POST['billing_last_name'])
    ->setBillingAddress($_POST['billing_address'])
    ->setBillingAddress2($_POST['billing_address_2'])
    ->setBillingCity($_POST['billing_city'])
;
$shipRocketApi->params
    ->setBillingPincode($_POST['billing_pincode'])
    ->setBillingState($_POST['billing_state'])
    ->setBillingCountry($_POST['billing_country'])
    ->setBillingEmail($_POST['billing_email'])
    ->setBillingPhone($_POST['billing_phone'])
    ->setShippingIsBilling($_POST['shipping_is_billing'])
    ->setShippingCustomerName($_POST['shipping_customer_name'])
    ->setShippingLastName($_POST['shipping_last_name'])
    ->setShippingAddress($_POST['shipping_address'])
    ->setShippingAddress2($_POST['shipping_address_2'])
;
$shipRocketApi->params
    ->setShippingCity($_POST['shipping_city'])
    ->setShippingPincode($_POST['shipping_pincode'])
    ->setShippingCountry($_POST['shipping_country'])
    ->setShippingState($_POST['shipping_state'])
    ->setShippingEmail($_POST['shipping_email'])
    ->setShippingPhone($_POST['shipping_phone'])
    ->setOrderItems($orderParams)
;
$shipRocketApi->params
    ->setPaymentMethod($_POST['payment_method'])
    ->setShippingCharges($_POST['shipping_charges'])
    ->setGiftwrapCharges($_POST['giftwrap_charges'])
    ->setTransactionCharges($_POST['transaction_charges'])
    ->setTotalDiscount($_POST['total_discount'])
    ->setSubTotal($_POST['sub_total'])
    ->setLength($_POST['length'])
    ->setBreadth($_POST['breadth'])
    ->setHeight($_POST['height'])
    ->setWeight($_POST['weight'])
;
