<?php

if (in_array($answer->status_code, [400,401,404,405,422,429,500,502,503,504])) {
    throw new ApiAnswerException(json_encode($answer));
}