<?php
/** @var string $token */
$curlSettings = new CurlSettings();
$curlSettings->addHeaders([
    "Authorization: Bearer {$token}",
    "Content-Type: application/json"
]);

return $curlSettings;