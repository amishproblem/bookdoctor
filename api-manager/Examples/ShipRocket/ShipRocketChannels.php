<?php
require_once __DIR__ . '/../../autoloader.php';

$answer = require_once __DIR__ . '/ShipRocketAuth.php';
$token = $answer->token;

$shipRocketApi = new ShipRocketApi();
$shipRocketApi->methodManager->channels();

/** @var CurlSettings $curlSettings */
$curlSettings = require_once __DIR__ . '/chunks/curlSettings.php';

$answer = $shipRocketApi->getSender()->sendGet(false, $curlSettings);

require __DIR__ . '/chunks/checkApiAnswerException.php';

return $answer;


