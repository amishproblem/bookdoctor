<?php
require_once __DIR__ . '/../../autoloader.php';

if (DataGuard::notEmptyArray([
    'shipment_id' => $_POST['shipment_id'],
],
    true
)){
    $answer = require_once __DIR__ . '/ShipRocketAuth.php';
    $token = $answer->token;

    $shipRocketApi = new ShipRocketApi();
    $shipRocketApi->methodManager->courierTrackShipmentShipmentId();
    $shipRocketApi->url->addPostfixSite("{$_POST['shipment_id']}");

    /** @var CurlSettings $curlSettings */
    $curlSettings = require_once __DIR__ . '/chunks/curlSettings.php';

    $answer = $shipRocketApi->getSender()->sendGet(false, $curlSettings);

    require __DIR__ . '/chunks/checkApiAnswerException.php';

    return $answer;
}

