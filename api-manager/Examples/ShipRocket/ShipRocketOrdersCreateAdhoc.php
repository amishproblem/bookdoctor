<?php
require_once __DIR__ . '/../../autoloader.php';

$guardFields = require_once __DIR__ . '/chunks/orderDataGuardArray.php';

if (DataGuard::notEmptyArray($guardFields, true)) {
    $answer = require_once __DIR__ . '/ShipRocketAuth.php';
    $token = $answer->token;

    $shipRocketApi = new ShipRocketApi();
    $shipRocketApi->methodManager->ordersCreateAdhoc();

    require_once __DIR__ . '/chunks/bindingOrderParams.php';

    /** @var CurlSettings $curlSettings */
    $curlSettings = require_once __DIR__ . '/chunks/curlSettings.php';

    $answer = $shipRocketApi->getSender()->setParams($shipRocketApi->params)->sendPost(false, true, $curlSettings);

    require __DIR__ . '/chunks/checkApiAnswerException.php';

    return $answer;
}

