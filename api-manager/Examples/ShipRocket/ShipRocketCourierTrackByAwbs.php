<?php
require_once __DIR__ . '/../../autoloader.php';

if (DataGuard::notEmptyArray([
    'awbs' => $_POST['awbs'],
],
    true
)){
    $answer = require_once __DIR__ . '/ShipRocketAuth.php';
    $token = $answer->token;

    $shipRocketApi = new ShipRocketApi();
    $shipRocketApi->methodManager->courierTrackAwbs();
    $shipRocketApi->params
        ->setAwbs($_POST['awbs'])
    ;

    /** @var CurlSettings $curlSettings */
    $curlSettings = require_once __DIR__ . '/chunks/curlSettings.php';

    $answer = $shipRocketApi->getSender()->setParams($shipRocketApi->params)->sendPost(false, true, $curlSettings);

    require __DIR__ . '/chunks/checkApiAnswerException.php';

    return $answer;
}

