<?php
require_once __DIR__ . '/../../autoloader.php';

if (DataGuard::notEmptyArray([
    'email' => $_POST['email'],
    'password' => $_POST['password'],
],
    true
)){
    $shipRocketApiAuth = new ShipRocketApi();
    $shipRocketApiAuth->methodManager->authLogin();
    $shipRocketApiAuth->params
        ->setEmail($_POST['email'])
        ->setPassword($_POST['password'])
    ;

    $answer = $shipRocketApiAuth->getSender()->setParams($shipRocketApiAuth->params)->sendPost();

    require __DIR__ . '/chunks/checkApiAnswerException.php';

    return $answer;
}

