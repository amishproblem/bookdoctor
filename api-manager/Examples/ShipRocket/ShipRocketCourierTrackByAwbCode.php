<?php
require_once __DIR__ . '/../../autoloader.php';

if (DataGuard::notEmptyArray([
    'awb_code' => $_POST['awb_code'],
],
    true
)){
    $answer = require_once __DIR__ . '/ShipRocketAuth.php';
    $token = $answer->token;

    $shipRocketApi = new ShipRocketApi();
    $shipRocketApi->methodManager->courierTrackAwbAwbCode();
    $shipRocketApi->url->addPostfixSite("{$_POST['awb_code']}");

    /** @var CurlSettings $curlSettings */
    $curlSettings = require_once __DIR__ . '/chunks/curlSettings.php';

    $answer = $shipRocketApi->getSender()->sendGet(false, $curlSettings);

    require __DIR__ . '/chunks/checkApiAnswerException.php';

    return $answer;
}

