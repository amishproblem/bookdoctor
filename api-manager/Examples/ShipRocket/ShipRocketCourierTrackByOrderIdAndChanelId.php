<?php
require_once __DIR__ . '/../../autoloader.php';

if (DataGuard::notEmptyArray([
    'order_id' => $_POST['order_id'],
],
    true
)){
    $answer = require_once __DIR__ . '/ShipRocketAuth.php';
    $token = $answer->token;

    $shipRocketApi = new ShipRocketApi();
    $shipRocketApi->methodManager->courierTrack();
    $shipRocketApi->url->addPostfixSite("?order_id={$_POST['order_id']}");

    if (!empty($_POST['channel_id'])) {
        $shipRocketApi->url->addGetParam('channel_id', $_POST['channel_id']);
    }

    /** @var CurlSettings $curlSettings */
    $curlSettings = require_once __DIR__ . '/chunks/curlSettings.php';

    $answer = $shipRocketApi->getSender()->sendGet(false, $curlSettings);

    require __DIR__ . '/chunks/checkApiAnswerException.php';

    return $answer;
}

