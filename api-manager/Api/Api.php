<?php

/**
 * Class Api
 * Created by Shaman
 */
abstract class Api
{

    /** @var Url */
    public $url;

    /** @var Params */
    public $params;

    /** @var MethodManager */
    public $methodManager;

    /** @var Sender */
    public $sender;

    /** @var bool */
    protected $debug;

    public function __construct($debug = false)
    {
        $this->debug = $debug;
        $this->setParams();
        $this->setMethodManager();
    }

    public function setMethod($method)
    {
        $this->url->setMethod($method);
    }

    public function getSender()
    {
        if (empty($this->sender)) {
            $this->sender = new Sender($this->url, $this->debug);
        }

        return $this->sender;
    }

    /**
     * Данный метод можно переопределить на кастомный
     */
    protected function setParams()
    {
        $this->params = new Params();
    }

    abstract protected function setMethodManager();
    abstract protected function setUrl();

}