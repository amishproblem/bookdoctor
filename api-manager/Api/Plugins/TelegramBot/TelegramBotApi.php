<?php


class TelegramBotApi extends Api
{

    /** @var TelegramBotUrl */
    public $url;

    /** @var TelegramBotMethodManager */
    public $methodManager;

    /** @var TelegramBotParams */
    public $params;

    public function __construct($debug = false)
    {
        parent::__construct($debug);
        $this->setUrl();
    }

    protected function setMethodManager()
    {
        $this->methodManager = new TelegramBotMethodManager($this);
    }

    protected function setUrl()
    {
        $this->url = new TelegramBotUrl();
    }

    protected function setParams()
    {
        $this->params = new TelegramBotParams();
    }
}