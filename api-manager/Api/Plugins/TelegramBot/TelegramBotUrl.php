<?php


class TelegramBotUrl extends Url
{

    private $method;
    private $token;

    public function __construct($method = null)
    {
        $this->method = $method;
    }

    /**
     * @param string $token
     * @return TelegramBotUrl
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @param string $method
     * @return TelegramBotUrl
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @param string $url
     * @return TelegramBotUrl
     */
    public function setUrl($url)
    {
        $this->prepareUrl = $url;
        return $this;
    }

    protected function prepareUrl()
    {
        if (empty($this->method) or empty($this->token)) {
            return null;
        }

        $this->prepareUrl = "https://api.telegram.org/bot{$this->token}/{$this->method}";

        return $this;
    }
}