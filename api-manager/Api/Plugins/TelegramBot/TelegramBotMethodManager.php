<?php

/**
 * Class TelegramBotMethodManager
 * Документация: https://tlgrm.ru/docs/bots/api
 */
class TelegramBotMethodManager extends MethodManager
{

    const SEND_MESSAGE = "sendMessage";
    const GET_ME = "getMe";
    const FORWARD_MESSAGE = "forwardMessage";
    const SEND_PHOTO = "sendPhoto";
    const SEND_AUDIO = "sendAudio";
    const GET_UPDATES = "getUpdates";
    const UPDATE = "Update";
    const SET_WEBHOOK = "setWebhook";
    const GET_WEBHOOK_INFO = "getWebhookInfo";
    const SEND_DOCUMENT = "sendDocument";
    const SEND_STICKER = "sendSticker";
    const SEND_VIDEO = "sendVideo";
    const SEND_VOICE = "sendVoice";
    const SEND_LOCATION = "sendLocation";
    const SEND_VENUE = "sendVenue";
    const SEND_CONTACT = "sendContact";
    const SEND_CHAT_ACTION = "sendChatAction";
    const GET_USER_PROFILE_PHOTOS = "getUserProfilePhotos";
    const GET_FILE = "getFile";
    const KICK_CHAT_MEMBER = "kickChatMember";
    const UNBAN_CHAT_MEMBER = "unbanChatMember";
    const ANSWER_CALLBACK_QUERY = "answerCallbackQuery";
    const EDIT_MESSAGE_TEXT = "editMessageText";
    const EDIT_MESSAGE_CAPTION = "editMessageCaption";
    const EDIT_MESSAGE_REPLY_MARKUP = "editMessageReplyMarkup";

    /**
     * @return mixed
     * Отправляет сообщение указанному пользователю.
     *
     * Обязательные параметры:
     *  - chat_id: ID чата, куда должно быть отправлено сообщение
     *  - text: Текст сообщения, которое будет отправлено
     */
    public function sendMessage()
    {
        return $this->handle(self::SEND_MESSAGE);
    }

    /**
     * @return mixed
     * Возвращает информацию о боте (ID, Имя и прочее)
     */
    public function getMe()
    {
        return $this->handle(self::GET_ME);
    }

    /**
     * @return mixed
     * Переслать сообщение любого типа(которое уже было ранее отправлено)
     *
     * Обязательные параметры:
     *  - chat_id: ID чата, куда должно быть отправлено сообщение
     *  - from_chat_id: ID чата, в которое было отправлено исходное сообщение
     *  - message_id: ID сообщения, которое было отправлено
     */
    public function forwardMessage()
    {
        return $this->handle(self::FORWARD_MESSAGE);
    }

    /**
     * @return mixed
     * Используйте этот метод для отправки фотографий. При успешном выполнении возвращается отправленное сообщение.
     *
     * Обязательные параметры:
     *  - chat_id: ID чата, куда должно быть отправлено сообщение
     *  - photo: Фото для отправки. Вы можете либо передать file_id в виде строки для повторной отправки фотографии,
     * которая уже находится на серверах Telegram, либо загрузить новую фотографию с помощью multipart/form-data.
     */
    public function sendPhoto()
    {
        return $this->handle(self::SEND_PHOTO);
    }

    /**
     * @return mixed
     * Обязательные параметры:
     *  - chat_id: ID чата, куда должно быть отправлено сообщение
     *  - audio: Аудиофайл для отправки. Вы можете либо передать file_id в виде строки для повторной отправки аудио,
     * которое уже находится на серверах Telegram, либо загрузить новый аудиофайл с помощью multipart/form-data.
     */
    public function sendAudio()
    {
        return $this->handle(self::SEND_AUDIO);
    }

    /**
     * @return mixed
     * Этот метод используется для получения обновлений через long polling (wiki). Ответ возвращается в виде массива объектов Update.
     */
    public function getUpdates()
    {
        return $this->handle(self::GET_UPDATES);
    }

    /**
     * @return mixed
     * Этот объект представляет из себя входящее обновление. Под обновлением подразумевается действие, совершённое с
     * ботом — например, получение сообщения от пользователя.
     *
     * Обязательные параметры:
     *  - update_id: Уникальный идентификатор обновления. Обновление идентификаторов начинается с определенного
     * положительного числа и последовательно увеличивается.
     */
    public function Update()
    {
        return $this->handle(self::UPDATE);
    }

    /**
     * @return mixed
     * Этот метод необходим для задания URL вебхука, на который бот будет отправлять обновления. Каждый раз при получении
     * обновления на этот адрес будет отправлен HTTPS POST с сериализованным в JSON объектом Update. При неудачном запросе
     * к вашему серверу попытка будет повторена умеренное число раз.
     *
     * Обязательные параметры:
     * - url: HTTPS url для отправки запросов. Чтобы удалить вебхук, отправьте пустую строку.
     */
    public function setWebhook()
    {
        return $this->handle(self::SET_WEBHOOK);
    }

    /**
     * @return mixed
     * Содержит информацию о текущем состоянии вебхука.
     */
    public function getWebhookInfo()
    {
        return $this->handle(self::GET_WEBHOOK_INFO);
    }

    /**
     * @return mixed
     * Обязательные параметры:
     *  - chat_id: ID чата, куда должно быть отправлено сообщение
     *  - document: Файл для отправки. Вы можете либо передать file_id в виде строки для повторной отправки файла,
     * который уже находится на серверах Telegram, либо загрузить новый файл с помощью multipart/form-data.
     */
    public function sendDocument()
    {
        return $this->handle(self::SEND_DOCUMENT);
    }

    /**
     * @return mixed
     * Обязательные параметры:
     *  - chat_id: ID чата, куда должно быть отправлено сообщение
     *  - sticker: Наклейка для отправки. Вы можете либо передать идентификатор file_id в виде строки для повторной
     * отправки стикера, который уже находится на серверах Telegram, либо загрузить новый стикер с помощью multipart/form-data.
     */
    public function sendSticker()
    {
        return $this->handle(self::SEND_STICKER);
    }

    /**
     * @return mixed
     * Обязательные параметры:
     *  - chat_id: ID чата, куда должно быть отправлено сообщение
     *  - video: Видео для отправки. Вы можете либо передать file_id в виде строки для повторной отправки видео,
     * которое уже находится на серверах Telegram, либо загрузить новый видеофайл с помощью multipart/form-data.
     */
    public function sendVideo()
    {
        return $this->handle(self::SEND_VIDEO);
    }

    /**
     * @return mixed
     * Обязательные параметры:
     *  - chat_id: ID чата, куда должно быть отправлено сообщение
     *  - voice: Аудиофайл для отправки. Вы можете либо передать file_id в виде строки для повторной отправки аудио,
     * которое уже находится на серверах Telegram, либо загрузить новый аудиофайл с помощью multipart/form-data.
     */
    public function sendVoice()
    {
        return $this->handle(self::SEND_VOICE);
    }

    public function sendLocation()
    {
        return $this->handle(self::SEND_LOCATION);
    }

    public function sendVenue()
    {
        return $this->handle(self::SEND_VENUE);
    }

    public function sendContact()
    {
        return $this->handle(self::SEND_CONTACT);
    }

    public function sendChatAction()
    {
        return $this->handle(self::SEND_CHAT_ACTION);
    }

    public function getUserProfilePhotos()
    {
        return $this->handle(self::GET_USER_PROFILE_PHOTOS);
    }

    public function getFile()
    {
        return $this->handle(self::GET_FILE);
    }

    public function kickChatMember()
    {
        return $this->handle(self::KICK_CHAT_MEMBER);
    }

    public function unbanChatMember()
    {
        return $this->handle(self::UNBAN_CHAT_MEMBER);
    }

    public function answerCallbackQuery()
    {
        return $this->handle(self::ANSWER_CALLBACK_QUERY);
    }

    public function editMessageText()
    {
        return $this->handle(self::EDIT_MESSAGE_TEXT);
    }

    public function editMessageCaption()
    {
        return $this->handle(self::EDIT_MESSAGE_CAPTION);
    }

    public function editMessageReplyMarkup()
    {
        return $this->handle(self::EDIT_MESSAGE_REPLY_MARKUP);
    }

}