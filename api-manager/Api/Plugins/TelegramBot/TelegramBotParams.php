<?php


class TelegramBotParams extends Params
{
    /**
     * @param $text
     * @return $this
     * Текст для отправки в чат
     */
    public function setText($text)
    {
        $this->params['text'] = $text;
        return $this;
    }


    /**
     * @param $chatId
     * @return $this
     * ID чата, куда должно быть отправлено сообщение
     */
    public function setChatId($chatId)
    {
        $this->params['chat_id'] = $chatId;
        return $this;
    }

    /**
     * @param $fromChatId
     * @return $this
     * ID чата, в которое было отправлено исходное сообщение
     */
    public function setFromChatId($fromChatId)
    {
        $this->params['from_chat_id'] = $fromChatId;
        return $this;
    }

    /**
     * @param $messageId
     * @return $this
     * ID сообщения, которое было отправлено
     */
    public function setMessageId($messageId)
    {
        $this->params['message_id'] = $messageId;
        return $this;
    }

    /**
     * @param $photo
     * @return $this
     * Фото для отправки. Вы можете либо передать file_id в виде строки для повторной отправки фотографии,
     * которая уже находится на серверах Telegram, либо загрузить новую фотографию с помощью multipart/form-data.
     */
    public function setPhoto($photo)
    {
        $this->params['photo'] = $photo;
        return $this;
    }

    /**
     * @param $audio
     * @return $this
     * Аудиофайл для отправки. Вы можете либо передать file_id в виде строки для повторной отправки аудио,
     * которое уже находится на серверах Telegram, либо загрузить новый аудиофайл с помощью multipart/form-data.
     */
    public function setAudio($audio)
    {
        $this->params['audio'] = $audio;
        return $this;
    }

    /**
     * @param $updateId
     * @return $this
     * The update‘s unique identifier. Update identifiers start from a certain positive number and increase sequentially.
     * This ID becomes especially handy if you’re using Webhooks, since it allows you to ignore repeated updates or to
     * restore the correct update sequence, should they get out of order.
     */
    public function setUpdateId($updateId)
    {
        $this->params['update_id'] = $updateId;
        return $this;
    }
    public function setMessage($message)
    {
        $this->params['message'] = $message;
        return $this;
    }

    public function setInlineQuery($inlineQuery)
    {
        $this->params['inline_query'] = $inlineQuery;
        return $this;
    }

    public function setChosenInlineResult($chosenInlineResult)
    {
        $this->params['chosen_inline_result'] = $chosenInlineResult;
        return $this;
    }

    public function setCallbackQuery($callbackQuery)
    {
        $this->params['callback_query'] = $callbackQuery;
        return $this;
    }

    public function setOffset($offset)
    {
        $this->params['offset'] = $offset;
        return $this;
    }

    public function setLimit($limit)
    {
        $this->params['limit'] = $limit;
        return $this;
    }

    public function setTimeout($timeout)
    {
        $this->params['timeout'] = $timeout;
        return $this;
    }

    public function setUrl($url)
    {
        $this->params['url'] = $url;
        return $this;
    }

    public function setCertificate($certificate)
    {
        $this->params['certificate'] = $certificate;
        return $this;
    }

    public function setHasCustomCertificate($hasCustomCertificate)
    {
        $this->params['has_custom_certificate'] = $hasCustomCertificate;
        return $this;
    }

    public function setPendingUpdateCount($pendingUpdateCount)
    {
        $this->params['pending_update_count'] = $pendingUpdateCount;
        return $this;
    }

    public function setLastErrorDate($lastErrorDate)
    {
        $this->params['last_error_date'] = $lastErrorDate;
        return $this;
    }

    public function setLastErrorMessage($lastErrorMessage)
    {
        $this->params['last_error_message'] = $lastErrorMessage;
        return $this;
    }

    public function setId($id)
    {
        $this->params['id'] = $id;
        return $this;
    }

    public function setFirstName($firstName)
    {
        $this->params['first_name'] = $firstName;
        return $this;
    }

    public function setLastName($lastName)
    {
        $this->params['last_name'] = $lastName;
        return $this;
    }

    public function setUsername($username)
    {
        $this->params['username'] = $username;
        return $this;
    }

    public function setType($type)
    {
        $this->params['type'] = $type;
        return $this;
    }

    public function setTitle($title)
    {
        $this->params['title'] = $title;
        return $this;
    }

    public function setAllMembersAreAdministrators($allMembersAreAdministrators)
    {
        $this->params['all_members_are_administrators'] = $allMembersAreAdministrators;
        return $this;
    }

    public function setFrom($from)
    {
        $this->params['from'] = $from;
        return $this;
    }

    public function setDate($date)
    {
        $this->params['date'] = $date;
        return $this;
    }

    public function setChat($chat)
    {
        $this->params['chat'] = $chat;
        return $this;
    }

    public function setForwardFrom($forwardFrom)
    {
        $this->params['forward_from'] = $forwardFrom;
        return $this;
    }

    public function setForwardDate($forwardDate)
    {
        $this->params['forward_date'] = $forwardDate;
        return $this;
    }

    public function setReplyToMessage($replyToMessage)
    {
        $this->params['reply_to_message'] = $replyToMessage;
        return $this;
    }

    public function setEntities($entities)
    {
        $this->params['entities'] = $entities;
        return $this;
    }

    public function setDocument($document)
    {
        $this->params['document'] = $document;
        return $this;
    }

    public function setSticker($sticker)
    {
        $this->params['sticker'] = $sticker;
        return $this;
    }

    public function setVideo($video)
    {
        $this->params['video'] = $video;
        return $this;
    }

    public function setVoice($voice)
    {
        $this->params['voice'] = $voice;
        return $this;
    }

    public function setCaption($caption)
    {
        $this->params['caption'] = $caption;
        return $this;
    }

    public function setContact($contact)
    {
        $this->params['contact'] = $contact;
        return $this;
    }

    public function setLocation($location)
    {
        $this->params['location'] = $location;
        return $this;
    }

    public function setVenue($venue)
    {
        $this->params['venue'] = $venue;
        return $this;
    }

    public function setNewChatMember($newChatMember)
    {
        $this->params['new_chat_member'] = $newChatMember;
        return $this;
    }

    public function setLeftChatMember($leftChatMember)
    {
        $this->params['left_chat_member'] = $leftChatMember;
        return $this;
    }

    public function setNewChatTitle($newChatTitle)
    {
        $this->params['new_chat_title'] = $newChatTitle;
        return $this;
    }

    public function setNewChatPhoto($newChatPhoto)
    {
        $this->params['new_chat_photo'] = $newChatPhoto;
        return $this;
    }

    public function setDeleteChatPhoto($deleteChatPhoto)
    {
        $this->params['delete_chat_photo'] = $deleteChatPhoto;
        return $this;
    }

    public function setGroupChatCreated($groupChatCreated)
    {
        $this->params['group_chat_created'] = $groupChatCreated;
        return $this;
    }

    public function setSupergroupChatCreated($supergroupChatCreated)
    {
        $this->params['supergroup_chat_created'] = $supergroupChatCreated;
        return $this;
    }

    public function setChannelChatCreated($channelChatCreated)
    {
        $this->params['channel_chat_created'] = $channelChatCreated;
        return $this;
    }

    public function setMigrateToChatId($migrateToChatId)
    {
        $this->params['migrate_to_chat_id'] = $migrateToChatId;
        return $this;
    }

    public function setMigrateFromChatId($migrateFromChatId)
    {
        $this->params['migrate_from_chat_id'] = $migrateFromChatId;
        return $this;
    }

    public function setPinnedMessage($pinnedMessage)
    {
        $this->params['pinned_message'] = $pinnedMessage;
        return $this;
    }

    public function setLength($length)
    {
        $this->params['length'] = $length;
        return $this;
    }

    public function setFileId($fileId)
    {
        $this->params['file_id'] = $fileId;
        return $this;
    }

    public function setWidth($width)
    {
        $this->params['width'] = $width;
        return $this;
    }

    public function setHeight($height)
    {
        $this->params['height'] = $height;
        return $this;
    }

    public function setFileSize($fileSize)
    {
        $this->params['file_size'] = $fileSize;
        return $this;
    }

    public function setDuration($duration)
    {
        $this->params['duration'] = $duration;
        return $this;
    }

    public function setPerformer($performer)
    {
        $this->params['performer'] = $performer;
        return $this;
    }

    public function setMimeType($mimeType)
    {
        $this->params['mime_type'] = $mimeType;
        return $this;
    }

    public function setThumb($thumb)
    {
        $this->params['thumb'] = $thumb;
        return $this;
    }

    public function setFileName($fileName)
    {
        $this->params['file_name'] = $fileName;
        return $this;
    }

    public function setPhoneNumber($phoneNumber)
    {
        $this->params['phone_number'] = $phoneNumber;
        return $this;
    }

    public function setUserId($userId)
    {
        $this->params['user_id'] = $userId;
        return $this;
    }

    public function setLongitude($longitude)
    {
        $this->params['longitude'] = $longitude;
        return $this;
    }

    public function setLatitude($latitude)
    {
        $this->params['latitude'] = $latitude;
        return $this;
    }

    public function setFoursquareId($foursquareId)
    {
        $this->params['foursquare_id'] = $foursquareId;
        return $this;
    }

    public function setTotalCount($totalCount)
    {
        $this->params['total_count'] = $totalCount;
        return $this;
    }

    public function setPhotos($photos)
    {
        $this->params['photos'] = $photos;
        return $this;
    }

    public function setFilePath($filePath)
    {
        $this->params['file_path'] = $filePath;
        return $this;
    }

    public function setKeyboard($keyboard)
    {
        $this->params['keyboard'] = $keyboard;
        return $this;
    }

    public function setResizeKeyboard($resizeKeyboard)
    {
        $this->params['resize_keyboard'] = $resizeKeyboard;
        return $this;
    }

    public function setOneTimeKeyboard($oneTimeKeyboard)
    {
        $this->params['one_time_keyboard'] = $oneTimeKeyboard;
        return $this;
    }

    public function setSelective($selective)
    {
        $this->params['selective'] = $selective;
        return $this;
    }

    public function setRequestContact($requestContact)
    {
        $this->params['request_contact'] = $requestContact;
        return $this;
    }

    public function setRequestLocation($requestLocation)
    {
        $this->params['request_location'] = $requestLocation;
        return $this;
    }

    public function setSwitchInlineQuery($switchInlineQuery)
    {
        $this->params['switch_inline_query'] = $switchInlineQuery;
        return $this;
    }

    public function setSwitchInlineQueryCurrentChat($switchInlineQueryCurrentChat)
    {
        $this->params['switch_inline_query_current_chat'] = $switchInlineQueryCurrentChat;
        return $this;
    }

    public function setCallbackGame($callbackGame)
    {
        $this->params['callback_game'] = $callbackGame;
        return $this;
    }

    public function setData($data)
    {
        $this->params['data'] = $data;
        return $this;
    }

    public function setForceReply($forceReply)
    {
        $this->params['force_reply'] = $forceReply;
        return $this;
    }

    public function setRetryAfter($retryAfter)
    {
        $this->params['retry_after'] = $retryAfter;
        return $this;
    }

    public function setParseMode($parseMode)
    {
        $this->params['parse_mode'] = $parseMode;
        return $this;
    }

    public function setDisableWebPagePreview($disableWebPagePreview)
    {
        $this->params['disable_web_page_preview'] = $disableWebPagePreview;
        return $this;
    }

    public function setDisableNotification($disableNotification)
    {
        $this->params['disable_notification'] = $disableNotification;
        return $this;
    }

    public function setReplyToMessageId($replyToMessageId)
    {
        $this->params['reply_to_message_id'] = $replyToMessageId;
        return $this;
    }

    public function setReplyMarkup($replyMarkup)
    {
        $this->params['reply_markup'] = $replyMarkup;
        return $this;
    }

    public function setAction($action)
    {
        $this->params['action'] = $action;
        return $this;
    }

    public function setShowAlert($showAlert)
    {
        $this->params['show_alert'] = $showAlert;
        return $this;
    }

    public function setQuery($query)
    {
        $this->params['query'] = $query;
        return $this;
    }

    public function setThumbWidth($thumbWidth)
    {
        $this->params['thumb_width'] = $thumbWidth;
        return $this;
    }

    public function setThumbHeight($thumbHeight)
    {
        $this->params['thumb_height'] = $thumbHeight;
        return $this;
    }

    public function setEditMessage($editMessage)
    {
        $this->params['edit_message'] = $editMessage;
        return $this;
    }

    public function setScore($score)
    {
        $this->params['score'] = $score;
        return $this;
    }

}