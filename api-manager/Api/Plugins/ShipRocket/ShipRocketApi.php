<?php


class ShipRocketApi extends Api
{

    /** @var ShipRocketUrl */
    public $url;

    /** @var ShipRocketMethodManager */
    public $methodManager;

    /** @var ShipRocketParams */
    public $params;

    public function __construct($debug = false)
    {
        parent::__construct($debug);
        $this->setUrl();
    }

    protected function setMethodManager()
    {
        $this->methodManager = new ShipRocketMethodManager($this);
    }

    protected function setUrl()
    {
        $this->url = new ShipRocketUrl();
    }

    protected function setParams()
    {
        $this->params = new ShipRocketParams();
    }
}