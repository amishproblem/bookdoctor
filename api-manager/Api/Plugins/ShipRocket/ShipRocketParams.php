<?php


class ShipRocketParams extends Params
{
    public function setEmail($email)
    {
        $this->params['email'] = (!empty($email)) ? $email : "";
        return $this;
    }

    public function setPassword($password)
    {
        $this->params['password'] = (!empty($password)) ? $password : "";
        return $this;
    }

    public function setOrderId($orderId)
    {
        $this->params['order_id'] = (!empty($orderId)) ? $orderId : "";
        return $this;
    }

    public function setOrderDate($orderDate)
    {
        $this->params['order_date'] = (!empty($orderDate)) ? $orderDate : "";
        return $this;
    }

    public function setPickupLocation($pickupLocation)
    {
        $this->params['pickup_location'] = (!empty($pickupLocation)) ? $pickupLocation : "";
        return $this;
    }

    public function setChannelId($channelId)
    {
        $this->params['channel_id'] = (!empty($channelId)) ? $channelId : "";
        return $this;
    }

    public function setComment($comment)
    {
        $this->params['comment'] = (!empty($comment)) ? $comment : "";
        return $this;
    }

    public function setBillingCustomerName($billingCustomerName)
    {
        $this->params['billing_customer_name'] = (!empty($billingCustomerName)) ? $billingCustomerName : "";
        return $this;
    }

    public function setBillingLastName($billingLastName)
    {
        $this->params['billing_last_name'] = (!empty($billingLastName)) ? $billingLastName : "";
        return $this;
    }

    public function setBillingAddress($billingAddress)
    {
        $this->params['billing_address'] = (!empty($billingAddress)) ? $billingAddress : "";
        return $this;
    }

    public function setBillingAddress2($billingAddress2)
    {
        $this->params['billing_address_2'] = (!empty($billingAddress2)) ? $billingAddress2 : "";
        return $this;
    }

    public function setBillingCity($billingCity)
    {
        $this->params['billing_city'] = (!empty($billingCity)) ? $billingCity : "";
        return $this;
    }

    public function setBillingPincode($billingPincode)
    {
        $this->params['billing_pincode'] = (!empty($billingPincode)) ? $billingPincode : "";
        return $this;
    }

    public function setBillingState($billingState)
    {
        $this->params['billing_state'] = (!empty($billingState)) ? $billingState : "";
        return $this;
    }

    public function setBillingCountry($billingCountry)
    {
        $this->params['billing_country'] = (!empty($billingCountry)) ? $billingCountry : "";
        return $this;
    }

    public function setBillingEmail($billingEmail)
    {
        $this->params['billing_email'] = (!empty($billingEmail)) ? $billingEmail : "";
        return $this;
    }

    public function setBillingPhone($billingPhone)
    {
        $this->params['billing_phone'] = (!empty($billingPhone)) ? $billingPhone : "";
        return $this;
    }

    public function setShippingIsBilling($shippingIsBilling)
    {
        $this->params['shipping_is_billing'] = (!empty($shippingIsBilling)) ? $shippingIsBilling : "";
        return $this;
    }

    public function setShippingCustomerName($shippingCustomerName)
    {
        $this->params['shipping_customer_name'] = (!empty($shippingCustomerName)) ? $shippingCustomerName : "";
        return $this;
    }

    public function setShippingLastName($shippingLastName)
    {
        $this->params['shipping_last_name'] = (!empty($shippingLastName)) ? $shippingLastName : "";
        return $this;
    }

    public function setShippingAddress($shippingAddress)
    {
        $this->params['shipping_address'] = (!empty($shippingAddress)) ? $shippingAddress : "";
        return $this;
    }

    public function setShippingAddress2($shippingAddress2)
    {
        $this->params['shipping_address_2'] = (!empty($shippingAddress2)) ? $shippingAddress2 : "";
        return $this;
    }

    public function setShippingCity($shippingCity)
    {
        $this->params['shipping_city'] = (!empty($shippingCity)) ? $shippingCity : "";
        return $this;
    }

    public function setShippingPincode($shippingPincode)
    {
        $this->params['shipping_pincode'] = (!empty($shippingPincode)) ? $shippingPincode : "";
        return $this;
    }

    public function setShippingCountry($shippingCountry)
    {
        $this->params['shipping_country'] = (!empty($shippingCountry)) ? $shippingCountry : "";
        return $this;
    }

    public function setShippingState($shippingState)
    {
        $this->params['shipping_state'] = (!empty($shippingState)) ? $shippingState : "";
        return $this;
    }

    public function setShippingEmail($shippingEmail)
    {
        $this->params['shipping_email'] = (!empty($shippingEmail)) ? $shippingEmail : "";
        return $this;
    }

    public function setShippingPhone($shippingPhone)
    {
        $this->params['shipping_phone'] = (!empty($shippingPhone)) ? $shippingPhone : "";
        return $this;
    }

    public function setOrderItems($orderItems)
    {

        $orderData['name'] = (!empty($orderItems['name'])) ? $orderItems['name'] : "";

        $orderData['sku'] = (!empty($orderItems['sku'])) ? $orderItems['sku'] : "";
        $orderData['units'] = (!empty($orderItems['units'])) ? $orderItems['units'] : "";
        $orderData['selling_price'] = (!empty($orderItems['selling_price'])) ? $orderItems['selling_price'] : "";
        $orderData['discount'] = (!empty($orderItems['discount'])) ? $orderItems['discount'] : "";
        $orderData['tax'] = (!empty($orderItems['tax'])) ? $orderItems['tax'] : "";
        $orderData['hsn'] = (!empty($orderItems['hsn'])) ? $orderItems['hsn'] : "";

        $this->params['order_items'] = [$orderData];
//        $this->params['order_items']['name'] = $orderItems['name'];
//        $this->params['order_items']['sku'] = $orderItems['sku'];
//        $this->params['order_items']['units'] = $orderItems['units'];
//        $this->params['order_items']['selling_price'] = $orderItems['selling_price'];
//        $this->params['order_items']['discount'] = $orderItems['discount'];
//        $this->params['order_items']['tax'] = $orderItems['tax'];
//        $this->params['order_items']['hsn'] = $orderItems['hsn'];
        return $this;
    }

    public function setName($name)
    {
        $this->params['name'] = (!empty($name)) ? $name : "";
        return $this;
    }

    public function setSku($sku)
    {
        $this->params['sku'] = (!empty($sku)) ? $sku : "";
        return $this;
    }

    public function setUnits($units)
    {
        $this->params['units'] = (!empty($units)) ? $units : "";
        return $this;
    }

    public function setSellingPrice($sellingPrice)
    {
        $this->params['selling_price'] = (!empty($sellingPrice)) ? $sellingPrice : "";
        return $this;
    }

    public function setDiscount($discount)
    {
        $this->params['discount'] = (!empty($discount)) ? $discount : "";
        return $this;
    }

    public function setTax($tax)
    {
        $this->params['tax'] = (!empty($tax)) ? $tax : "";
        return $this;
    }

    public function setHsn($hsn)
    {
        $this->params['hsn'] = (!empty($hsn)) ? $hsn : "";
        return $this;
    }

    public function setPaymentMethod($paymentMethod)
    {
        $this->params['payment_method'] = (!empty($paymentMethod)) ? $paymentMethod : "";
        return $this;
    }

    public function setShippingCharges($shippingCharges)
    {
        $this->params['shipping_charges'] = (!empty($shippingCharges)) ? $shippingCharges : "";
        return $this;
    }

    public function setGiftwrapCharges($giftwrapCharges)
    {
        $this->params['giftwrap_charges'] = (!empty($giftwrapCharges)) ? $giftwrapCharges : "";
        return $this;
    }

    public function setTransactionCharges($transactionCharges)
    {
        $this->params['transaction_charges'] = (!empty($transactionCharges)) ? $transactionCharges : "";
        return $this;
    }

    public function setTotalDiscount($totalDiscount)
    {
        $this->params['total_discount'] = (!empty($totalDiscount)) ? $totalDiscount : "";
        return $this;
    }

    public function setSubTotal($subTotal)
    {
        $this->params['sub_total'] = (!empty($subTotal)) ? $subTotal : "";
        return $this;
    }

    public function setLength($length)
    {
        $this->params['length'] = (!empty($length)) ? $length : "";
        return $this;
    }

    public function setBreadth($breadth)
    {
        $this->params['breadth'] = (!empty($breadth)) ? $breadth : "";
        return $this;
    }

    public function setHeight($height)
    {
        $this->params['height'] = (!empty($height)) ? $height : "";
        return $this;
    }

    public function setWeight($weight)
    {
        $this->params['weight'] = (!empty($weight)) ? $weight : "";
        return $this;
    }

    public function setOrderProductId($orderProductId)
    {
        $this->params['order_product_id'] = (!empty($orderProductId)) ? $orderProductId : "";
        return $this;
    }

    public function setQuantity($quantity)
    {
        $this->params['quantity'] = (!empty($quantity)) ? $quantity : "";
        return $this;
    }

    public function setAction($action)
    {
        $this->params['action'] = (!empty($action)) ? $action : "";
        return $this;
    }

    public function setMasterSku($masterSku)
    {
        $this->params['master_sku'] = (!empty($masterSku)) ? $masterSku : "";
        return $this;
    }

    public function setShipmentId($shipmentId)
    {
        $this->params['shipment_id'] = (!empty($shipmentId)) ? $shipmentId : "";
        return $this;
    }

    public function setCourierId($courierId)
    {
        $this->params['courier_id'] = (!empty($courierId)) ? $courierId : "";
        return $this;
    }

    public function setStatus($status)
    {
        $this->params['status'] = (!empty($status)) ? $status : "";
        return $this;
    }

    public function setPickupPostcode($pickupPostcode)
    {
        $this->params['pickup_postcode'] = (!empty($pickupPostcode)) ? $pickupPostcode : "";
        return $this;
    }

    public function setDeliveryPostcode($deliveryPostcode)
    {
        $this->params['delivery_postcode'] = (!empty($deliveryPostcode)) ? $deliveryPostcode : "";
        return $this;
    }

    public function setCod($cod)
    {
        $this->params['cod'] = (!empty($cod)) ? $cod : "";
        return $this;
    }

    public function setDeclaredValue($declaredValue)
    {
        $this->params['declared_value'] = (!empty($declaredValue)) ? $declaredValue : "";
        return $this;
    }

    public function setMode($mode)
    {
        $this->params['mode'] = (!empty($mode)) ? $mode : "";
        return $this;
    }

    public function setIsReturn($isReturn)
    {
        $this->params['is_return'] = (!empty($isReturn)) ? $isReturn : "";
        return $this;
    }

    public function setDeliveryCountry($deliveryCountry)
    {
        $this->params['delivery_country'] = (!empty($deliveryCountry)) ? $deliveryCountry : "";
        return $this;
    }

    public function setPage($page)
    {
        $this->params['page'] = (!empty($page)) ? $page : "";
        return $this;
    }

    public function setPerPage($perPage)
    {
        $this->params['per_page'] = (!empty($perPage)) ? $perPage : "";
        return $this;
    }

    public function setSort($sort)
    {
        $this->params['sort'] = (!empty($sort)) ? $sort : "";
        return $this;
    }

    public function setSortBy($sortBy)
    {
        $this->params['sort_by'] = (!empty($sortBy)) ? $sortBy : "";
        return $this;
    }

    public function setTo($to)
    {
        $this->params['to'] = (!empty($to)) ? $to : "";
        return $this;
    }

    public function setFrom($from)
    {
        $this->params['from'] = (!empty($from)) ? $from : "";
        return $this;
    }

    public function setFilter($filter)
    {
        $this->params['filter'] = (!empty($filter)) ? $filter : "";
        return $this;
    }

    public function setFilterBy($filterBy)
    {
        $this->params['filter_by'] = (!empty($filterBy)) ? $filterBy : "";
        return $this;
    }

    public function setSearch($search)
    {
        $this->params['search'] = (!empty($search)) ? $search : "";
        return $this;
    }

    public function setPickupCustomerName($pickupCustomerName)
    {
        $this->params['pickup_customer_name'] = (!empty($pickupCustomerName)) ? $pickupCustomerName : "";
        return $this;
    }

    public function setPickupLastName($pickupLastName)
    {
        $this->params['pickup_last_name'] = (!empty($pickupLastName)) ? $pickupLastName : "";
        return $this;
    }

    public function setPickupAddress($pickupAddress)
    {
        $this->params['pickup_address'] = (!empty($pickupAddress)) ? $pickupAddress : "";
        return $this;
    }

    public function setPickupAddress2($pickupAddress2)
    {
        $this->params['pickup_address_2'] = (!empty($pickupAddress2)) ? $pickupAddress2 : "";
        return $this;
    }

    public function setPickupCity($pickupCity)
    {
        $this->params['pickup_city'] = (!empty($pickupCity)) ? $pickupCity : "";
        return $this;
    }

    public function setPickupState($pickupState)
    {
        $this->params['pickup_state'] = (!empty($pickupState)) ? $pickupState : "";
        return $this;
    }

    public function setPickupCountry($pickupCountry)
    {
        $this->params['pickup_country'] = (!empty($pickupCountry)) ? $pickupCountry : "";
        return $this;
    }

    public function setPickupPincode($pickupPincode)
    {
        $this->params['pickup_pincode'] = (!empty($pickupPincode)) ? $pickupPincode : "";
        return $this;
    }

    public function setPickupEmail($pickupEmail)
    {
        $this->params['pickup_email'] = (!empty($pickupEmail)) ? $pickupEmail : "";
        return $this;
    }

    public function setPickupPhone($pickupPhone)
    {
        $this->params['pickup_phone'] = (!empty($pickupPhone)) ? $pickupPhone : "";
        return $this;
    }

    public function setPickupIsdCode($pickupIsdCode)
    {
        $this->params['pickup_isd_code'] = (!empty($pickupIsdCode)) ? $pickupIsdCode : "";
        return $this;
    }

    public function setPickupLocationId($pickupLocationId)
    {
        $this->params['pickup_location_id'] = (!empty($pickupLocationId)) ? $pickupLocationId : "";
        return $this;
    }

    public function setShippingIsdCode($shippingIsdCode)
    {
        $this->params['shipping_isd_code'] = (!empty($shippingIsdCode)) ? $shippingIsdCode : "";
        return $this;
    }

    public function setPostcode($postcode)
    {
        $this->params['postcode'] = (!empty($postcode)) ? $postcode : "";
        return $this;
    }

    public function setProductId($productId)
    {
        $this->params['product_id'] = (!empty($productId)) ? $productId : "";
        return $this;
    }

    public function setListingId($listingId)
    {
        $this->params['listing_id'] = (!empty($listingId)) ? $listingId : "";
        return $this;
    }

    public function setId($id)
    {
        $this->params['id'] = (!empty($id)) ? $id : "";
        return $this;
    }

    public function setBrand($brand)
    {
        $this->params['brand'] = (!empty($brand)) ? $brand : "";
        return $this;
    }

    public function setSize($size)
    {
        $this->params['size'] = (!empty($size)) ? $size : "";
        return $this;
    }

    public function setWidth($width)
    {
        $this->params['width'] = (!empty($width)) ? $width : "";
        return $this;
    }

    public function setEan($ean)
    {
        $this->params['ean'] = (!empty($ean)) ? $ean : "";
        return $this;
    }

    public function setUpc($upc)
    {
        $this->params['upc'] = (!empty($upc)) ? $upc : "";
        return $this;
    }

    public function setIsbn($isbn)
    {
        $this->params['isbn'] = (!empty($isbn)) ? $isbn : "";
        return $this;
    }

    public function setColor($color)
    {
        $this->params['color'] = (!empty($color)) ? $color : "";
        return $this;
    }

    public function setImeiSerialnumber($imeiSerialnumber)
    {
        $this->params['imei_serialnumber'] = (!empty($imeiSerialnumber)) ? $imeiSerialnumber : "";
        return $this;
    }

    public function setCostPrice($costPrice)
    {
        $this->params['cost_price'] = (!empty($costPrice)) ? $costPrice : "";
        return $this;
    }

    public function setMrp($mrp)
    {
        $this->params['mrp'] = (!empty($mrp)) ? $mrp : "";
        return $this;
    }

    public function setImageUrl($imageUrl)
    {
        $this->params['image_url'] = (!empty($imageUrl)) ? $imageUrl : "";
        return $this;
    }

    public function setTaxCode($taxCode)
    {
        $this->params['tax_code'] = (!empty($taxCode)) ? $taxCode : "";
        return $this;
    }

    public function setType($type)
    {
        $this->params['type'] = (!empty($type)) ? $type : "";
        return $this;
    }

    public function setQty($qty)
    {
        $this->params['qty'] = (!empty($qty)) ? $qty : "";
        return $this;
    }

    public function setLowStock($lowStock)
    {
        $this->params['low_stock'] = (!empty($lowStock)) ? $lowStock : "";
        return $this;
    }

    public function setCategoryCode($categoryCode)
    {
        $this->params['category_code'] = (!empty($categoryCode)) ? $categoryCode : "";
        return $this;
    }

    public function setDescription($description)
    {
        $this->params['description'] = (!empty($description)) ? $description : "";
        return $this;
    }

    public function setCountry($country)
    {
        $this->params['country'] = (!empty($country)) ? $country : "";
        return $this;
    }

    public function setPinCode($pinCode)
    {
        $this->params['pin_code'] = (!empty($pinCode)) ? $pinCode : "";
        return $this;
    }

    public function setPhone($phone)
    {
        $this->params['phone'] = (!empty($phone)) ? $phone : "";
        return $this;
    }

    public function setAddress($address)
    {
        $this->params['address'] = (!empty($address)) ? $address : "";
        return $this;
    }

    public function setAddress2($address2)
    {
        $this->params['address_2'] = (!empty($address2)) ? $address2 : "";
        return $this;
    }

    public function setCity($city)
    {
        $this->params['city'] = (!empty($city)) ? $city : "";
        return $this;
    }

    public function setState($state)
    {
        $this->params['state'] = (!empty($state)) ? $state : "";
        return $this;
    }

    public function setAwbs($awbs)
    {
        $this->params['awbs'] = (!empty($awbs)) ? $awbs : "";
        return $this;
    }

    public function setVendorDetails($vendorDetails)
    {
        $this->params['vendor_details'] = (!empty($vendorDetails)) ? $vendorDetails : "";
        return $this;
    }

    public function setPrintLabel($printLabel)
    {
        $this->params['print_label'] = (!empty($printLabel)) ? $printLabel : "";
        return $this;
    }

    public function setGenerateManifest($generateManifest)
    {
        $this->params['generate_manifest'] = (!empty($generateManifest)) ? $generateManifest : "";
        return $this;
    }

    public function setResellerName($resellerName)
    {
        $this->params['reseller_name'] = (!empty($resellerName)) ? $resellerName : "";
        return $this;
    }

    public function setIsdCode($isdCode)
    {
        $this->params['isd_code'] = (!empty($isdCode)) ? $isdCode : "";
        return $this;
    }

    public function setBillingIsdCode($billingIsdCode)
    {
        $this->params['billing_isd_code'] = (!empty($billingIsdCode)) ? $billingIsdCode : "";
        return $this;
    }

    public function setRequestPickup($requestPickup)
    {
        $this->params['request_pickup'] = (!empty($requestPickup)) ? $requestPickup : "";
        return $this;
    }

    public function setIds($ids)
    {
        $this->params['ids'] = (!empty($ids)) ? $ids : "";
        return $this;
    }

    public function setOrderIds($orderIds)
    {
        $this->params['order_ids'] = (!empty($orderIds)) ? $orderIds : "";
        return $this;
    }

}