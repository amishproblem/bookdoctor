<?php

/**
 * Class ShipRocketMethodManager
 * Документация: https://apidocs.shiprocket.in/?version=latest
 */
class ShipRocketMethodManager extends MethodManager
{
    const AUTH_LOGIN = "auth/login";
    const ORDERS_CREATE_ADHOC = "orders/create/adhoc";
    const ORDERS_CREATE = "orders/create";
    const ORDERS_ADDRESS_PICKUP = "orders/address/pickup";
    const ORDERS_ADDRESS_UPDATE = "orders/address/update";
    const ORDERS_CANCEL = "orders/cancel";
    const ORDERS_FULFILL = "orders/fulfill";
    const ORDERS_MAPPING = "orders/mapping";
    const ORDERS_IMPORT = "orders/import";
    const COURIER_ASSIGN_AWB = "courier/assign/awb";
    const COURIER_INTERNATIONAL_SERVICEABILITY = "courier/international/serviceability";
    const COURIER_GENERATE_PICKUP = "courier/generate/pickup";
    const ORDERS = "orders";
    const ORDERS_SHOW_ORDER_ID = "orders/show/order_id";
    const ORDERS_EXPORT = "orders/export";
    const ORDERS_CREATE_RETURN = "orders/create/return";
    const ORDERS_PROCESSING_RETURN = "orders/processing/return";
    const COURIER_SERVICEABILITY = "courier/serviceability";
    const SHIPMENTS = "shipments";
    const SHIPMENTS_SHIPMENT_ID = "shipments/shipment_id";
    const MANIFESTS_GENERATE = "manifests/generate";
    const MANIFESTS_PRINT = "manifests/print";
    const COURIER_GENERATE_LABEL = "courier/generate/label";
    const ORDERS_PRINT_INVOICE = "orders/print/invoice";
    const SHIPMENTS_CREATE_FORWARD_SHIPMENT = "shipments/create/forward-shipment";
    const COURIER_TRACK_AWB_AWB_CODE = "courier/track/awb/";
    const COURIER_TRACK_AWBS = "courier/track/awbs";
    const COURIER_TRACK = "courier/track";
    const COURIER_TRACK_SHIPMENT_SHIPMENT_ID = "courier/track/shipment/";
    const SETTINGS_COMPANY_PICKUP = "settings/company/pickup";
    const SETTINGS_COMPANY_ADDPICKUP = "settings/company/addpickup";
    const PRODUCTS = "products";
    const PRODUCTS_SHOW_PRODUCT_ID = "products/show/product_id";
    const PRODUCTS_IMPORT = "products/import";
    const PRODUCTS_SAMPLE = "products/sample";
    const LISTINGS = "listings";
    const LISTINGS_LINK = "listings/link";
    const LISTINGS_IMPORT = "listings/import";
    const LISTINGS_EXPORT_MAPPED = "listings/export/mapped";
    const LISTINGS_EXPORT_UNMAPPED = "listings/export/unmapped";
    const LISTINGS_SAMPLE = "listings/sample";
    const CHANNELS = "channels";
    const INVENTORY = "inventory";
    const INVENTORY_PRODUCT_ID_UPDATE = "inventory/product_id/update";
    const COUNTRIES = "countries";
    const COUNTRIES_SHOW_COUNTRY_ID = "countries/show/country_id";
    const OPEN_POSTCODE_DETAILS = "open/postcode/details";
    const ACCOUNT_DETAILS_STATEMENT = "account/details/statement";
    const BILLING_DISCREPANCY = "billing/discrepancy";
    const ERRORS_IMPORT_ID_CHECK = "errors/import_id/check";

    /**
     * Уникальный URN метода: auth/login
     *
     */
    public function authLogin()
    {
        return $this->handle(self::AUTH_LOGIN);
    }

    /**
     * Уникальный URN метода: orders/create/adhoc
     */
    public function ordersCreateAdhoc()
    {
        return $this->handle(self::ORDERS_CREATE_ADHOC);
    }

    /**
     * Уникальный URN метода: orders/create
     */
    public function ordersCreate()
    {
        return $this->handle(self::ORDERS_CREATE);
    }

    /**
     * Уникальный URN метода: orders/address/pickup
     */
    public function ordersAddressPickup()
    {
        return $this->handle(self::ORDERS_ADDRESS_PICKUP);
    }

    /**
     * Уникальный URN метода: orders/address/update
     */
    public function ordersAddressUpdate()
    {
        return $this->handle(self::ORDERS_ADDRESS_UPDATE);
    }

    /**
     * Уникальный URN метода: orders/cancel
     */
    public function ordersCancel()
    {
        return $this->handle(self::ORDERS_CANCEL);
    }

    /**
     * Уникальный URN метода: orders/fulfill
     */
    public function ordersFulfill()
    {
        return $this->handle(self::ORDERS_FULFILL);
    }

    /**
     * Уникальный URN метода: orders/mapping
     */
    public function ordersMapping()
    {
        return $this->handle(self::ORDERS_MAPPING);
    }

    /**
     * Уникальный URN метода: orders/import
     */
    public function ordersImport()
    {
        return $this->handle(self::ORDERS_IMPORT);
    }

    /**
     * Уникальный URN метода: courier/assign/awb
     */
    public function courierAssignAwb()
    {
        return $this->handle(self::COURIER_ASSIGN_AWB);
    }

    /**
     * Уникальный URN метода: courier/international/serviceability
     */
    public function courierInternationalServiceability()
    {
        return $this->handle(self::COURIER_INTERNATIONAL_SERVICEABILITY);
    }

    /**
     * Уникальный URN метода: courier/generate/pickup
     */
    public function courierGeneratePickup()
    {
        return $this->handle(self::COURIER_GENERATE_PICKUP);
    }

    /**
     * Уникальный URN метода: orders
     */
    public function orders()
    {
        return $this->handle(self::ORDERS);
    }

    /**
     * Уникальный URN метода: orders/show/order_id
     */
    public function ordersShowOrderId()
    {
        return $this->handle(self::ORDERS_SHOW_ORDER_ID);
    }

    /**
     * Уникальный URN метода: orders/export
     */
    public function ordersExport()
    {
        return $this->handle(self::ORDERS_EXPORT);
    }

    /**
     * Уникальный URN метода: orders/create/return
     */
    public function ordersCreateReturn()
    {
        return $this->handle(self::ORDERS_CREATE_RETURN);
    }

    /**
     * Уникальный URN метода: orders/processing/return
     */
    public function ordersProcessingReturn()
    {
        return $this->handle(self::ORDERS_PROCESSING_RETURN);
    }

    /**
     * Уникальный URN метода: courier/serviceability
     */
    public function courierServiceability()
    {
        return $this->handle(self::COURIER_SERVICEABILITY);
    }

    /**
     * Уникальный URN метода: shipments
     */
    public function shipments()
    {
        return $this->handle(self::SHIPMENTS);
    }

    /**
     * Уникальный URN метода: shipments/shipment_id
     */
    public function shipmentsShipmentId()
    {
        return $this->handle(self::SHIPMENTS_SHIPMENT_ID);
    }

    /**
     * Уникальный URN метода: manifests/generate
     */
    public function manifestsGenerate()
    {
        return $this->handle(self::MANIFESTS_GENERATE);
    }

    /**
     * Уникальный URN метода: manifests/print
     */
    public function manifestsPrint()
    {
        return $this->handle(self::MANIFESTS_PRINT);
    }

    /**
     * Уникальный URN метода: courier/generate/label
     */
    public function courierGenerateLabel()
    {
        return $this->handle(self::COURIER_GENERATE_LABEL);
    }

    /**
     * Уникальный URN метода: orders/print/invoice
     */
    public function ordersPrintInvoice()
    {
        return $this->handle(self::ORDERS_PRINT_INVOICE);
    }

    /**
     * Уникальный URN метода: shipments/create/forward-shipment
     */
    public function shipmentsCreateForwardShipment()
    {
        return $this->handle(self::SHIPMENTS_CREATE_FORWARD_SHIPMENT);
    }

    /**
     * Уникальный URN метода: courier/track/awb/awb_code
     */
    public function courierTrackAwbAwbCode()
    {
        return $this->handle(self::COURIER_TRACK_AWB_AWB_CODE);
    }

    /**
     * Уникальный URN метода: courier/track/awbs
     */
    public function courierTrackAwbs()
    {
        return $this->handle(self::COURIER_TRACK_AWBS);
    }

    /**
     * Уникальный URN метода: courier/track?order_id=123&channel_id=12345
     */
    public function courierTrack()
    {
        return $this->handle(self::COURIER_TRACK);
    }

    /**
     * Уникальный URN метода: courier/track/shipment/shipment_id
     */
    public function courierTrackShipmentShipmentId()
    {
        return $this->handle(self::COURIER_TRACK_SHIPMENT_SHIPMENT_ID);
    }

    /**
     * Уникальный URN метода: settings/company/pickup
     */
    public function settingsCompanyPickup()
    {
        return $this->handle(self::SETTINGS_COMPANY_PICKUP);
    }

    /**
     * Уникальный URN метода: settings/company/addpickup
     */
    public function settingsCompanyAddpickup()
    {
        return $this->handle(self::SETTINGS_COMPANY_ADDPICKUP);
    }

    /**
     * Уникальный URN метода: products
     */
    public function products()
    {
        return $this->handle(self::PRODUCTS);
    }

    /**
     * Уникальный URN метода: products/show/product_id
     */
    public function productsShowProductId()
    {
        return $this->handle(self::PRODUCTS_SHOW_PRODUCT_ID);
    }

    /**
     * Уникальный URN метода: products/import
     */
    public function productsImport()
    {
        return $this->handle(self::PRODUCTS_IMPORT);
    }

    /**
     * Уникальный URN метода: products/sample
     */
    public function productsSample()
    {
        return $this->handle(self::PRODUCTS_SAMPLE);
    }

    /**
     * Уникальный URN метода: listings
     */
    public function listings()
    {
        return $this->handle(self::LISTINGS);
    }

    /**
     * Уникальный URN метода: listings/link
     */
    public function listingsLink()
    {
        return $this->handle(self::LISTINGS_LINK);
    }

    /**
     * Уникальный URN метода: listings/import
     */
    public function listingsImport()
    {
        return $this->handle(self::LISTINGS_IMPORT);
    }

    /**
     * Уникальный URN метода: listings/export/mapped
     */
    public function listingsExportMapped()
    {
        return $this->handle(self::LISTINGS_EXPORT_MAPPED);
    }

    /**
     * Уникальный URN метода: listings/export/unmapped
     */
    public function listingsExportUnmapped()
    {
        return $this->handle(self::LISTINGS_EXPORT_UNMAPPED);
    }

    /**
     * Уникальный URN метода: listings/sample
     */
    public function listingsSample()
    {
        return $this->handle(self::LISTINGS_SAMPLE);
    }

    /**
     * Уникальный URN метода: channels
     */
    public function channels()
    {
        return $this->handle(self::CHANNELS);
    }

    /**
     * Уникальный URN метода: inventory
     */
    public function inventory()
    {
        return $this->handle(self::INVENTORY);
    }

    /**
     * Уникальный URN метода: inventory/product_id/update
     */
    public function inventoryProductIdUpdate()
    {
        return $this->handle(self::INVENTORY_PRODUCT_ID_UPDATE);
    }

    /**
     * Уникальный URN метода: countries
     */
    public function countries()
    {
        return $this->handle(self::COUNTRIES);
    }

    /**
     * Уникальный URN метода: countries/show/country_id
     */
    public function countriesShowCountryId()
    {
        return $this->handle(self::COUNTRIES_SHOW_COUNTRY_ID);
    }

    /**
     * Уникальный URN метода: open/postcode/details
     */
    public function openPostcodeDetails()
    {
        return $this->handle(self::OPEN_POSTCODE_DETAILS);
    }

    /**
     * Уникальный URN метода: account/details/statement
     */
    public function accountDetailsStatement()
    {
        return $this->handle(self::ACCOUNT_DETAILS_STATEMENT);
    }

    /**
     * Уникальный URN метода: billing/discrepancy
     */
    public function billingDiscrepancy()
    {
        return $this->handle(self::BILLING_DISCREPANCY);
    }

    /**
     * Уникальный URN метода: errors/import_id/check
     */
    public function errorsImportIdCheck()
    {
        return $this->handle(self::ERRORS_IMPORT_ID_CHECK);
    }
}