<?php


class ShipRocketUrl extends Url
{
    /** @var string */
    private $method;

    public function __construct($method = null)
    {
        $this->method = $method;
    }

    /**
     * @param string $method
     * @return ShipRocketUrl
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @param string $url
     * @return ShipRocketUrl
     */
    public function setUrl($url)
    {
        $this->prepareUrl = $url;
        return $this;
    }

    protected function prepareUrl()
    {
        if (empty($this->method)) {
            return null;
        }

        $this->prepareUrl = "https://apiv2.shiprocket.in/v1/external/{$this->method}";

        return $this;
    }
}