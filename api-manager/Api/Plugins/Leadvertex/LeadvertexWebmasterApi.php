<?php


class LeadvertexWebmasterApi extends LeadvertexApi
{

    /** @var LeadvertexWebmasterMethodManager */
    public $methodManager;

    protected function setMethodManager()
    {
        $this->methodManager = new LeadvertexWebmasterMethodManager($this);
    }

    public function setApiMode()
    {
        $this->url->setApiMode('webmaster/v2');
    }
}