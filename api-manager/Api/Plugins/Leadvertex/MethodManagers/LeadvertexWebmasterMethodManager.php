<?php


class LeadvertexWebmasterMethodManager extends MethodManager
{

    const ADD_ORDER = "addOrder";
    const GET_ORDERS_BY_IDS = "getOrdersByIds";


    public function addOrder()
    {
        return $this->handle(self::ADD_ORDER);
    }

    public function getOrdersByIds()
    {
        return $this->handle(self::GET_ORDERS_BY_IDS);
    }

}

