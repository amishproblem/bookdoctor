<?php


class LeadvertexAdminMethodManager extends MethodManager
{
    const ADD_ORDER = 'addOrder';
    const UPDATE_ORDER = 'updateOrder';
    const ADD_GOOD = "addGood";
    const ACTIVATE_GOOD_IN_OFFER = "activateGoodInOffer";
    const UPDATE_GOOD = "updateGood";
    const INCREASE_GOOD_RESERVE = "increaseGoodReserve";
    const REDUCE_GOOD_RESERVE = "reduceGoodReserve";

    const GET_STATUS_LIST = "getStatusList";
    const GET_ORDERS_ID_IN_STATUS = "getOrdersIdsInStatus";
    const GET_ORDERS_BY_CONDITION ="getOrdersIdsByCondition";
    const GET_OFFER_GOODS = "getOfferGoods";
    const GET_GOOD_CATEGORIES = "getGoodCategories";
    const GET_ORDERS_BY_IDS = "getOrdersByIds";
    const GET_ORDERS_BY_RUSSIAN_POST_TRACKS = "getOrdersByRussianPostTracks";
    const CHECK_USED_BY_ORDER = "checkUsedByOrder"; //Проверка редактируется ли заказ
    const GET_ORDER_HISTORY = "getOrderHistory";
    const GET_ORDER_HISTORY_BY_TIME_SAVE = "getOrderHistoryByTimeSave"; //Получение истории заказа за определенный период времени
    const GET_OPERATOR_ACTIONS = "getOperatorActions";
    const GET_OPERATORS = "getOperators";
    const GET_ACTIVE_ACTIONS = "getActiveOperators";
    const GET_ONLINE_OPERATORS = "getOnlineOperators";
    const GET_WEBMASTERS = "getWebmasters";
    const GET_WEBMASTER_PAYMENTS = "getWebmasterPayments";
    const GET_WEBMASTER_ORDERED_PAYMENTS ="getWebmasterOrderedPayments"; //Получение данных по заказанным выплатам веб-мастера

    public function getStatusList()
    {
        return $this->handle(self::GET_STATUS_LIST);
    }

    public function getOrdersIdsInStatus()
    {
        return $this->handle(self::GET_ORDERS_ID_IN_STATUS);
    }

    public function getOrdersIdsByCondition()
    {
        return $this->handle(self::GET_ORDERS_BY_CONDITION);
    }

    public function getOfferGoods()
    {
        return $this->handle(self::GET_OFFER_GOODS);
    }

    public function getGoodCategories()
    {
        return $this->handle(self::GET_GOOD_CATEGORIES);
    }

    public function getOrdersByIds()
    {
        return $this->handle(self::GET_ORDERS_BY_IDS);
    }

    public function getOrdersByRussianPostTracks()
    {
        return $this->handle(self::GET_ORDERS_BY_RUSSIAN_POST_TRACKS);
    }

    public function checkUsedByOrder()
    {
        return $this->handle(self::CHECK_USED_BY_ORDER);
    }

    public function getOrderHistory()
    {
        return $this->handle(self::GET_ORDER_HISTORY);
    }

    public function getOrderHistoryByTimeSave()
    {
        return $this->handle(self::GET_ORDER_HISTORY_BY_TIME_SAVE);
    }

    public function getOperatorActions()
    {
        return $this->handle(self::GET_OPERATOR_ACTIONS);
    }

    public function getOperators()
    {
        return $this->handle(self::GET_OPERATORS);
    }

    public function getActiveOperators()
    {
        return $this->handle(self::GET_ACTIVE_ACTIONS);
    }

    public function getOnlineOperators()
    {
        return $this->handle(self::GET_ONLINE_OPERATORS);
    }

    public function getWebmasters()
    {
        return $this->handle(self::GET_WEBMASTERS);
    }

    public function getWebmasterPayments()
    {
        return $this->handle(self::GET_WEBMASTER_PAYMENTS);
    }

    public function getWebmasterOrderedPayments()
    {
        return $this->handle(self::GET_WEBMASTER_ORDERED_PAYMENTS);
    }

    public function addOrder()
    {
        return $this->handle(self::ADD_ORDER);
    }

    public function updateOrder()
    {
        return $this->handle(self::UPDATE_ORDER);
    }

    public function addGood()
    {
        return $this->handle(self::ADD_GOOD);
    }

    public function activateGoodInOffer()
    {
        return $this->handle(self::ACTIVATE_GOOD_IN_OFFER);
    }

    public function updateGood()
    {
        return $this->handle(self::UPDATE_GOOD);
    }

    public function increaseGoodReserve()
    {
        return $this->handle(self::INCREASE_GOOD_RESERVE);
    }

    public function reduceGoodReserve()
    {
        return $this->handle(self::REDUCE_GOOD_RESERVE);
    }

}