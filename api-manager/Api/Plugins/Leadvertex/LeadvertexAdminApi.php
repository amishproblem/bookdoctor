<?php


class LeadvertexAdminApi extends LeadvertexApi
{

    /** @var LeadvertexAdminMethodManager */
    public $methodManager;

    protected function setMethodManager()
    {
        $this->methodManager = new LeadvertexAdminMethodManager($this);
    }

    public function setApiMode()
    {
        $this->url->setApiMode('admin');
    }
}