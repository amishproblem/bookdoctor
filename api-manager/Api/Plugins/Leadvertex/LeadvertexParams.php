<?php


class LeadvertexParams extends Params
{

    /**
     * @param $fio
     * @return $this
     * Расшифровка: Ф.И.О.
     */
    public function setFio($fio)
    {
        $this->params['fio'] = $fio;
        return $this;
    }

    /**
     * @param $phone
     * @return $this
     * Расшифровка: Номер телефона
     */
    public function setPhone($phone)
    {
        $this->params['phone'] = $phone;
        return $this;
    }

    /**
     * @param $utmSource
     * @return $this
     * Расшифровка: Источник компании
     */
    public function setUtmSource($utmSource)
    {
        $this->params['utm_source'] = $utmSource;
        return $this;
    }

    /**
     * @param $utmMedium
     * @return $this
     * Расшифрока: Тип трафика
     */
    public function setUtmMedium($utmMedium)
    {
        $this->params['utm_medium'] = $utmMedium;
        return $this;
    }

    /**
     * @param $utmCampaign
     * @return $this
     * Расшифрока: Название компании
     */
    public function setUtmCampaign($utmCampaign)
    {
        $this->params['utm_campaign'] = $utmCampaign;
        return $this;
    }

    /**
     * @param $utmTerm
     * @return $this
     * Расшифрока: Идентификатор объявления
     */
    public function setUtmTerm($utmTerm)
    {
        $this->params['utm_term'] = $utmTerm;
        return $this;
    }

    /**
     * @param $utmContent
     * @return $this
     * Расшифрока: Ключевое слово
     */
    public function setUtmContent($utmContent)
    {
        $this->params['utm_content'] = $utmContent;
        return $this;
    }

    /**
     * @param Goods $goods
     * @return Params
     * Расшифрока: Содержит информацию о товарах в заказе
     */
    public function setGoods(Goods $goods)
    {
        $this->params['goods'] = $goods->getGoods();
        return $this;
    }

    /**
     * @param $domain
     * @return $this
     * Расшифрока: Домен, на котором был совершен заказ
     */
    public function setDomain($domain)
    {
        $this->params['domain'] = $domain;
        return $this;
    }

    /**
     * @param $referer
     * @return $this
     * Расшифрока: referer клиента, сделавшего заказ
     */
    public function setReferer($referer)
    {
        $this->params['referer'] = $referer;
        return $this;
    }

    /**
     * @param $ip
     * @return $this
     * Расшифрока: ip клиента, сделавшего заказ
     */
    public function setIp($ip)
    {
        $this->params['ip'] = $ip;
        return $this;
    }

    /**
     * @param $country
     * @return $this
     * Расшифрока: Страна
     */
    public function setCountry($country)
    {
        $this->params['country'] = $country;
        return $this;
    }

    /**
     * @param $postIndex
     * @return $this
     * Расшифрока: Почтовый индекс
     */
    public function setPostIndex($postIndex)
    {
        $this->params['postIndex'] = $postIndex;
        return $this;
    }

    /**
     * @param $region
     * @return $this
     * Расшифрока: Регион
     */
    public function setRegion($region)
    {
        $this->params['region'] = $region;
        return $this;
    }

    /**
     * @param $city
     * @return $this
     * Расшифрока: Город
     */
    public function setCity($city)
    {
        $this->params['city'] = $city;
        return $this;
    }

    /**
     * @param $address
     * @return $this
     * Расшифрока: Адрес
     */
    public function setAddress($address)
    {
        $this->params['address'] = $address;
        return $this;
    }

    /**
     * @param $house
     * @return $this
     * Расшифрока: Дом/строение
     */
    public function setHouse($house)
    {
        $this->params['house'] = $house;
        return $this;
    }

    /**
     * @param $flat
     * @return $this
     * Расшифрока: Квартира/офис
     */
    public function setFlat($flat)
    {
        $this->params['flat'] = $flat;
        return $this;
    }

    /**
     * @param $email
     * @return $this
     * Расшифрока: Email
     */
    public function setEmail($email)
    {
        $this->params['email'] = $email;
        return $this;
    }

    /**
     * @param $quantity
     * @return $this
     * Расшифрока: Количество единиц товара
     */
    public function setQuantity($quantity)
    {
        $this->params['quantity'] = $quantity;
        return $this;
    }

    /**
     * @param $number
     * @param $additionalData
     * @return $this
     * Расшифрока: Дополнительное поле с номером (1-25)
     */
    public function setAdditional($number, $additionalData)
    {
        if ($number > 25 or $number < 1) {
            echo "Поля с номером {$number} не существует" . PHP_EOL;
            return $this;
        }

        $this->params['additional' . $number] = $additionalData;
        return $this;
    }

    /**
     * @param $datetime
     * @return $this
     * Расшифрока: Дата и время совершения заказа
     */
    public function setDatetime($datetime)
    {
        $this->params['datetime'] = $datetime;
        return $this;
    }

    /**
     * @param $timezone
     * @return $this
     * Расшифрока: Смещение времени клиента в минутах относительно UTC (чтобы не звонить ему ночью).
     */
    public function setTimezone($timezone)
    {
        $this->params['timezone'] = $timezone;
        return $this;
    }

    /**
     * @param $status
     * @return $this
     * Расшифрока: Статус заказа
     */
    public function setStatus($status)
    {
        $this->params['status'] = $status;
        return $this;
    }

    /**
     * @param $statusGroup
     * @return $this
     * Расшифрока: Группа статуса
     */
    public function setStatusGroup($statusGroup)
    {
        $this->params['statusGroup'] = $statusGroup;
        return $this;
    }

    /**
     * @param $price
     * @return $this
     * Расшифрока: Цена за единицу товара, по которой он был продан
     */
    public function setPrice($price)
    {
        $this->params['price'] = $price;
        return $this;
    }

    /**
     * @param $total
     * @return $this
     * Расшифрока: Цена итого
     */
    public function setTotal($total)
    {
        $this->params['total'] = $total;
        return $this;
    }

    /**
     * @param $operatorID
     * @return $this
     * Расшифрока: ID оператора, NULL - если заказ не закреплен за оператором
     */
    public function setOperatorID($operatorID = null)
    {
        $this->params['operatorID'] = $operatorID;
        return $this;
    }


    /**
     * @param $id
     * @param null $login
     * @param null $sum
     * @param null $paid
     * @return $this
     * Расшифровка: webmaster - содержит информацию о вознаграждении веб-мастера
     *                  - id - id веб-мастера
     *                  - login - login веб-мастера
     *                  - sum - сумма вознаграждения за заказ
     *                  - paid - статус вознаграждения. -1 - отказано, 0 - в обработке, 1 - выплачено
     */
    public function setWebmaster($id, $login = null, $sum = null, $paid = null)
    {
        $this->params['webmaster']['id'] = $id;
        $this->params['webmaster']['login'] = $login;
        $this->params['webmaster']['sum'] = $sum;
        $this->params['webmaster']['paid'] = $paid;
        return $this;
    }

    /**
     * @param $timeSpent
     * @return $this
     * Расшифровка: Затрачено времени на заказ в секундах
     */
    public function setTimeSpent($timeSpent)
    {
        $this->params['timeSpent'] = $timeSpent;
        return $this;
    }

    /**
     * @param $lastUpdate
     * @return $this
     * Расшифровка: Статус изменен
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->params['lastUpdate'] = $lastUpdate;
        return $this;
    }

    /**
     * @param $lastModify
     * @return $this
     * Расшифровка:
     */
    public function setLastModify($lastModify)
    {
        $this->params['lastModify'] = $lastModify;
        return $this;
    }

    /**
     * @param $russianpostTrack
     * @return $this
     * Расшифровка:
     */
    public function setRussianpostTrack($russianpostTrack)
    {
        $this->params['russianpostTrack'] = $russianpostTrack;
        return $this;
    }

    /**
     * @param $russianpostStatus
     * @return $this
     * Расшифровка:
     */
    public function setRussianpostStatus($russianpostStatus)
    {
        $this->params['russianpostStatus'] = $russianpostStatus;
        return $this;
    }

    /**
     * @param $novaposhtaTrack
     * @return $this
     * Расшифровка:
     */
    public function setNovaposhtaTrack($novaposhtaTrack)
    {
        $this->params['novaposhtaTrack'] = $novaposhtaTrack;
        return $this;
    }

    /**
     * @param $kazpostTrack
     * @return $this
     * Расшифровка:
     */
    public function setKazpostTrack($kazpostTrack)
    {
        $this->params['kazpostTrack'] = $kazpostTrack;
        return $this;
    }

    /**
     * @param $kazpostStatus
     * @return $this
     * Расшифровка:
     */
    public function setkazpostStatus($kazpostStatus)
    {
        $this->params['kazpostStatus'] = $kazpostStatus;
        return $this;
    }

    /**
     * @param $belpostTrack
     * @return $this
     * Расшифровка:
     */
    public function setBelpostTrack($belpostTrack)
    {
        $this->params['belpostTrack'] = $belpostTrack;
        return $this;
    }


    /**
     * @param $lastTextSms
     * @return $this
     * Расшифровка: Текст текстового SMS
     */
    public function setLastTextSms($lastTextSms)
    {
        $this->params['lastTextSms'] = $lastTextSms;
        return $this;
    }

    /**
     * @param $lastVoiceSms
     * @return $this
     * Расшифровка: Текст голосового SMS
     */
    public function setLastVoiceSms($lastVoiceSms)
    {
        $this->params['lastVoiceSms'] = $lastVoiceSms;
        return $this;
    }

    /**
     * @param $lastVoiceSmsStatus
     * @return $this
     * Расшифровка: Статус голосового SMS
     */
    public function setLastVoiceSmsStatus($lastVoiceSmsStatus)
    {
        $this->params['lastVoiceSmsStatus'] = $lastVoiceSmsStatus;
        return $this;
    }

    /**
     * @param $callmodeRecallAt
     * @return $this
     * Расшифровка: Время перезвона в формате ГГГГ-ММ-ДД ЧЧ:ММ:СС
     */
    public function setCallmodeRecallAt($callmodeRecallAt)
    {
        $this->params['callmodeRecallAt'] = $callmodeRecallAt;
        return $this;
    }

    /**
     * @param $roboLog
     * @return $this
     * Расшифровка: Лог перезвона робота
     */
    public function setRoboLog($roboLog)
    {
        $this->params['roboLog'] = $roboLog;
        return $this;
    }

    /**
     * @param $recallLog
     * @return $this
     * Расшифровка: Лог недозвонов
     */
    public function setRecallLog($recallLog)
    {
        $this->params['recallLog'] = $recallLog;
        return $this;
    }

    /**
     * @param $phone
     * @param $ip
     * @return $this
     * Расшифровка: similar - содержит информацию о дублях в заказе
     *                  - phone - количество дублей по номеру телефона
     *                  - ip - количество дублей по IP-адресу
     */
    public function setSimilar($phone, $ip)
    {
        $this->params['similar']['phone'] = $phone;
        $this->params['similar']['ip'] = $ip;
        return $this;
    }

    /**
     * @param $cdekTrack
     * @return $this
     * Расшифровка: Номер заказа в Cdek
     */
    public function setCdekTrack($cdekTrack)
    {
        $this->params['cdekTrack'] = $cdekTrack;
        return $this;
    }

    /**
     * @param $cdekStatus
     * @return $this
     * Расшифровка: Статус Cdek
     */
    public function setCdekStatus($cdekStatus)
    {
        $this->params['cdekStatus'] = $cdekStatus;
        return $this;
    }

    /**
     * @param $approvedAt
     * @return $this
     * Расшифровка: Дата апрува
     */
    public function setApprovedAt($approvedAt)
    {
        $this->params['approvedAt'] = $approvedAt;
        return $this;
    }

    /**
     * @param $canceledAt
     * @return $this
     * Расшифровка: Дата отмены
     */
    public function setCanceledAt($canceledAt)
    {
        $this->params['canceledAt'] = $canceledAt;
        return $this;
    }

    /**
     * @param $shippedAt
     * @return $this
     * Расшифровка: Дата отправки
     */
    public function setShippedAt($shippedAt)
    {
        $this->params['shippedAt'] = $shippedAt;
        return $this;
    }

    /**
     * @param $buyoutAt
     * @return $this
     * Расшифровка: Дата выкупа
     */
    public function setBuyoutAt($buyoutAt)
    {
        $this->params['buyoutAt'] = $buyoutAt;
        return $this;
    }

    /**
     * @param $refundedAt
     * @return $this
     * Расшифровка: Дата возврата
     */
    public function setRefundedAt($refundedAt)
    {
        $this->params['refundedAt'] = $refundedAt;
        return $this;
    }

    /**
     * @param $comment
     * @return $this
     * Расшифровка: Комментарий к заказу
     */
    public function setComment($comment)
    {
        $this->params['comment'] = $comment;
        return $this;
    }

    /**
     * @param $externalWebmaster
     * @return $this
     * Расшифровка: External webmaster ID
     */
    public function setExternalWebmaster($externalWebmaster)
    {
        $this->params['externalWebmaster'] = $externalWebmaster;
        return $this;
    }

    /**
     * @param $externalID
     * @return $this
     * Расшифровка: External ID
     */
    public function setExternalID($externalID)
    {
        $this->params['externalID'] = $externalID;
        return $this;
    }
}