<?php


abstract class LeadvertexApi extends Api
{

    /** @var LeadvertexUrl */
    public $url;

    /** @var LeadvertexParams */
    public $params;

    /** @var Goods */
    public $goods;

    /** @var Good */
    public $goodTemplate;

    public function __construct($debug = false)
    {
        parent::__construct($debug);
        $this->setUrl();
        $this->goodTemplate = new Good();
    }

    public function setMethod($method)
    {
        $this->goods = new Goods($method);
        $this->setApiMode();
        $this->url->setMethod($method);
    }

    protected function setUrl()
    {
        $this->url = new LeadvertexUrl();
    }

    protected function setParams()
    {
        $this->params = new LeadvertexParams();
    }

    abstract public function setApiMode();
}