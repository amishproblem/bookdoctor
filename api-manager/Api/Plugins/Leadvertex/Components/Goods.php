<?php

/**
 * Class Goods
 * Содержит информацию о товарах в заказе.
 */
class Goods
{
    const IMPORT = 'import';
    const ADD = 'add';
    const UPDATE = 'update';
    const DELETE = 'delete';

    protected $import = [];
    protected $add = [];
    protected $update = [];
    protected $delete = [];
    private $method;

    public function __construct($method)
    {
        $this->method = $method;
    }

    public function registerGood(Good $good, $mode = null)
    {
        switch ($mode) {
            case self::ADD:
                $this->add[] = $good->getGood();
                break;
            case self::UPDATE:
                $this->update[] = $good->getGood();
                break;
            case self::DELETE:
                $this->delete[] = $good->getGood();
                break;
            case self::IMPORT:
            default:
                $this->import[] = $good->getGood();
                break;
        }
    }

    public function getGoods()
    {
        switch ($this->method) {
            case 'addOrder':
                return $this->import;
                break;
            case 'updateOrder':
                return [
                    self::ADD => $this->add,
                    self::UPDATE => $this->update,
                    self::DELETE => $this->delete,
                ];
                break;
        }
    }
}