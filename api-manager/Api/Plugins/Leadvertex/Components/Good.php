<?php

/**
 * Class Good
 * Сущность товара
 */
class Good
{
    protected $good = [];

    /**
     * @param $goodId
     * @return $this
     * Расшифрока: Уникальный ID товара
     */
    public function setGoodID($goodId)
    {
        $clone = clone $this;
        $clone->good['goodID'] = $goodId;

        return $clone;
    }

    public function getGood()
    {
        return $this->good;
    }

    /**
     * @param $name
     * @return $this
     * Расшифрока: Наименование товара
     */
    public function setName($name)
    {
        $clone = clone $this;
        $clone->good['name'] = $name;

        return $clone;
    }

    /**
     * @param $quantity
     * @return $this
     * Расшифрока: Количество
     */
    public function setQuantity($quantity)
    {
        $clone = clone $this;
        $clone->good['quantity'] = $quantity;

        return $clone;
    }

    /**
     * @param $price
     * @return $this
     * Расшифрока: Цена за указанное количество
     */
    public function setPrice($price)
    {
        $clone = clone $this;
        $clone->good['price'] = $price;

        return $clone;
    }

    /**
     * @param $categoryID
     * @return $this
     * Расшифрока: Id категории товара
     */
    public function setCategoryID($categoryID)
    {
        $clone = clone $this;
        $clone->good['categoryID'] = $categoryID;

        return $clone;
    }

    /**
     * @param $categoryName
     * @return $this
     * Расшифрока: Наименование категории товара
     */
    public function setCategoryName($categoryName)
    {
        $clone = clone $this;
        $clone->good['categoryName'] = $categoryName;

        return $clone;
    }

    /**
     * @param $purchasingPrice
     * @return $this
     * Расшифрока: Закупочная цена
     */
    public function setPurchasingPrice($purchasingPrice)
    {
        $clone = clone $this;
        $clone->good['purchasingPrice'] = $purchasingPrice;

        return $clone;
    }

}
