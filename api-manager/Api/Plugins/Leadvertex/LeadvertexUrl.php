<?php


class LeadvertexUrl extends Url
{

    private $offerName;
    private $apiMode;
    private $method;
    private $token;

    public function __construct($method = null)
    {
        $this->method = $method;
    }

    /**
     * @param string $token
     * @return LeadvertexUrl
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @param string $method
     * @return LeadvertexUrl
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @param string $apiMode
     * @return LeadvertexUrl
     */
    public function setApiMode($apiMode)
    {
        $this->apiMode = $apiMode;
        return $this;
    }

    /**
     * @param string $url
     * @return LeadvertexUrl
     */
    public function setUrl($url)
    {
        $this->prepareUrl = $url;
        return $this;
    }

    /**
     * @param null $offerName
     * @return LeadvertexUrl
     */
    public function setOfferName($offerName)
    {
        $this->offerName = $offerName;
        return $this;
    }

    protected function prepareUrl()
    {
        if (empty($this->offerName) or empty($this->apiMode) or empty($this->method) or empty($this->token)) {
            return null;
        }

        $this->prepareUrl = "https://{$this->offerName}.leadvertex.ru/api/{$this->apiMode}/{$this->method}.html?token={$this->token}";

        return $this;
    }
}