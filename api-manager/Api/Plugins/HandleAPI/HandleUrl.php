<?php


class HandleUrl extends Url
{
    /** @var string */
    protected $handleUrl;

    /**
     * @param string $handleUrl
     * @return HandleUrl
     */
    public function setUrl($handleUrl)
    {
        $this->handleUrl = $handleUrl;
        return $this;
    }
    /**
     * @return string
     * Возвращает полностью подготовленный URL, на который можно отправлять запрос
     */
    protected function prepareUrl()
    {
        if (!empty($this->handleUrl)) {
            $this->prepareUrl = $this->handleUrl;
        }
    }
}