<?php


class HandleApi extends Api
{
    /** @var HandleUrl */
    public $url;

    public function __construct($debug = false)
    {
        parent::__construct($debug);
        $this->setUrl();
    }

    protected function setMethodManager(){}

    protected function setUrl()
    {
        $this->url = new HandleUrl();
    }
}