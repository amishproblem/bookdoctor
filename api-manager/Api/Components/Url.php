<?php


abstract class Url
{
    /** @var string */
    protected $prepareUrl;

    public function getUrl()
    {
        if (empty($this->prepareUrl)) {
            $this->prepareUrl();
        }

        return $this->prepareUrl;
    }

    public function addGetParam($key, $value)
    {
        if (empty($this->prepareUrl)) {
            $this->prepareUrl();
        }
        $this->prepareUrl .= "&{$key}={$value}";
        return $this;
    }

    public function addPostfixSite($postfix)
    {
        if (empty($this->prepareUrl)) {
            $this->prepareUrl();
        }
        $this->prepareUrl .= $postfix;
        return $this;
    }

    /**
     * @return string
     * Возвращает полностью подготовленный URL, на который можно отправлять запрос
     */
    abstract protected function prepareUrl();
}