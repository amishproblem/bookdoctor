<?php


class Params
{
    /** var array */
    protected $params = [];

    /**
     * Возвращает массив с параметрами
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param $key
     * @param $value
     * @return Params
     * Установить кастомное поле
     */
    public function setCustomField($key, $value)
    {
        $this->params[$key] = $value;
        return $this;
    }
}