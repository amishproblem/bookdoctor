<?php

class Sender
{
    /** @var string */
    private $url;

    /** @var Params */
    private $params;

    /** @var bool */
    private $debug;

    public function __construct(Url $url, $debug = false)
    {
        $this->debug = $debug;
        $this->url = $url->getUrl();
    }

    /**
     * @param bool $assoc
     * @param bool $sendInJson
     * @param CurlSettings|null $curlSettings
     * @return mixed|string
     * Отправляет запрос методом POST
     */
    public function sendPost($assoc = false, $sendInJson = false, CurlSettings $curlSettings = null)
    {
        if (!$this->debug) {
            if($curl = curl_init()) {
                curl_setopt($curl, CURLOPT_URL, $this->url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
                curl_setopt($curl, CURLOPT_POST, true);
                if ($sendInJson) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($this->params->getParams(), JSON_UNESCAPED_UNICODE));
                } else {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($this->params->getParams()));
                }

                if (!empty($curlSettings) and !empty($curlSettings->getHeaders())) {
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $curlSettings->getHeaders());
                }

                $answer = curl_exec($curl);
                curl_close($curl);

                if ($this->isJSON($answer)) {
                    return json_decode($answer, $assoc);
                }

                return $answer;
            }
        } else {
            echo print_r([
                'Вы находитесь в режиме Дебага',
                'url' => $this->url,
                'post-params' => http_build_query($this->params->getParams())
            ]);
            exit;
        }

        return 'Error';
    }

    /**
     * @param bool $assoc
     * @param CurlSettings|null $curlSettings
     * @return mixed|string
     * Отправляет запрос методом GET
     */
    public function sendGet($assoc = false, CurlSettings $curlSettings = null)
    {
        if (!$this->debug) {
            if($curl = curl_init()) {
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_URL, $this->url);

                if (!empty($curlSettings) and !empty($curlSettings->getHeaders())) {
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $curlSettings->getHeaders());
                }

                $answer = curl_exec($curl);
                curl_close($curl);

                if ($this->isJSON($answer)) {
                    return json_decode($answer, $assoc);
                }

                return $answer;
            }
        } else {
            echo print_r([
                'Вы находитесь в режиме Дебага',
                'url' => $this->url
            ]);
            exit;
        }

        return 'Error';
    }

    public function isJSON($data = null) {
        if(!empty($data)) {
            $tmp = json_decode($data);
            return json_last_error() === JSON_ERROR_NONE and (is_object($tmp));
        }

        return false;
    }


    public function setParams(Params $params)
    {
        $clone = clone $this;
        $clone->params = $params;

        return $clone;
    }

}