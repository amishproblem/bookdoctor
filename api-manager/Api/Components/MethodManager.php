<?php


class MethodManager
{
    /** @var Api */
    protected $api;

    public function __construct(Api $api)
    {
        $this->api = $api;
    }

    protected function handle($method)
    {
        $this->api->setMethod($method);

        return $method;
    }
}