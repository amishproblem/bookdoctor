<?php


class DataGuard
{

    public static function isInt($data)
    {
        if (empty($data) or !is_int($data)) {
            return false;
        }

        return true;
    }

    public static function isString($data)
    {
        if (empty($data) or !is_string($data)) {
            return false;
        }

        return true;
    }

    public static function isFloat($data)
    {
        if (empty($data) or !is_float($data)) {
            return false;
        }

        return true;
    }

    public static function isArray($data)
    {
        if (empty($data) or !is_array($data)) {
            return false;
        }

        return true;
    }

    public static function isBool($data)
    {
        if (empty($data) or !is_bool($data)) {
            return false;
        }

        return true;
    }

    public static function notEmpty($data)
    {
        if (empty($data)) {
            return false;
        }

        return true;
    }

    /**
     * @param $params
     * @param bool $throwIsNeed
     * @return bool
     * @throws InvalidDataException
     */
    public static function notEmptyArray($params, $throwIsNeed = false)
    {
        $result = 1;
        foreach ($params as $key => $param) {
            $paramResult = self::notEmpty($param);
            $result *= (int)$paramResult;

            if ($throwIsNeed === true and $paramResult === false) {
                throw new InvalidDataException("Поле '{$key}' не было передано");
            }

        }

        return (bool)$result;
    }

    public static function postDataGuard($key, $value, $message = 'Доступ запрещен!')
    {
        if (!($_POST[$key] == $value)) {
            exit($message);
        }
    }

}