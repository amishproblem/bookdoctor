<?php


class Logger
{

    const LOG_DIR_PATH = __DIR__ . '/../../logs';
    const LOG_FILE_NAME = 'log.txt';

    /**
     * Логирует кастомные переменные
     * @param $message
     * @param $var
     */
    public static function logCustomVar($message, $var)
    {
        $type = 'Тип: Пользовательский лог' . PHP_EOL;
        $time = 'Время: ' . date("Y-m-d H:i:s") . PHP_EOL;
        $message = 'Комментарий: ' . $message . PHP_EOL;
        $var = 'Логируемая переменная: ' . self::toString($var) . PHP_EOL;

        self::logging($type . $time . $message . $var);
    }

    /**
     * Логирует входящий запрос, состоящий из массивов $_GET и $_POST.
     */
    public static function logInput()
    {
        $type = 'Тип: Входные данные' . PHP_EOL;
        $time = 'Время: ' . date("Y-m-d H:i:s") . PHP_EOL;
        $get = 'GET: ' . self::toString($_GET) . PHP_EOL;
        $post = 'POST: ' . self::toString($_POST) . PHP_EOL;

        self::logging($type . $time . $get . $post);
    }

    /**
     * Логирует данные, которые отправляются к серверу по API от нас.
     * @param Api $api
     * @param string $where
     */
    public static function logOutput(Api $api, $where = 'серверу')
    {
        $type = "Тип: Запрос к {$where}" . PHP_EOL;
        $time = 'Время: ' . date("Y-m-d H:i:s") . PHP_EOL;

        $url = 'URL: ' . $api->url->getUrl() . PHP_EOL;
        $params = 'POST: ' . self::toString($api->params->getParams()) . PHP_EOL;

        self::logging($type . $time . $url . $params);
    }

    /**
     * Логирует данные, которые возвращает сервер (ответ от API).
     * @param $answer
     * @param string $whois
     */
    public static function logAnswer($answer, $whois = 'сервера')
    {
        $type = "Тип: Ответ от {$whois}" . PHP_EOL;
        $time = 'Время: ' . date("Y-m-d H:i:s") . PHP_EOL;
        $answer = 'Тело ответа:'  . self::toString($answer) . PHP_EOL;

        self::logging($type . $time . $answer);
    }

    protected static function logging($var)
    {
        $logDirPath = realpath(self::LOG_DIR_PATH);
        $logFilePath = $logDirPath . DIRECTORY_SEPARATOR . self::LOG_FILE_NAME;

        /** Создаем каталог, если его не существует */
        if (!file_exists($logDirPath)) {
            mkdir($logDirPath,0777, true);
        }

        /** Дописываем лог в файл, если он существует, иначе пишем в новый файл */
        if (file_exists($logFilePath)) {
            file_put_contents(
                $logFilePath,
                self::getSeparator() . self::toString($var) . self::getSeparator(),
                FILE_APPEND
            );
        } else {
            file_put_contents(
                $logFilePath,
                self::getSeparator() . self::toString($var) . self::getSeparator()
            );
        }

        self::clearOldLines($logFilePath,30000, 10000);
    }

    /**
     * @param $logFilePath
     * @param $maxLines
     * @param $deleteLines
     * Проверяем файл ($logFilePath) на допустимую длину ($maxLines), если он превышает её,
     * тогда удаляем последние ($deleteLines) строк.
     */
    protected static function clearOldLines($logFilePath, $maxLines, $deleteLines)
    {
        $logFileContent = file_get_contents($logFilePath);

        if (self::getCountLinesToFile($logFileContent) > $maxLines) {
            $array = explode("\n", $logFileContent);
            for ($i = 0; $i < $deleteLines; $i++) {
                unset($array[$i]);
            }
            $logFileContent = implode("\n", $array);

            file_put_contents(
                $logFilePath,
                $logFileContent
            );
        }
    }

    /**
     * @param $varToLog
     * @return string
     * Делает из переменной значение строкового типа, пригодное для записи в лог
     */
    protected static function toString($varToLog)
    {
        if (!is_string($varToLog)) {
            return print_r($varToLog, true);
        }

        return $varToLog;
    }

    /**
     * @return string
     * Добавляет в файл с логами разделительную строку
     */
    protected static function getSeparator()
    {
        return '==========================================================' . PHP_EOL;
    }


    /**
     * @param $file
     * @return int
     * Получаем количество строк в файле
     */
    protected static function getCountLinesToFile($file)
    {
        return substr_count($file, "\n") + 1;
    }

}