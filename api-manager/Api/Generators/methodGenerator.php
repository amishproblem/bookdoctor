<?php


class methodGenerator
{
    public static function generateParams($csvPath)
    {
        $handle = fopen($csvPath, 'r');
        $example = file_get_contents(__DIR__ . '/paramsExample');
        while (($data = fgetcsv($handle, '1000', ",")) !== false) {
            $methodCode = $example;

            $textCase = self::modifyToManyCases($data[0]);

            $snake_case = $textCase['snake_case'];
            $CamelCase = $textCase['CamelCase'];

            $lowerCamelCase = lcfirst($CamelCase);

            $methodCode = str_replace('{{snake_case}}', $snake_case, $methodCode);
            $methodCode = str_replace('{{CamelCase}}', $CamelCase, $methodCode);
            $methodCode = str_replace('{{lowerCamelCase}}', $lowerCamelCase, $methodCode);

            echo '<pre>' . $methodCode . PHP_EOL . '</pre>';
        }
    }

    protected static function modifyToManyCases($string)
    {
        return [
            'snake_case' => self::to_snake_case($string),
            'lowerCamelCase' => self::toLowerCamelCase($string),
            'CamelCase' => self::ToCamelCase($string),
            'CAPS_CASE' => self::TO_CAPS_CASE($string),
        ];
    }

    protected static function to_snake_case($string)
    {
        $snake_case = mb_strtolower($string);
        return str_replace(['{','}'], '', $snake_case);
    }

    protected static function toLowerCamelCase($string)
    {
        $snake_case = mb_strtolower($string);
        $snake_case = str_replace(['{','}'], '', $snake_case);
        $CamelCase = ucwords(str_replace(['/','-','_'], ' ', $snake_case));
        $CamelCase = str_replace(' ', '', $CamelCase);
        return lcfirst($CamelCase);
    }

    protected static function ToCamelCase($string)
    {
        $snake_case = mb_strtolower($string);
        $snake_case = str_replace(['{','}'], '', $snake_case);
        $CamelCase = ucwords(str_replace(['/','-','_'], ' ', $snake_case));
        return str_replace(' ', '', $CamelCase);
    }

    protected static function TO_CAPS_CASE($string)
    {
        $snake_case = mb_strtolower($string);
        $snake_case = str_replace(['{','}'], '', $snake_case);
        $CAPS_CASE = mb_strtoupper($snake_case);
        return str_replace(['/','-'],'_', $CAPS_CASE);
    }

    public static function generateMethodManager($csvPath)
    {
        $handle = fopen($csvPath, 'r');
        $exampleConstant = file_get_contents(__DIR__ . '/methodManagerConstExample');
        $exampleMethod = file_get_contents(__DIR__ . '/methodManagerFunctionExample');

        $consts = [];
        $methods = [];

        while (($data = fgetcsv($handle, '1000', ",")) !== false) {
            $constantCode = $exampleConstant;
            $methodCode = $exampleMethod;

            $textCase = self::modifyToManyCases($data[0]);

            $snake_case = $textCase['snake_case'];
            $CamelCase = $textCase['CamelCase'];
            $lowerCamelCase = $textCase['lowerCamelCase'];
            $CAPS_CASE = $textCase['CAPS_CASE'];

            $methodCode = str_replace('{{snake_case}}', $snake_case, $methodCode);
            $methodCode = str_replace('{{CamelCase}}', $CamelCase, $methodCode);
            $methodCode = str_replace('{{lowerCamelCase}}', $lowerCamelCase, $methodCode);
            $methodCode = str_replace('{{CAPS_CASE}}', $CAPS_CASE, $methodCode);

            $constantCode = str_replace('{{snake_case}}', $snake_case, $constantCode);
            $constantCode = str_replace('{{CamelCase}}', $CamelCase, $constantCode);
            $constantCode = str_replace('{{lowerCamelCase}}', $lowerCamelCase, $constantCode);
            $constantCode = str_replace('{{CAPS_CASE}}', $CAPS_CASE, $constantCode);

            $consts[] = $constantCode;
            $methods[] = $methodCode;
        }

        echo '<pre>' . implode(PHP_EOL, $consts) . PHP_EOL . PHP_EOL . implode(PHP_EOL, $methods) . '</pre>';
    }
}